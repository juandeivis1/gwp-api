import knex from "knex";
import { Adquisicion } from "../interfaces/Adquisicion";
import { Costos } from "../interfaces/Costos";
import { Calidad } from "../interfaces/Calidad";
import { Riesgo } from "../interfaces/Riegos";
import { Pendiente } from "../interfaces/Pendiente";
import { Bitacora } from "../interfaces/Bitacora";
import { Avance } from "../interfaces/Avance";
import { Mensaje } from "../interfaces/Mensaje";


const knexNew = knex({
    client: 'mysql',
    connection: {
        host: '172.19.0.26',
        port: 3306,
        user: 'root',
        password: 'root',
        database: 'bd_gv_proyectos_viales'
    }
});

const knexProd = knex({
    client: 'mysql',
    connection: {
        host: '172.19.0.26',
        port: 3306,
        user: 'root',
        password: 'root',
        database: 'bd_gv_proyectos'
    }
});

export const migrarProyectos = () => {
    knexNew('gv_pro')
        .truncate().then(res => {
            knexProd('gv_pro')
                .select('proId', 'proyecto', 'created', 'updated')
                .map((proyecto: any) => {
                    try {
                        let proyectoObj = Object.assign(JSON.parse(proyecto.proyecto), { creado: proyecto.created, actualizado: proyecto.updated })
                        knexNew('gv_pro')
                            .insert(proyectoObj)
                            // .where('proId', '=', proyecto.proId)
                            .then(res => {

                            })
                            .catch(err => {
                                console.log('error:', proyecto.proId);
                            })
                        return proyectoObj
                    } catch (err) {
                        console.log('error json:', proyecto.proId);
                    }
                })
                .then((proyectos: any[]) => {
                    console.log(proyectos);
                })
                .catch(err => {
                    console.log('error:', err);
                })
        })

}

export const obtenerProyectos = () => {

    return knexNew('gv_pro')
        .select('proId', 'nombre', 'sinonimo', 'expediente', 'portafolio', 'prioridad', 'programa', 'creado', 'actualizado')
        .then((proyectos: any[]) => {
            return proyectos
        })
        .catch(err => {
            console.log('error:', err);
        })
}

export const migrarAdquiciones = () => {
    knexNew('gv_pro_adquisicion')
        .select('proId', 'adquisicion')
        .map((adquisicion: any) => {
            try {
                let tempAdquisicion = Object.assign({ abs: null, mopt: null, cne: null, ordenCompra: null, otras: null, contratoCalidad: null, ordenCalidad: null, contratacion: null, refrendo: null }, JSON.parse(adquisicion.adquisicion))

                knexNew('gv_pro_adquisicion')
                    .update(tempAdquisicion)
                    .where('proId', '=', adquisicion.proId)
                    .then(res => {
                        // console.log('listo:', adquisicion.proId);
                    })
                    .catch(err => {
                        console.log('error:', adquisicion.proId);
                    })
                return tempAdquisicion
            } catch (err) {
                console.log('error json:', adquisicion.proId);
            }


        })
        .then((adquisicion: Adquisicion[]) => {
            console.log(adquisicion.length);
        })
}

export const obtenerAdquisiciones = () => {
    return knexNew('gv_pro_adquisicion')
        .select('proId', 'abs', 'mopt', 'cne', 'otras', 'contratoCalidad', 'ordenCalidad', 'contratacion', 'refrendo')
        .then((adquisiciones: Adquisicion[]) => {
            return { adquisiciones };
        })
        .catch(err => {
            console.log('error:', err);
        })
}

export const migrarCostos = () => {
    knexNew('gv_pro_costos')
        .select('proId', 'costos')
        .map((costo: any) => {
            try {
                let costosTemp = Object.assign({ procedencia: null, prevision: null, estimado: null, adjudicado: null, calidad: null, comunales: null, ordenServicio: null, especie: null, especifica: null, ampliacion: null, reajuste: null, instituciones: null, extra1: null, extra2: null }, JSON.parse(costo.costos))
                knexNew('gv_pro_costos')
                    .update(costosTemp)
                    .where('proId', '=', costo.proId)
                    .then(res => {
                        console.log('listo:', costo.proId);
                    })
                    .catch(err => {
                        console.log('error:', costo.proId);
                    })
                return costosTemp
            }
            catch (err) {
                console.log('error json:', costo.proId);
            }
        })
        .then(res => {
            console.log(res);
        })
        .catch(err => {
            console.log(err);
        })
}

export const obtenerCostos = () => {
    knexNew('gv_pro_costos')
        .select('proId', 'procedencia', 'prevision', 'estimado', 'adjudicado', 'calidad', 'comunales', 'ordenServicio', 'especie', 'especifica', 'ampliacion', 'reajuste', 'instituciones', 'extra1', 'extra2')
        .then((costos: Costos[]) => {
            return { costos }
        })
        .catch(err => {
            console.log('error:', err);
        })

}

export const obtenerCalidad = () => {
    return knexNew('gv_pro_calidad')
        .select('proId', 'estado')
        .then((calidad: Calidad[]) => {
            return { calidad }
        })
        .catch(err => {
            console.log('error:', err);
        })

}

export const migrarRiesgos = () => {
    knexNew('gv_riesgos')
        .select('riesgoId', 'riesgo')
        .map((riesgo: any) => {
            try {
                let riesgoTemp = JSON.parse(riesgo.riesgo)
                knexNew('gv_riesgos')
                    .update(riesgoTemp)
                    .where('riesgoId', '=', riesgo.riesgoId)
                    .then(res => {
                        console.log('listo:', riesgo.riesgoId);
                    })
                    .catch(err => {
                        console.log('error:', riesgo.riesgoId);
                    })

                return riesgoTemp
            }
            catch (err) {
                console.log('error json:', riesgo.riesgoId, err);
            }

        })
        .then((riesgos: Riesgo[]) => {
            console.log(riesgos)
        })
        .catch(err => {
            console.log('error:', err);
        })
}

export const obtenerRiesgos = () => {
    return knexNew('gv_riesgos')
        .select('riesgoId', 'fase', 'categoria', 'subcategoria', 'causa', 'consecuencia', 'actividad')
        .then((riesgos: Riesgo[]) => {
            return { riesgos }
        })
        .catch(err => {
            console.log('error:', err);
        })

}

export const migrarAvance = () => {
    knexNew('gv_pro_avance')
        .select('proId', 'avance')
        .map((avance: any) => {
            try {
                let avanceTemp = JSON.parse(avance.avance)

                delete avanceTemp.bitacora
                delete avanceTemp.pendientes

                // actualizar avance

                knexNew('gv_pro_avance')
                    .update(avanceTemp)
                    .where('proId', '=', avance.proId)
                    .then(res => {
                        // console.log('listo:',avance.proId);
                    })
                    .catch(err => {
                        console.log('error:', avance.proId, avanceTemp);
                    })


                return avanceTemp
            }
            catch (err) {
                console.log('error json:', avance.proId, err);
            }

        })
        .then((avances: Avance[]) => {
            console.log(avances)
        })
        .catch(err => {
            console.log('error:', err);
        })
}

export const migrarBitacoras = () => {
    knexNew('gv_pro_bitacoras')
        .truncate()
        .then(res => {
            console.log('bitacoras limpias');
            knexNew('gv_pro_avance')
                .select('proId', 'avance')
                .map((avance: any) => {
                    try {
                        let avanceTemp = JSON.parse(avance.avance)

                        // insertar bitacoras
                        if (avanceTemp.bitacora) {

                            avanceTemp.bitacora.map(bitacora => {
                                bitacora = { ...bitacora, proId: avance.proId }

                                knexNew('gv_pro_bitacoras')
                                    .insert(bitacora)
                                    .then(res => {
                                        // console.log('correcto');
                                    })
                                    .catch(err => {
                                        console.log(bitacora);
                                    })
                                return bitacora
                            })

                        }

                    }
                    catch (err) {
                        console.log('error json:', avance.proId, err);
                    }

                })
                .then((bitacoras) => {
                    console.log(bitacoras.length)
                })
                .catch(err => {
                    console.log('error:', err);
                })
        })
        .catch(err => {
            console.log(err);
        })
}

export const obtenerBitacoras = (proId: number) => {
    return knexNew('gv_pro_bitacoras')
        .select('usuario', 'linea', 'fecha')
        .where('proId', proId)
        .then((bitacoras: Bitacora[]) => {
            return { bitacoras }
        })
        .catch(err => {
            console.log('error:', err);
        })
}

export const migrarPendientes = () => {
    knexNew('gv_pro_pendientes')
        .truncate()
        .then(res => {
            console.log('pendientes limpias');
            knexNew('gv_pro_avance')
                .select('proId', 'avance')
                .map((avance: any) => {
                    try {
                        let avanceTemp = JSON.parse(avance.avance)

                        // insertar pendientes

                        if (avanceTemp.pendientes) {
                            avanceTemp.pendientes.map(pendiente => {
                                pendiente = { ...pendiente, proId: avance.proId }
                                knexNew('gv_pro_pendientes')
                                    .insert(pendiente)
                                    .then(res => {
                                        // console.log('correcto');
                                    })
                                    .catch(err => {
                                        console.log(pendiente);
                                    })
                                return pendiente
                            })
                        }
                    }
                    catch (err) {
                        console.log('error json:', avance.proId, err);
                    }

                })
                .then((pendientes) => {
                    console.log(pendientes.length)
                })
                .catch(err => {
                    console.log('error:', err);
                })
        })
        .catch(err => {
            console.log(err);
        })
}

export const obtenerPendientes = (proId: number) => {
    return knexNew('gv_pro_pendientes')
        .select('asunto', 'generado', '	resuelto')
        .where('proId', proId)
        .then((pendientes: Pendiente[]) => {
            return { pendientes }
        })
        .catch(err => {
            console.log('error:', err);
        })
}

export const obtenerAvance = (proId: number) => {
    return knexNew('gv_pro_avance')
        .select('porcentaje', 'inicio', 'fin')
        .where('proId', proId)
        .then((avance: Avance) => {
            return { avance }
        })
        .catch(err => {
            console.log(err);
        })
}

export const migrarMensajes = () => {
    knexNew(' gv_pro_mensaje')
        .truncate()
        .then(res => {
            knexNew('gv_pro_mensajes')
                .map((res: any) => {
                    let mensajes = JSON.parse(res.mensajes)
                    return mensajes.map(mensaje => {
                        let nuevoMensaje = { ...mensaje, proId: res.proId }
                        return knexNew(' gv_pro_mensaje')
                            .insert(nuevoMensaje)
                            .then(res => {
                                return nuevoMensaje
                            })
                            .catch(err => {
                                console.log('error', nuevoMensaje);
                            })
                    })
                })
                .then(res => {
                    console.log(res.length);
                })
                .catch(err => {
                    console.log('error:', err);
                })
        })
}

export const obtenerMensajes = (proId: number) => {
    return knexNew(' gv_pro_mensaje')
        .select('mensaje', 'usuario', 'ts')
        .where('proId', proId)
        .then((mensajes: Mensaje[]) => {
            return { mensajes }
        })
        .catch(err => {
            console.log('error:', err);
            return []
        })
}

export const obtenerProyecto = (proId: number) => {
    return knexNew(' gv_pro')
        .select('proId', 'nombre', 'sinonimo', 'expediente', 'portafolio', 'prioridad', 'programa', 'creado', 'actualizado')
        .where('proId', proId)
        .then((proyecto: any[]) => {
            return { proyecto: proyecto[0] }
        })
        .catch(err => {
            console.log('error:', err);
            return {}
        })
}

export const migrarRiesgosProyectos = () => {
    knexNew('gv_pro_riesgo')
        .truncate()
        .then(res => {
            knexNew('gv_pro_riesgos')
                .map((res: any) => {
                    let riesgos = JSON.parse(res.riesgos)
                    return riesgos.map(riesgo => {
                        let nuevoRiesgo = { ...riesgo, proId: res.proId }
                        return knexNew('gv_pro_riesgo')
                            .insert(nuevoRiesgo)
                            .then(res => {
                                return nuevoRiesgo
                            })
                            .catch(err => {
                                console.log('error', nuevoRiesgo);
                            })
                    })
                })
                .then(res => {
                    console.log(res.length);
                })
                .catch(err => {
                    console.log('error:', err);
                })
        })
}

export const obtenerSeguimiento = (proId: number) => {
    return knexNew('gv_pro_seguimiento')
        .select('ejecutado', 'ejecutar')
        .where('proId', proId)
        .then(seguimiento => {
            return { seguimiento: seguimiento[0] }
        })
        .catch(err => {
            console.log('error:', err);
            return {}
        })
}

export const migrarSeguimiento = () => {
    knexNew('gv_pro_seguimiento')
        .map((seguimiento: any) => {
            let seguimientoTemp = { ...JSON.parse(seguimiento.seguimiento) }
            delete seguimientoTemp.completar
            knexNew('gv_pro_seguimiento')
                .update(seguimientoTemp)
                .where('proId', seguimiento.proId)
                .then(res => {
                    return seguimientoTemp
                })
                .catch(err => {
                    console.log('error', seguimiento.proId, seguimientoTemp);
                })
        })
        .then(seguimientos => {
            console.log(seguimientos);
        })
        .catch(err => {
            console.log('error', err);
        })
}

export const migrarCompletar = () => {
    knexNew('gv_pro_completar')
        .truncate()
        .then(res => {
            return knexNew('gv_pro_seguimiento')
                .map((res: any) => {
                    let seguimiento = JSON.parse(res.seguimiento)
                    if (seguimiento.completar) {
                        return seguimiento.completar.map(punto => {
                            punto = { ...punto, proId: res.proId }
                            // console.log(punto);
                            return knexNew('gv_pro_completar')
                                .insert(punto)
                                .then(res => {
                                    return punto;
                                })
                                .catch(err => {
                                    console.log('error', punto);

                                })
                        })
                    } else {
                        return null
                    }
                })
                .then(res => {
                    console.log(res.length);
                })
                .catch(err => {
                    console.log(err);

                })
        })
}

export const obtenerCompletar = (proId: number) => {
    return knexNew('gv_pro_completar')
        .select('punto', 'check')
        .where('proId', proId)
        .then(completar => {
            return { completar }
        })
}


