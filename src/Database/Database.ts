import knex from "knex";

export const knexGV = knex({
    client: 'mysql',
    connection: {
      port: 3306,
      user : 'root',
      host : '172.19.0.26',
      password : 'root',
      // host : 'localhost',
      // password : '12345678',
      database : 'bd_gv_proyectos_viales'
    }
  });

  export const bookshelf = require('bookshelf')(knexGV);