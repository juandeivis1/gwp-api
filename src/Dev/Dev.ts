import * as express from 'express'
import { implemented, requested, request, finish, users, user } from './Dev.functions';

const devRouter = express.Router()

devRouter.get('/implemented', implemented)
devRouter.get('/requested', requested)
devRouter.post('/request', request)
devRouter.post('/finish', finish)
devRouter.get('/users', users)
devRouter.post('/user', user)

export { devRouter as dev};