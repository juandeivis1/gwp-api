import * as express from 'express'
import { knexGV } from '../Database/Database';

export const implemented = (req: express.Request, resp: express.Response) => {
    knexGV('dev_solicitudes')
        .where('public', '=', 1)
        .select('implemented', 'tsRelease')
        .orderBy('tsRelease', 'desc')
        .limit(10)
        .map((implementacion: any) => Object.assign(JSON.parse(implementacion.implemented), { tsRelease: implementacion.tsRelease }))
        .then(res => {
            resp.json(res)
        })
        .catch(err => {
            resp.status(400).json({ mpzError: 'Algo salio mal al obtener implementaciones' })
            console.log(err);
        })
}

export const requested = (req: express.Request, resp: express.Response) => {
    knexGV('dev_solicitudes')
        .where('public', '=', 0)
        .select('id', 'tsCreated', 'request')
        .orderBy('tsCreated', 'asc')
        .map((solicitud: any) => Object.assign(JSON.parse(solicitud.request), { tsCreated: solicitud.tsCreated, id: solicitud.id }))
        .then(res => {
            resp.json(res)
        })
        .catch(err => {
            resp.status(400).json({ mpzError: 'Algo salio mal al obtener solicitudes' })
            console.log(err);
        })
}

export const request = (req: express.Request, resp: express.Response) => {    
    let { request } = req.body
    const ts = new Date().getTime()
    knexGV('dev_solicitudes')
        .insert({
            tsCreated: ts,
            request: JSON.stringify(request)
        }, 'id')
        .then(res => {
            resp.json(res[0])
        })
        .catch(err => {
            resp.status(400).json({ mpzError: 'Algo salio mal al obtener solicitudes' })
            console.log(err);
        })
}

export const finish = (req: express.Request, resp: express.Response) => {    
    let { id, implementado } = req.body

    const ts = new Date().getTime()
    if (id) {
        knexGV('dev_solicitudes')
            .update({
                implemented: JSON.stringify(implementado),
                public: 1,
                tsRelease: ts
            }, 'id')
            .where('id', '=', id)
            .then(() => {
                resp.json(id)
            })
            .catch(err => {
                resp.status(400).json({ mpzError: 'Algo salio mal al finalizar implementación' })
                console.log(err);
            })
    } else {
        knexGV('dev_solicitudes')
            .insert({
                implemented: JSON.stringify(implementado),
                public: 1,
                tsRelease: ts,
                tsCreated: ts
            }, 'id')
            .then(res => {
                resp.json(res[0])
            })
            .catch(err => {
                resp.status(400).json({ mpzError: 'Algo salio mal al obtener solicitudes' })
                console.log(err);
            })
    }
}

export const users = (req: express.Request, resp: express.Response) => {
    knexGV('gv_users')
        .select('userId', 'user', 'active', 'team', 'permissions', 'name')
        .orderBy('user', 'asc')
        .map((usuario: any) => {
            usuario.permissions = JSON.parse(usuario.permissions)
            return usuario
        })
        .then(res => {
            resp.json(res)
        })
        .catch(err => {
            resp.status(400).json({ mpzError: 'Algo salio mal al obtener usuarios' })
            console.log(err);
        })
}

export const user = (req: express.Request, resp: express.Response) => {    
    let { user, userId } = req.body
    
    if (userId) {
        knexGV('gv_users')
            .update({
                user: user.user,
                active: user.active,
                team: user.team,
                permissions: JSON.stringify(user.permissions),
                name: user.name
            }, 'userId')
            .where('userId', '=', userId)
            .then(res => {
                resp.json(userId)
            })
            .catch(err => {
                resp.status(400).json({ mpzError: 'Algo salio mal al actualizar usuario' })
                console.log(err);
            })
    } else {
        knexGV('gv_users')
            .insert({
                user: user.user,
                active: user.active,
                team: user.team,
                permissions:  JSON.stringify(user.permissions),
                name: user.name,
                fcmTokens: '[]'
            }, 'userId')
            .then(res => {
                resp.json(res)
            })
            .catch(err => {
                resp.status(400).json({ mpzError: 'Algo salio mal al actualizar usuario' })
                console.log(err);
            })

    }
}