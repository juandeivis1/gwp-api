import * as express from "express";
import { actividadUsuario } from "../Functions/Functions.common";
import Costos from "../../Modelos/Costos/Costos";
import { proyectoActualizado } from "../Proyectos/Proyectos.functions";

export const nuevosCostos = (request: express.Request, response: express.Response) => {

    let { proId, costos } = request.body;
    let ts = Date.now()

    existeCostos(costos, proId, ts, request, response)

}

const existeCostos = async (costos, proId, ts, request, response) => {
    let { geolocation, authorization } = request.headers;

    let existe = await Costos.forge({ proId }).fetch()

    existe && existe.toJSON() ?
        actualizar(costos, proId, ts, geolocation, authorization, response)
        : insertar(costos, proId, ts, geolocation, authorization, response)

}

const actualizar = async (costos, proId: number, ts: number, geolocation, authorization, response: express.Response) => {
    delete costos.proId
    try {
        let nuevoSeguimiento = await Costos
            .where({ proId })
            .save({ ...costos }, { patch: true })
            .call('toJSON')

        response.json({ ts: ts });
        proyectoActualizado(proId, ts)
        actividadUsuario(proId, authorization, { ts: ts, tipo: 'Nuevos costos', geolocation: geolocation, data: JSON.stringify(costos) })
    } catch (err) {
        console.log(`Error al actualizar el seguimiento (${proId},'${costos}',${ts})`, err, '\n');
        response.status(500).json({ mpz: 'Error al actualizar el seguimiento en MPZ! :(' })
    }
}

const insertar = async (costos, proId: number, ts: number, geolocation, authorization, response: express.Response) => {
    try {
        let newseguimiento = { ...costos, proId: proId }

        let nuevoSeguimiento = await Costos
            .forge()
            .save(newseguimiento)
            .call('toJSON')

        response.json({ ts: ts });
        proyectoActualizado(proId, ts)
        actividadUsuario(proId, authorization, { ts: ts, tipo: 'Creación costos', geolocation: geolocation, data: JSON.stringify(costos) })
    } catch (err) {
        console.log(`Error al insertar el seguimiento (${proId},'${costos}',${ts})`, err, '\n');
        response.status(500).json({ mpz: 'Error al insertar el seguimiento en MPZ! :(' })
    }
}
