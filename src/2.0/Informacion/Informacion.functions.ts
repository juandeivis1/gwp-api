import * as express from "express";
import { actividadUsuario } from "../Functions/Functions.common";
import Informacion from "../../Modelos/Informacion/Informacion";
import { proyectoActualizado } from "../Proyectos/Proyectos.functions";

export const nuevaInformacion = (request: express.Request, response: express.Response) => {
    let { proId, informacion } = request.body;
    let ts = Date.now()
    if (informacion.coordenadas) {
        informacion.coordenadas = JSON.stringify(informacion.coordenadas)
    }
    if (informacion.equipo) {
        informacion.equipo = JSON.stringify(informacion.equipo)
    }
    existeInformacion(informacion, proId, ts, request, response)
}

const existeInformacion = async (informacion, proId, ts, request, response) => {
    let { geolocation, authorization } = request.headers;

    let existe = await Informacion.forge({ proId }).fetch()

    existe && existe.toJSON() ?
        actualizar(informacion, proId, ts, geolocation, authorization, response)
        : insertar(informacion, proId, ts, geolocation, authorization, response)

}

const actualizar = async (informacion, proId: number, ts: number, geolocation, authorization, response: express.Response) => {
    try {
        let nuevaInformacion = await Informacion
            .where({ proId })
            .save({ ...informacion }, { patch: true })
            .call('toJSON')

        response.json({ ts: ts });
        proyectoActualizado(proId, ts)
        actividadUsuario(proId, authorization, { ts: ts, tipo: 'Nueva informacion', geolocation: geolocation, data: JSON.stringify(informacion) })
    } catch (err) {
        console.log(`Error al actualizar el informacion (${proId},'${informacion}',${ts})`, err, '\n');
        response.status(500).json({ mpz: 'Error al actualizar el informacion en MPZ! :(' })
    }
}

const insertar = async (informacion, proId: number, ts: number, geolocation, authorization, response: express.Response) => {
    try {
        let newInformacion = { ...informacion, proId: proId }

        let nuevaInformacion = await Informacion
            .forge()
            .save(newInformacion)
            .call('toJSON')

        response.json({ ts: ts });
        proyectoActualizado(proId, ts)
        actividadUsuario(proId, authorization, { ts: ts, tipo: 'Creación informacion', geolocation: geolocation, data: JSON.stringify(informacion) })
    } catch (err) {
        console.log(`Error al insertar el informacion (${proId},'${informacion}',${ts})`, err, '\n');
        response.status(500).json({ mpz: 'Error al insertar el informacion en MPZ! :(' })
    }
}