import * as express from "express";
import { enviarNuevoProyecto } from "../../Messaging/Messaging";
import Proyecto from "../../Modelos/Proyecto/Proyecto";
import { actividadUsuario } from "../Functions/Functions.common";
import { encryptByToken } from "../../Security/Security";

// Funciones 2.0

export const proById = async (proId) => {
    return Proyecto
        .where({ proId })
        .fetch({
            columns: Proyecto.columns,
            withRelated: Proyecto.relationships
        })
        .call('toJSON')
}

export const sincronizarProyecto = async (request: express.Request, response: express.Response) => {
    const { authorization } = request.headers;

    const { proId } = request.body;
    let proyecto = await Proyecto
        .where({ proId })
        .fetch({
            columns: Proyecto.columns,
            withRelated: Proyecto.relationships
        })
        .call('toJSON')

    if (proyecto.informacion.equipo) {
        proyecto.informacion.equipo = JSON.parse(proyecto.informacion.equipo)
    }
    if (proyecto.informacion.coordenadas) {
        proyecto.informacion.coordenadas = JSON.parse(proyecto.informacion.coordenadas)
    }

    const encryptedProyecto = encryptByToken(proyecto, authorization);
    response.json({ proyecto: encryptedProyecto })
}

export const sincronizarProyectos = async (request: express.Request, response: express.Response) => {
    let { authorization } = request.headers;

    let proyectos = await Proyecto
        .forge()
        .orderBy('actualizado', 'desc')
        .fetchAll({
            columns: Proyecto.columns,
            withRelated: [
                'avance',
                'informacion'
            ]
        })
        .call('toJSON')

    proyectos = proyectos.map(proyecto => {
        proyecto.informacion.equipo = proyecto.informacion.equipo ? JSON.parse(proyecto.informacion.equipo) : null;
        proyecto.informacion.coordenadas = proyecto.informacion.coordenadas ? JSON.parse(proyecto.informacion.coordenadas) : null;
        return proyecto;
    })
    const encryptedProyectos = encryptByToken(proyectos, authorization);
    response.json({ proyectos: encryptedProyectos })
}

export const sincronizarPortafolio = async (request: express.Request, response: express.Response) => {
    let { portafolio } = request.body;
    let { authorization } = request.headers;

    let proyectos = await Proyecto
        .forge()
        .where({ portafolio: portafolio })
        .orderBy('actualizado', 'desc')
        .fetchAll({ columns: Proyecto.columns, withRelated: ['bitacora', 'informacion'] })
        .call('toJSON')

    proyectos = proyectos.map(proyecto => {
        if (proyecto.informacion.equipo) {
            proyecto.informacion.equipo = JSON.parse(proyecto.informacion.equipo)
        }
        if (proyecto.informacion.coordenadas) {
            proyecto.informacion.coordenadas = JSON.parse(proyecto.informacion.coordenadas)
        }
        return proyecto;
    })
    const encryptedProyecto = encryptByToken(proyectos, authorization);

    response.json({ proyectos: encryptedProyecto })
}

export const proyectoActualizado = async (proId: number, ts: number) => {

    try {
        const proyecto = await Proyecto
        .forge()
        .where({ proId: proId })
        .save({ actualizado: ts }, { patch: true })
        .call('toJSON');
    } catch (err) {
        console.log(err);
    }
        
}

export const actualizarProyecto = async (request: express.Request, response: express.Response) => {
    let { geolocation, authorization } = request.headers;
    let { proId, proyecto } = request.body;
    let ts = Date.now()
    proyecto.actualizado = ts
    try {
        await Proyecto
            .where({ proId })
            .save(proyecto, { patch: true })
        response.json({ ts: ts })
        actividadUsuario(proId, authorization, { ts: ts, tipo: 'Actualización proyecto', geolocation: geolocation, data: JSON.stringify(proyecto) });
    } catch (err) {
        console.log(`'Error al actualizar el proyecto ${proId}, ${proyecto}, ${ts}`, err, '\n');
        response.status(500).json({ mpz: 'Error al actualizar el proyecto de MPZ! :(' })
    }
}

export const registrarProyecto = async (request: express.Request, response: express.Response) => {
    let dev = request.headers.origin == 'http://localhost:4200';
    let { geolocation, authorization } = request.headers;
    let { proyecto } = request.body;
    let ts = Date.now();
    proyecto.creado = ts;
    try {
        let nuevoProyecto = await Proyecto
            .forge()
            .save(proyecto)
            .call('toJSON')
        response.json({ proId: nuevoProyecto.proId, ts: ts });
        enviarNuevoProyecto(nuevoProyecto.sinonimo, nuevoProyecto.expediente, nuevoProyecto.proId, dev);
        actividadUsuario(nuevoProyecto.proId, authorization, { ts: ts, tipo: 'Creación proycto', geolocation: geolocation, data: JSON.stringify(proyecto) })

    } catch (err) {
        console.log(`call insertPro(${proyecto},${ts})`, new Date(), err, '\n');
        response.status(500).json({ mpz: 'Error al crear proyecto en MPZ! :(' })
    }
}