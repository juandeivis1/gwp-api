import * as express from "express";
import { registrarProyecto, actualizarProyecto, sincronizarProyecto, sincronizarProyectos, sincronizarPortafolio } from "./Proyectos.functions";
let proyectosRouter2 = express.Router();

proyectosRouter2.get("/todos", sincronizarProyectos )

proyectosRouter2.post("/sincronizar", sincronizarProyecto )

proyectosRouter2.post("/portafolio", sincronizarPortafolio )

proyectosRouter2.post("/registrar", registrarProyecto )

proyectosRouter2.post("/actualizar", actualizarProyecto )

export {proyectosRouter2 as proyectos2};
