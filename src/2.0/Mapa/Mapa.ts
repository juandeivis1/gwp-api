import * as express from "express";
import { buscarCamino, buscarProyectos } from "./Mapa.functions";

let mapaRouter = express.Router();

mapaRouter.post('/searchRoads', buscarCamino)
mapaRouter.post('/searchProjects', buscarProyectos)

export { mapaRouter as mapa };