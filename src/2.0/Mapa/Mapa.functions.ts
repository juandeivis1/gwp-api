import * as express from "express";
import Caminos from "../../Modelos/Caminos/Caminos";
import Proyecto from "../../Modelos/Proyecto/Proyecto";
import { encryptByToken } from "../../Security/Security";

export const buscarCamino = async (req: express.Request, resp: express.Response) => {
    const { camino } = req.body;
    const { authorization } = req.headers;

    const caminos = await Caminos.query(qb => {

        if (!!camino.distrito && camino.distrito.length > 0) {
            qb.whereIn('distrito', camino.distrito)
        }
        if (!!camino.caminoId) {
            qb.where({ caminoId: camino.caminoId })
        }
    })
        .fetchAll()
        .call('toJSON')
    const encryptedCaminos = encryptByToken(caminos, authorization);

    resp.json(encryptedCaminos)
}

export const buscarProyectos = async (req, resp) => {
    const { filtros } = req.body;
    const { authorization } = req.headers;

    const proyectos = await Proyecto
        .query(qb => {
            qb
                .select('gv_pro.proId', 'gv_pro.nombre', 'gv_pro.sinonimo', 'gv_pro.expediente', 'gv_pro.portafolio', 'gv_pro.prioridad', 'gv_pro.programa', 'gv_pro.creado', 'gv_pro.actualizado', 'gv_pro_informacion.coordenadas')
                .leftJoin('gv_pro_informacion', 'gv_pro_informacion.proId', 'gv_pro.proId')
                .whereNotNull('gv_pro_informacion.coordenadas')
            if (filtros.portafolio.length > 0) {
                qb.whereIn('portafolio', filtros.portafolio)
            }
            if (filtros.prioridad.length > 0) {
                qb.whereIn('prioridad', filtros.prioridad)
            }
            if (filtros.programa.length > 0) {
                qb.whereIn('programa', filtros.programa)
            }
            if (filtros.district.length > 0) {
                qb.whereIn('district', filtros.district)
            }
            if (filtros.city.length > 0) {
                qb.whereIn('city', filtros.city)
            }
            if (filtros.community.length > 0) {
                qb.whereIn('community', filtros.community)
            }
            if (filtros.neighborhood.length > 0) {
                qb.whereIn('neighborhood', filtros.neighborhood)
            }
        })
        .fetchAll()
        .call('toJSON')

    const encryptedProyectos = encryptByToken(proyectos, authorization);

    resp.json(encryptedProyectos)
}