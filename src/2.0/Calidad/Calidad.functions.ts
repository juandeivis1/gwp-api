import * as express from "express";
import { actividadUsuario } from "../Functions/Functions.common";
import Calidad from "../../Modelos/Calidad/Calidad";
import { proyectoActualizado } from "../Proyectos/Proyectos.functions";

export const nuevaCalidad = (request: express.Request, response: express.Response) => {
    let { proId, calidad } = request.body;
    let ts = Date.now()

    existeCalidad(calidad, proId, ts, request, response)

}

const existeCalidad = async (calidad, proId, ts, request, response) => {
    let { geolocation, authorization } = request.headers;
    
    let existe = await Calidad.forge({ proId }).fetch()
    
    existe && existe.toJSON() ?
    actualizar(calidad, proId, ts, geolocation, authorization, response)
    : insertar(calidad, proId, ts, geolocation, authorization, response)
    
}

const actualizar = async (calidad, proId: number, ts: number, geolocation, authorization, response: express.Response) => {
    try {
        let nuevaCalidad = await Calidad
        .where({ proId })
        .save({ ...calidad }, { patch: true })
        .call('toJSON')
        
        response.json({ ts: ts });
        proyectoActualizado(proId, ts)
        actividadUsuario(proId, authorization, { ts: ts, tipo: 'Nueva calidad', geolocation: geolocation, data: JSON.stringify(calidad) })
    } catch (err) {
        console.log(`Error al actualizar el calidad (${proId},'${calidad}',${ts})`, err, '\n');
        response.status(500).json({ mpz: 'Error al actualizar el calidad en MPZ! :(' })
    }
}

const insertar = async (calidad, proId: number, ts: number, geolocation, authorization, response: express.Response) => {
    try {
        let newseguimiento = { ...calidad, proId: proId }
        
        let nuevaCalidad = await Calidad
        .forge()
        .save(newseguimiento)
        .call('toJSON')
        
        response.json({ ts: ts });
        proyectoActualizado(proId, ts)
        actividadUsuario(proId, authorization, { ts: ts, tipo: 'Creación calidad', geolocation: geolocation, data: JSON.stringify(calidad) })
    } catch (err) {
        console.log(`Error al insertar el calidad (${proId},'${calidad}',${ts})`, err, '\n');
        response.status(500).json({ mpz: 'Error al insertar el calidad en MPZ! :(' })
    }
}

// const actualizarCalidad = (proId, calidad, ts, request: express.Request, response: express.Response) => {
//     let knex = request.headers.origin == 'http://localhost:4200' ? knexDev : knexProd
//     let { geolocation, authorization } = request.headers;

//     let calidadTemp = JSON.stringify(calidad)
//     calidadTemp = calidadTemp
//         .replace(/\n/gm, " ")
//         .replace(/\r/gm, " ")
//         .replace(/\t/gm, " ")
//         .replace(/\f/gm, " ")
//         .replace(/\&/gm, " ")
//         .replace(/\\b/gm, " ")
//         .replace(/\\n/gm, " ")
//         .replace(/\\r/gm, " ")
//         .replace(/\\t/gm, " ")
//         .replace(/\\f/gm, " ")
//         .replace(/\\&/gm, " ")
//         .replace(/\r\n/gm, " ")
//         .replace(/\\r\\n/gm, " ");

//     knex.raw(`call calidad(${proId},'${calidadTemp}',${ts})`)
//         .then(() => {
//             response.json({ ts: ts });
//             actividadUsuario(proId, authorization, { ts: ts, tipo: 'Nueva calidad agregada', geolocation: geolocation, newData: calidad }, knex)
//         })
//         .catch(err => {
//             console.log(`call calidad(${proId},'${calidadTemp}',${ts})`, new Date(), err, '\n');
//             response.status(500).json({ mpz: 'Error al actualizar calidad en MPZ! :(' })
//         })

// }