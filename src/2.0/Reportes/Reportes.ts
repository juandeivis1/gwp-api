import * as express from 'express';
import { reporteSeguimiento, reporteFase, reportePrioridad, reporteCostosPortafolio, reporteCostosPrograma, reportePendientes, reporteCostosModalidad, reporteProyectosPorFecha, reporteProyectosPorPortafolioPrograma } from './Reportes.funcitons';

const reportesRouter = express.Router();

export { reportesRouter as reportes2 };

reportesRouter.post('/seguimiento', reporteSeguimiento)
reportesRouter.post('/fase', reporteFase)
reportesRouter.post('/prioridad', reportePrioridad)
reportesRouter.get('/portafolio', reporteCostosPortafolio)
reportesRouter.get('/programa', reporteCostosPrograma)
reportesRouter.get('/pendientes', reportePendientes)
reportesRouter.get('/modalidad', reporteCostosModalidad)
reportesRouter.post('/fecha', reporteProyectosPorFecha)
reportesRouter.post('/porpro', reporteProyectosPorPortafolioPrograma)
