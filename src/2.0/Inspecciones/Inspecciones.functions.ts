import * as express from "express";
import Inspecciones from "../../Modelos/Inspecciones/Inspecciones";

export const guardarInspeccion = (req: express.Request, resp: express.Response) => {
    const { inspeccion } = req.body
    console.log(inspeccion);
    
    // inspeccion.id ? actualizarInspeccion(inspeccion, resp) : crearInspeccion(inspeccion, resp)
    resp.json(null)
}

const actualizarInspeccion = async (inspeccion, resp) => {
    const ts = Date.now()
    try {
        const nuevaInspeccon = await Inspecciones.where({ id: inspeccion.id }).save(inspeccion, { patch: true }).call('toJSON')
        resp.json({ id: nuevaInspeccon.id })
    }
    catch (err) {
        console.log(`'Error al insertar la inspeccion ${inspeccion},${ts}`, err, '\n');
        resp.status(500).json({ mpz: 'Error al insertar la inspeccion en MPZ! :(' })
    }
}

const crearInspeccion = async (inspeccion, resp) => {
    const ts = Date.now();
    inspeccion.creacion = ts
    try {
        const nuevaInspeccon = await Inspecciones.forge().save(inspeccion).call('toJSON')
        resp.json({ id: nuevaInspeccon.id })
    }
    catch (err) {
        console.log(`'Error al insertar la inspeccion ${inspeccion},${ts}`, err, '\n');
        resp.status(500).json({ mpz: 'Error al insertar la inspeccion en MPZ! :(' })
    }
}

export const finalizarInspeccion = (req: express.Request, resp: express.Response) => {
    const { inspeccion } = req.body
    const ts = Date.now();
    inspeccion.realizado = ts;
    inspeccion.id ? actualizarInspeccion(inspeccion, resp) : crearInspeccion(inspeccion, resp)

}