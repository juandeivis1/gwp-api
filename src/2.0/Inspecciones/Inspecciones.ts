import * as express from "express";
import { guardarInspeccion } from "./Inspecciones.functions";

let inspeccionesRouter2 = express.Router();

inspeccionesRouter2.post('/guardar', guardarInspeccion)

export { inspeccionesRouter2 as inspecciones2 };