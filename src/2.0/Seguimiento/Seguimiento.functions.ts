import * as express from "express";

import Seguimiento from "../../Modelos/Seguimiento/Seguimiento";
import { proyectoActualizado } from "../Proyectos/Proyectos.functions";
import { actividadUsuario } from "../Functions/Functions.common";
import Inspeccion from "../../Modelos/Inspeccion/Inspeccion";
import { Completar } from "../../Modelos/Completar/Completar";


export const nuevoSegumiento = (request: express.Request, response: express.Response) => {
    let { proId, seguimiento } = request.body;
    let ts = Date.now()

    existeSeguiento(seguimiento, proId, ts, request, response)
}

const existeSeguiento = async (seguimiento, proId, ts, request, response) => {
    let { geolocation, authorization } = request.headers;

    let existe = await Seguimiento.forge({ proId }).fetch()

    existe && existe.toJSON() ?
        actualizar(seguimiento, proId, ts, geolocation, authorization, response)
        : insertar(seguimiento, proId, ts, geolocation, authorization, response)
}

const actualizar = async (seguimiento, proId: number, ts: number, geolocation, authorization, response: express.Response) => {
    try {
        let nuevoSeguimiento = await Seguimiento
            .where({ proId })
            .save({ ...seguimiento }, { patch: true })
            .call('toJSON')

        response.json({ ts: ts });
        proyectoActualizado(proId, ts)
        actividadUsuario(proId, authorization, { ts: ts, tipo: 'Nuevo seguimiento', geolocation: geolocation, data: JSON.stringify(seguimiento) })
    } catch (err) {
        console.log(`Error al actualizar el seguimiento (${proId},'${seguimiento}',${ts})`, err, '\n');
        response.status(500).json({ mpz: 'Error al actualizar el seguimiento en MPZ! :(' })
    }
}

const insertar = async (seguimiento, proId: number, ts: number, geolocation, authorization, response: express.Response) => {
    try {
        let newseguimiento = { ...seguimiento, proId: proId }

        let nuevoSeguimiento = await Seguimiento
            .forge()
            .save(newseguimiento)
            .call('toJSON')

        response.json({ ts: ts });
        proyectoActualizado(proId, ts)
        actividadUsuario(proId, authorization, { ts: ts, tipo: 'Creación seguimiento', geolocation: geolocation, data: JSON.stringify(seguimiento) })
    } catch (err) {
        console.log(`Error al insertar el seguimiento (${proId},'${seguimiento}',${ts})`, err, '\n');
        response.status(500).json({ mpz: 'Error al insertar el seguimiento en MPZ! :(' })
    }
}

export const nuevaInspeccion = async (request: express.Request, response: express.Response) => {
    let { geolocation, authorization } = request.headers;

    let { proId, inspeccion } = request.body;
    let nuevaInspeccion = { ...inspeccion, proId: proId }
    let ts = Date.now()

    try {
        let inspeccionCreada = Inspeccion.forge().save(nuevaInspeccion).call('toJSON')
        inspeccion.inspeccionId = inspeccionCreada.id

        response.json({ ts: ts, inspeccion: inspeccion });
        proyectoActualizado(proId, ts)
        actividadUsuario(proId, authorization, { ts: ts, tipo: 'Nueva inspeccion', geolocation: geolocation, data: JSON.stringify(nuevaInspeccion) })
    } catch (err) {
        console.log(`Error al insertar la inspeccion ('${nuevaInspeccion}',${ts})`, err, '\n');
        response.status(500).json({ mpz: 'Error al insertar el seguimiento en MPZ! :(' })
    }
}

export const finalizarInspeccion = async (request: express.Request, response: express.Response) => {
    let { geolocation, authorization } = request.headers;
    let { inspeccion } = request.body;
    let { proId, inspeccionId } = inspeccion
    let ts = Date.now()
    delete inspeccion.inspeccionId
    try {
        let inspeccionFinalizada = await Inspeccion.where({ inspeccionId: inspeccionId }).save(inspeccion, { patch: true }).call('toJSON')

        response.json({ ts: ts });
        proyectoActualizado(proId, ts)
        actividadUsuario(inspeccion.proId, authorization, { ts: ts, tipo: 'Inspeccion finalizada', geolocation: geolocation, data: JSON.stringify(inspeccion) })
    } catch (err) {
        console.log(`Error al insertar la inspeccion ${nuevaInspeccion},${ts}`, err, '\n');
        response.status(500).json({ mpz: 'Error al finalizar la inspeccion en MPZ! :(' })
    }
}

export const generarCompletar = async (request: express.Request, response: express.Response) => {
    let { geolocation, authorization } = request.headers;
    let { proId } = request.body;
    let ts = Date.now()
    
    try {
        let lista = listaGenerar.map(punto => {
            return Completar.forge().save({ punto: punto, proId: proId, check: false }).call('toJSON')
        })
        let resultado = await Promise.all(lista)
        resultado = resultado.map(_resultado => {
            let id = _resultado.id
            delete _resultado.id
            return {..._resultado, completarId: id }
        })
        response.json({lista : resultado, ts: ts});

        proyectoActualizado(proId, ts)
        actividadUsuario(proId, authorization, { ts: ts, tipo: 'Lista completar', geolocation: geolocation, data: 'puntos creados' })
    } catch (err) {
        console.log(`Error al insertar completar ${ts})`, err, '\n');
        response.status(500).json({ mpz: 'Error al insertar lista por completar en MPZ! :(' })
    }
}

export const completarListo = async (request: express.Request, response: express.Response) => {
    let { geolocation, authorization } = request.headers;
    let { completar } = request.body;
    let ts = Date.now()
    
    try {
        await Completar.where({completarId : completar.completarId }).save({  check: true }, {patch: true}).call('toJSON')
        response.json({ ts: ts});
        proyectoActualizado(completar.proId, ts)
        actividadUsuario(completar.proId, authorization, { ts: ts, tipo: 'Lista completar', geolocation: geolocation, data: JSON.stringify(completar) })
    } catch (err) {
        console.log(`Error al insertar completar ${ts})`, err, '\n');
        response.status(500).json({ mpz: 'Error al completar punto en MPZ! :(' })
    }
}

export const completarNuevo = async (request: express.Request, response: express.Response) => {
    let { geolocation, authorization } = request.headers;
    let { completar } = request.body;
    let ts = Date.now()
    
    try {
        let nuevoCompletar = await Completar.forge().save(completar).call('toJSON')
        response.json({ ts: ts, completarId: nuevoCompletar.id });
        proyectoActualizado(completar.proId, ts)
        actividadUsuario(completar.proId, authorization, { ts: ts, tipo: 'Lista completar', geolocation: geolocation, data: JSON.stringify(completar) })
    } catch (err) {
        console.log(`Error al insertar completar ${ts})`, err, '\n');
        response.status(500).json({ mpz: 'Error al completar punto en MPZ! :(' })
    }
}

const listaGenerar = [
    "Definir acción",
    "Definir equipo de proyecto",
    "Realizar visita al proyecto",
    "Elaborar Estudio Topográfico",
    "Elaborar Identificación del Proyecto",
    "Revisar Documento de Identificación",
    "Informe Social",
    "Definir Alcances ",
    "Elaborar fundamentos Técnicos",
    "Elaborar Plan de Calidad",
    "Elaborar Plan de Riesgos",
    "Revisar estudios",
    "Presentar estudios corregidos",
    "Aprobar Diseños Finales",
    "Enviar Planos al CFIA",
    "Pagar tasación CFIA",
    "Permiso de construcción",
    "Gestionar Aportes",
    "Elaborar Términos de Referencia",
    "Revisar Términos de Referencia",
    "Generar solicitud ABS",
    "Trámites de contratación Administrativa",
    "Visita Preoferta",
    "Emitir Criterio de Adjudicación",
    "Definir fecha para inicio de obras",
    "Dar orden de inicio",
    "Autorización salida de materiales",
    "Ejecución de obra",
    "Gestión de Cambios",
    "Elaborar Informe Final de Control de Calidad",
    "Revisar Informe Final de Control de Calidad ",
    "Elaborar Informe Final de Obras",
    "Revisar Informe final de Obras",
    "Tramitar Pago a la empresa",
    "Gestionar Transferencia de Beneficios",
    "Realizar Cierre del Proyecto",
    "Proyecto Cerrado"
]