import * as express from "express";
import { nuevoSegumiento, nuevaInspeccion, finalizarInspeccion, generarCompletar, completarListo, completarNuevo } from "./Seguimiento.functions";
let seguimientoRouter2 = express.Router();

seguimientoRouter2.post("/actualizar", nuevoSegumiento)
seguimientoRouter2.post("/inspeccion/nueva", nuevaInspeccion)
seguimientoRouter2.post("/inspeccion/finalizar", finalizarInspeccion)
seguimientoRouter2.post("/completar", generarCompletar)
seguimientoRouter2.post("/completar/listo", completarListo)
seguimientoRouter2.post("/completar/nuevo", completarNuevo)

export { seguimientoRouter2 as seguimiento2 };
