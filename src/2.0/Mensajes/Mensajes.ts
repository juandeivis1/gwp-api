import * as express from "express";
import { nuevoMensaje } from "./Mensajes.functions";

let mensajesRouter2 = express.Router();

mensajesRouter2.post("/nuevo", nuevoMensaje)

export { mensajesRouter2 as mensajes2 };