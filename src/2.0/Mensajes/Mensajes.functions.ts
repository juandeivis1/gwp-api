import * as express from "express";

import { sendNewMessageTo } from "../../Messaging/Messaging";
import Mensajes from "../../Modelos/Mensajes/Mensajes";
import { proyectoActualizado, proById } from "../Proyectos/Proyectos.functions";
import { actividadUsuario, obtenerTokensEquipo } from "../Functions/Functions.common";


export const nuevoMensaje = async (request: express.Request, response: express.Response) => {
    let { geolocation, authorization } = request.headers;
    let { mensaje } = request.body;
    let ts = Date.now()
    try {

        let nuevoMensaje = await Mensajes.forge().save(mensaje).call('toJSON');
        const proyecto = await proById(mensaje.proId);        
        let tokens = await obtenerTokensEquipo(mensaje.proId);
        let resultado;
        tokens.length > 0 ?
            resultado = await sendNewMessageTo(proyecto.expediente, `${mensaje.usuario.toUpperCase()}: ${mensaje.mensaje}`, tokens, mensaje, mensaje.proId)
            : resultado = { successCount: 0 }
        response.json({ ts: ts, success: resultado })
        proyectoActualizado(mensaje.proId, ts)
        actividadUsuario(mensaje.proId, authorization, { ts: ts, tipo: 'Nuevo mensaje', geolocation: geolocation, data: JSON.stringify(mensaje) })
    } catch (err) {
        console.log(err);
        response.status(500).json({ mpz: 'Error al insertar mensaje en MPZ! :(' })
    }
}

