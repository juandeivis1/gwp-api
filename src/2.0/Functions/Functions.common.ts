import Actividad from '../../Modelos/Actividad/Actividad';
import { tokenIdByToken } from '../Auth/Auth.functions';
import Token from '../../Modelos/Token/Token';
import Informacion from '../../Modelos/Informacion/Informacion';

export const actividadUsuario = async (proId, authorization, actividad) => {
    try {
        let infoToken = await tokenIdByToken(authorization)
        insertarActividad(proId, actividad, infoToken.idToken)
    }
    catch(error) {
        console.log(`Error al obtener el token '${authorization}`, actividad, error, '\n');
    }
}

const insertarActividad = async (proId, actividad, idToken) => {
    try {
        let listo = await Actividad
        .forge()
        .save({
            idToken: idToken,
            proId: proId,
            ts: actividad.ts,
            tipo: actividad.tipo,
            geolocation: actividad.geolocation,
            data: actividad.data
        })
        .call('toJSON')        
    }
    catch(error) {
        console.log(`Error al intentar insertar actividad: proId: '${proId}, idTokne: ${idToken}`, actividad, error, '\n');
    }
}

export const obtenerTokens = async (usuarios: string[]) => {
    let ts = Date.now() - 3888000000;
    return await Token
        .query(qb => {
            qb
                .select('fcmToken')
                .where('lastAccess', '>=', ts)
                .whereNull('invalidated')
                .leftJoin('gv_users', 'gv_users.userId', 'gv_tokens.userId')
                .andWhere('gv_users.user', 'in', usuarios)
                .whereNotNull('gv_tokens.fcmToken')
        })
        .fetchAll()
        .call('toJSON')
        .map(fcm => fcm.fcmToken)
}

export const obtenerTokensEquipo = async (proId: number) => {
    let informacion = await Informacion
        .forge({ proId: proId })
        .fetch()
        .call('toJSON')

    let usuarios = JSON.parse(informacion.equipo)
    usuarios = usuarios.map(user => user.nombre)
    return await obtenerTokens(usuarios)
}