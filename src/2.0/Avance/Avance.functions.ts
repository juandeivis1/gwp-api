import * as express from "express";
import { enviarNuevaBitacora } from './../../Messaging/Messaging'
import { proyectoActualizado, proById } from "../Proyectos/Proyectos.functions";
import { Bitacora } from "../../Modelos/Bitacora/Bitacora";
import { Pendiente } from "../../Modelos/Pendiente/Pendiente";
import Avance from "../../Modelos/Avance/Avance";
import { actividadUsuario } from "../Functions/Functions.common";


export const nuevoAvance = (request: express.Request, response: express.Response) => {

    let { proId, avance } = request.body;
    let ts = Date.now()

    existeAvance(avance, proId, ts, request, response)
}

const existeAvance = async (avance, proId, ts, request, response) => {
    try {
        let { geolocation, authorization } = request.headers;
        let existe = await Avance.forge({ proId }).fetch()

        existe && existe.toJSON() ?
            actualizar(avance, proId, ts, geolocation, authorization, response) :
            insertar(avance, proId, ts, geolocation, authorization, response)
    }
    catch (err) {
        console.log(`Error desconocido en el avance ${proId},'${avance}',${ts}`, new Date(), err, '\n');
        response.status(500).json({ mpz: 'Error al comprobar el avance en MPZ! :(' })
    }
}

const actualizar = async (avance: Avance, proId: number, ts: number, geolocation, authorization, response: express.Response) => {
    try {
        let nuevoAvance = await Avance
            .where({ proId })
            .save({ ...avance }, { patch: true })
            .call('toJSON')

        response.json({ ts: ts });
        proyectoActualizado(proId, ts)
        actividadUsuario(proId, authorization, { ts: ts, tipo: 'Nuevo avance', geolocation: geolocation, data: JSON.stringify(avance) })
    }
    catch (err) {
        console.log(`Error al actualizar el avance: ${proId},'${avance}',${ts})`, new Date(), err, '\n');
        response.status(500).json({ mpz: 'Error al actualizar el avance en MPZ! :(' })
    }
}

const insertar = async (avance: Avance, proId: number, ts: number, geolocation, authorization, response: express.Response) => {
    try {
        let newAvance = { ...avance, proId: proId }

        let nuevoAvance = await Avance
            .forge()
            .save(newAvance)
            .call('toJSON')

        response.json({ ts: ts });
        proyectoActualizado(proId, ts)
        actividadUsuario(proId, authorization, { ts: ts, tipo: 'Creación avance', geolocation: geolocation, data: JSON.stringify(avance) })
    }
    catch (err) {
        console.log(`Error al insertar avance ${proId},'${avance}',${ts} `, new Date(), err, '\n');
        response.status(500).json({ mpz: 'Error al insertar el avance en MPZ! :(' })
    }
}

export const nuevaBitacora = async (request: express.Request, response: express.Response) => {
    let dev = request.headers.origin == 'http://localhost:4200'
    let { geolocation, authorization } = request.headers;

    let { proId, bitacora } = request.body;
    let ts = Date.now()

    bitacora = { ...bitacora, proId: proId }

    try {
        let nuevaBitacora = await Bitacora
            .forge()
            .save(bitacora)
            .call('toJSON');
        
        response.json(ts);
        const proyecto = await proById(proId); 
        const body = `${bitacora.usuario.toUpperCase()}: ${bitacora.linea}`;
        enviarNuevaBitacora(body, proyecto.expediente, proId);
        proyectoActualizado(proId, ts);
        actividadUsuario(proId, authorization, { ts: ts, tipo: 'Nuevo bitacora', geolocation: geolocation, data: JSON.stringify(bitacora) });
    }
    catch (err) {
        console.log(`Error al insertar la bitacora: ${proId}, ${bitacora}`, ts, err, '\n');
        response.status(500).json({ mpz: 'Error al insertar la bitacora en MPZ! :(' })
    }
}

export const pendienteNuevo = async (request: express.Request, response: express.Response) => {
    let { geolocation, authorization } = request.headers;

    let { proId, pendiente } = request.body;
    let ts = Date.now()

    try {
        let nuevoPendiente = await Pendiente
            .forge()
            .save({ proId: proId, ...pendiente })
            .call('toJSON')

        pendiente.pendienteId = nuevoPendiente.id
        delete pendiente.id;

        response.json({ ts: ts, pendiente: pendiente });
        proyectoActualizado(proId, ts)
        actividadUsuario(proId, authorization, { ts: ts, tipo: 'Nuevo pendiente', geolocation: geolocation, data: JSON.stringify(pendiente) })
    }
    catch (err) {
        console.log(`'Error al insertar pendiente: ${pendiente}, proId: ${proId}`, new Date(), err, '\n');
        response.status(500).json({ mpz: 'Error al insertar pendiente! :(' })
    }
}

export const pendienteAtendido = async (request: express.Request, response: express.Response) => {
    let { geolocation, authorization } = request.headers;

    let { pendienteId, resuelto, proId } = request.body;
    let ts = Date.now()

    try {
        let pendienteFinalizdo = await Pendiente
            .where({ pendienteId })
            .save({ resuelto: resuelto }, { patch: true })
            .call('toJSON')

        response.json(ts);
        proyectoActualizado(proId, ts)
        actividadUsuario(proId, authorization, { ts: ts, tipo: 'Pendiente finalizado', geolocation: geolocation, data: JSON.stringify({ pendienteId: pendienteId, resuelto: resuelto }) });
    } 
    catch (err) {
            console.log(`'Error al finalizar pendiente: ${pendienteId}`, ts, err, '\n');
            response.status(500).json({ mpz: 'Error al finalizar pendiente! :(' })
    }
}