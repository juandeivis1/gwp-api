import * as express from "express";
import { nuevoAvance, nuevaBitacora, pendienteNuevo, pendienteAtendido } from "./Avance.functions";
let avanceRouter2 = express.Router();

avanceRouter2.post("/actualizar", nuevoAvance )

avanceRouter2.post("/bitacora", nuevaBitacora )

avanceRouter2.post("/pendiente", pendienteNuevo )

avanceRouter2.post("/pendiente/atendido", pendienteAtendido )

export { avanceRouter2 as avance2};