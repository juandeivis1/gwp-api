import * as express from "express";
import * as axios from 'axios';
import TokenGenerator from 'uuid-token-generator'
import Usuario from "../../Modelos/Usuario/Usuario";
import { subscribeToUpdates, unsubscribeToUpdates } from "../../Messaging/Messaging";
import Token from "../../Modelos/Token/Token";

const tokgen = new TokenGenerator(512);

export const authChecker = (req: express.Request, res: express.Response, next: express.NextFunction) => {
    if (req.headers.authorization == 'undefined') {
        let originalUrl = req.originalUrl
        if (originalUrl === "/auth2/") {
            next()
        } else {
            res.status(401).json({ mpz: 'Autorización invalida!!!' })
        }
    } else if (req.headers.geolocation == 'undefined') {
        res.status(401).json({ mpz: 'Geolocalización requerida para GWPVI!' })
    } else {
        tokenValidity(req, res, next)
    }
}

export const tokenValidity = (req: express.Request, response: express.Response, next: express.NextFunction) => {    
    let { authorization } = req.headers

    tokenIdByToken(authorization)
        .then(token => {
            let ts = Date.now();
            if (token.lastAccess) {
                if ((ts - token.lastAccess) < 2592000000) {
                    updateValidity(token.idToken, ts.toString()); //escribir nuevo ts
                    next();
                } else {
                    invalidarToken(token.idToken, 'Token expirado')
                    response.status(401).json({ mpz: 'Token expirado!' });
                }
            } else {
                response.status(401).json({ mpz: 'Token invalido!' });
            }
        })
        .catch(err => {
            response.status(401).json({ mpz: 'No posee Token para consulta!' });
        })
}

export const tokenIdByToken = async (authorization) => {
    return Token
        .forge({ token: authorization })
        .fetch({ columns: ['idToken', 'lastAccess', 'invalidated'] })
        .call('toJSON')
}

const updateValidity = (idToken: string, ts: string) => {
    Token
        .where({ idToken })
        .save({ lastAccess: ts }, { patch: true })
        .call('toJSON')
        .then(user => { })
        .catch(err => {
            console.log(`Error al actualizar la validez ${idToken}`, err, '\n');
        })
}

const invalidarToken = (idToken, razon) => {
    let ts = Date.now()
    Token
        .where({ idToken })
        .save({ reason: razon, invalidated: ts }, { patch: true })
        .call('toJSON')
        .then(user => { })
        .catch(err => {
            console.log(`Error al invalidar token ${idToken}`, err, '\n');
        })
}

export const autenticarUsuario = (request: express.Request, response: express.Response) => {
    let { user, password } = request.body;
    
    axios.default
        .post("http://172.19.0.60:2300/Auth_API.asmx/Autenticar", `x_user=${user}&x_password=${password}`)
        .then(res => {
            usuarioActivo(user, response)
        })
        .catch(err => {
            response.status(500).json({ mpz: 'Credenciales invalidas!' });
        });
}

const usuarioActivo = (user, response: express.Response) => {
    Usuario
        .forge({ user })
        .fetch({ columns: ['userId', 'permissions', 'active'] })
        .call('toJSON')
        .then(_user => {
            _user.active ? crearToken(_user, response) : response.status(500).json({ mpz: 'Usuario sin acceso' });
        })
        .catch((err) => {
            console.log(err);

            response.status(500).json({ mpz: 'Error al consultar usuario' });
        })
}

const crearToken = (user, response) => {
    let tokenAcceso = tokgen.generate();
    let ts = Date.now();

    Token
        .forge()
        .save({ userId: user.userId, token: tokenAcceso, ts: ts, lastAccess: ts })
        .call('toJSON')
        .then(() => {
            response.json({ token: tokenAcceso, permisos: JSON.parse(user.permissions), nombre: user.name })
        })
        .catch(err => {
            console.log(err);
            response.status(500).json({ mpz: 'Error al registrar token de acceso!' });
        })
}


export const logout = (request: express.Request, response: express.Response) => {
    let dev = request.headers.origin == 'http://localhost:4200'
    let { token, fcm } = request.body;

    Token
        .forge({ token })
        .fetch({
            columns: ['idToken']
        })
        .call('toJSON')
        .then(token => {
            invalidarToken(token.idToken, 'Logout')
        })
        .catch(err => {
            response.status(401).json({ mpz: 'No posee Token para consulta!' });
        })

    removerFCM(fcm, dev)
    response.json(null)
}

const removerFCM = (fcmToken, dev) => {
    unsubscribeToUpdates(fcmToken, dev)
}


export const registarFCM = async (request: express.Request, response: express.Response) => {
    let dev = request.headers.origin == 'http://localhost:4200'
    let { authorization } = request.headers
    let { fcmToken } = request.body;

    await Token
        .where({ token: authorization })
        .save({ fcmToken: fcmToken }, { patch: true })

    let usuario = await Token
        .where({ token: authorization })
        .fetch({ withRelated: ['user'] })
        .call('toJSON')
        
    response.json(usuario.user.permissions)
    subscribeToUpdates(fcmToken, dev)
}


export const nuevoFCM = async (request: express.Request, response: express.Response) => {
    let { oldFcmToken, newFcmToken } = request.body;
    let dev = request.headers.origin == 'http://localhost:4200'
    let { authorization } = request.headers

    await Token
        .where({ token: authorization })
        .save({ fcmToken: newFcmToken }, { patch: true })

    response.json(newFcmToken)
    subscribeToUpdates(newFcmToken, dev)
    unsubscribeToUpdates(oldFcmToken, dev)
}

