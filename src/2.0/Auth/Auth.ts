import * as express from "express";
import { autenticarUsuario, logout, registarFCM, nuevoFCM } from "./Auth.functions";
const authRouter2 = express.Router();

authRouter2.post("/", autenticarUsuario);

authRouter2.post("/logout", logout);

authRouter2.post("/fcm", registarFCM);

authRouter2.post("/newfcm", nuevoFCM);


export { authRouter2 as auth2 };
