import * as express from "express";

import { actividadUsuario } from "../Functions/Functions.common";
import Riesgos from "../../Modelos/Riesgos/Riesgos";
import Riesgo from "../../Modelos/Riesgo/Riesgo";
import { proyectoActualizado } from "../Proyectos/Proyectos.functions";
import { encryptByToken } from "../../Security/Security";

export const insertarRiesgo = async (request: express.Request, response: express.Response) => {
    let { geolocation, authorization } = request.headers;
    let { riesgo } = request.body;
    let ts = Date.now();
    try {
        let nuevoRiesgo = await Riesgos
            .forge()
            .save({ ...riesgo })
        response.json({ id: nuevoRiesgo.id })
        actividadUsuario(0, authorization, { ts: ts, tipo: 'Riesgo creado', geolocation: geolocation, data: JSON.stringify(riesgo) })
    } catch (err) {
        console.log(`Error al insetar riesgo ${riesgo}`, ts, err);
        response.status(500).json({ mpz: 'Error al insetar riesgo en MPZ! :(' })
    }
}

export const obtenerRiesgos = async (request: express.Request, response: express.Response) => {
    let { authorization } = request.headers;

    let ts = Date.now();
    try {
        let riesgos = await Riesgos
            .fetchAll()
        const encryptedriesgos = encryptByToken(riesgos, authorization);

        response.json({ riesgos: encryptedriesgos })
    } catch (err) {
        console.log(`Error al obtener resgos`, ts, err);
        response.status(500).json({ mpz: 'Error al obtener riesgos desde MPZ! :(' })
    }
}

export const agregarRiesgo = async (request: express.Request, response: express.Response) => {
    let { geolocation, authorization } = request.headers;
    let { riesgo, proId } = request.body;
    riesgo = { ...riesgo, proId: proId, idRiesgo: riesgo.riesgoId }

    delete riesgo.riesgoId
    let ts = Date.now()
    try {
        let nuevoRiesgo = await Riesgo
            .forge()
            .save({ ...riesgo })
            .call('toJSON')

        proyectoActualizado(proId, ts)
        actividadUsuario(proId, authorization, { ts: ts, tipo: 'Riesgo agregado', geolocation: geolocation, data: JSON.stringify(riesgo) })
        response.json({ riesgoId: nuevoRiesgo.id, ts: ts })
    }
    catch (err) {
        console.log(`Error al agregar riesgo ${proId}, ${riesgo},${ts}`, err, '\n');
        response.status(500).json({ mpz: 'Error al actualizar los riesgos en MPZ! :(' })
    }
}

export const actualizarRiesgo = async (request: express.Request, response: express.Response) => {
    let { geolocation, authorization } = request.headers;
    let { riesgo } = request.body;
    let ts = Date.now()
    try {
        let nuevoRiesgo = await Riesgo
            .where({ riesgoId: riesgo.riesgoId })
            .save(riesgo, { patch: true })

        proyectoActualizado(riesgo.proId, ts)
        actividadUsuario(riesgo.proId, authorization, { ts: ts, tipo: 'Riesgo actualizado', geolocation: geolocation, data: JSON.stringify(riesgo) })
        response.json({ ts: ts })
    }
    catch (err) {
        console.log(`Error al agregar riesgo ${riesgo.proId}, ${riesgo},${ts}`, err, '\n');
        response.status(500).json({ mpz: 'Error al actualizar riesgo en MPZ! :(' })
    }
}

export const eliminararRiesgo = async (request: express.Request, response: express.Response) => {
    let { geolocation, authorization } = request.headers;
    let { riesgo } = request.body;

    let ts = Date.now()
    try {
        let nuevoRiesgo = await Riesgo
            .where({ riesgoId: riesgo.riesgoId })
            .destroy()

        proyectoActualizado(riesgo.proId, ts)
        actividadUsuario(riesgo.proId, authorization, { ts: ts, tipo: 'Riesgo eliminado', geolocation: geolocation, data: JSON.stringify(riesgo) })
        response.json({ ts: ts })
    }
    catch (err) {
        console.log(`Error al agregar riesgo ${riesgo.proId}, ${riesgo},${ts}`, err, '\n');
        response.status(500).json({ mpz: 'Error al eliminar riesgo en MPZ! :(' })
    }
}