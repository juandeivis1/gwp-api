import * as express from "express";
import { insertarRiesgo, obtenerRiesgos, agregarRiesgo, eliminararRiesgo, actualizarRiesgo } from "./Riesgos.functions";
let riesgosRouter2 = express.Router();

riesgosRouter2.post("/insertar", insertarRiesgo)
riesgosRouter2.get("/obtener", obtenerRiesgos)
riesgosRouter2.post("/actualizar", actualizarRiesgo)
riesgosRouter2.post("/agregar", agregarRiesgo)
riesgosRouter2.post("/eliminar", eliminararRiesgo)


export { riesgosRouter2 as riesgos2 };
