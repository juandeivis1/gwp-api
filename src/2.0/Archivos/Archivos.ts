import * as express from "express";
import { subirArchivo, enviarLink } from "./Archivos.functions";
let archivosRouter2 = express.Router();

archivosRouter2.post('/subir', subirArchivo)
archivosRouter2.post('/enviar', enviarLink)

export { archivosRouter2 as archivos2 };