import * as express from "express";
import { enviarArchivo } from "../../Messaging/Messaging";
import Archivos from "../../Modelos/Archivos/Archivos";
import { proyectoActualizado } from "../Proyectos/Proyectos.functions";
import { obtenerTokens } from "../Functions/Functions.common";

export const subirArchivo = async (req: express.Request, resp: express.Response) => {
    const { archivo } = req.body;
    let ts = Date.now()

    archivo.fileId ? actualizar(archivo, ts, resp) : insertar(archivo, ts, resp)
}

const actualizar = async (archivo, ts, resp) => {
    delete archivo.fileId;
    try {
        await Archivos
            .where({ fileId: archivo.fileId })
            .save(archivo, { patch: true })
            .call('toJSON')
        resp.json({ ts: ts, update: true })
        proyectoActualizado(archivo.proId, ts)
    } catch (err) {
        resp.status(500).json({ mpz: 'Error al actualizar el archivo en MPZ! :(' })
        console.log('Error al actualizar el archivo en MPZ! :(', err);
    }
}

const insertar = async (archivo, ts, resp) => {
    try {
        let nuevoArchivo = await Archivos
            .forge()
            .save(archivo)
            .call('toJSON')
        resp.json({ ts: ts, fileId: nuevoArchivo.id, update: false })
        proyectoActualizado(archivo.proId, ts)
    } catch (err) {
        resp.status(500).json({ mpz: 'Error al insertar el archivo en MPZ! :(' })
        console.log('Error al insertar el archivo en MPZ! :(', err);
    }
}

export const enviarLink = async (req: express.Request, resp: express.Response) => {
    const { users, user, fase, url } = req.body;
    
    let tokens = await obtenerTokens(users)

    let respuesta = await enviarArchivo(tokens, user, fase, url)
    resp.send(respuesta)
}
