import * as express from "express";
import { proyectoActualizado } from "../Proyectos/Proyectos.functions";
import Adquisicion from "../../Modelos/Adquisicion/Adquisicion";
import { actividadUsuario } from "../Functions/Functions.common";


export const nuevaAdquisicion = (request: express.Request, response: express.Response) => {
    let { proId, adquisicion } = request.body;
    let ts = Date.now()

    existeAdquisicion(adquisicion, proId, ts, request, response)
}


const existeAdquisicion = async (adquisicion, proId, ts, request, response) => {
    let { geolocation, authorization } = request.headers;

    let existe = await Adquisicion.forge({ proId }).fetch()

    existe && existe.toJSON() ?
        actualizar(adquisicion, proId, ts, geolocation, authorization, response)
        : insertar(adquisicion, proId, ts, geolocation, authorization, response)
}

const actualizar = async (adquisicion, proId: number, ts: number, geolocation, authorization, response: express.Response) => {
    delete adquisicion.proId
    
    try {
        await Adquisicion
            .where({ proId })
            .save(adquisicion, { patch: true })
            .call('toJSON')

        response.json({ ts: ts });
        proyectoActualizado(proId, ts)
        actividadUsuario(proId, authorization, { ts: ts, tipo: 'Nuevas adquisiciones', geolocation: geolocation, data: JSON.stringify(adquisicion) })
    } catch (err) {
        console.log(`Error al actualizar las adquisiciones ${proId},${adquisicion},${ts}`, err, '\n');
        response.status(500).json({ mpz: 'Error al actualizar las adquisiciones en MPZ! :(' })
    }

}

const insertar = async (adquisicion, proId: number, ts: number, geolocation, authorization, response: express.Response) => {
    let newAdquisicion = { ...adquisicion, proId: proId }
    try {
        let nuevaAdquisicion = await Adquisicion
            .forge()
            .save(newAdquisicion)
            .call('toJSON')

        response.json({ ts: ts });
        proyectoActualizado(proId, ts)
        actividadUsuario(proId, authorization, { ts: ts, tipo: 'Creación adquisiciones', geolocation: geolocation, data: JSON.stringify(adquisicion) })
    } catch (err) {
        console.log(`'Error al insertar las adquisiciones ${proId},${adquisicion},${ts}`, err, '\n');
        response.status(500).json({ mpz: 'Error al insertar las adquisiciones en MPZ! :(' })
    }

}
