import { bookshelf } from "../../Database/Database";

export default class Accion extends bookshelf.Model<Accion> {

    get tableName() { return 'gv_action'; }

}
