import { bookshelf } from "../../Database/Database";

export default class Mensajes extends bookshelf.Model<Mensajes> {

    get tableName() { return 'gv_pro_mensaje'; }

}