import { bookshelf } from "../../Database/Database";

export default class Riesgos extends bookshelf.Model<Riesgos> {

    get tableName() { return 'gv_riesgos'; }

}