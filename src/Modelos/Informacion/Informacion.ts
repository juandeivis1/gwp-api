import { bookshelf } from "../../Database/Database";

export default class Informacion extends bookshelf.Model<Informacion> {

    get tableName() { return 'gv_pro_informacion'; }

}
