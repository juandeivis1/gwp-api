import { bookshelf } from "../../Database/Database";

export default class Avance extends bookshelf.Model<Avance> {
    
    get tableName() { return 'gv_pro_avance'; }
    get idAttribute() { return 'proId' }

}
