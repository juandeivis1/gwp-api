import { bookshelf } from "../../Database/Database";

export class Parametros extends bookshelf.Model<Parametros> {
    
    get tableName() { return 'gv_parameters'; }

}