import { bookshelf } from "../../Database/Database";

export default class Caminos extends bookshelf.Model<Caminos> {

    get tableName() { return 'gv_caminos'; }

}