import { bookshelf } from "../../Database/Database";


export default class Archivos extends bookshelf.Model<Archivos> {
    
    get tableName() { return 'gv_pro_archivos'; }

}