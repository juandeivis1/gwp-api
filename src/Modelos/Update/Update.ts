import { bookshelf } from "../../Database/Database";

export default class Update extends bookshelf.Model<Update> {

    get tableName() { return 'gv_updates'; }

}