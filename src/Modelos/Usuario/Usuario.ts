import { bookshelf } from "../../Database/Database";
import Token from "../Token/Token";

export default class Usuario extends bookshelf.Model<Usuario> {

    
    get tableName() { return 'gv_users'; }
    get idAttribute() { return 'userId' }

    tokens = () => {
        return this.hasMany(Token, 'userId')
    }

}