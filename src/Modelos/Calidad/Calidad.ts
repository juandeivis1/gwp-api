import { bookshelf } from "../../Database/Database";

export default class Calidad extends bookshelf.Model<Calidad> {

    get tableName() { return 'gv_pro_calidad'; }

}