import { bookshelf } from "../../Database/Database";

export class Bitacora extends bookshelf.Model<Bitacora> {

    get tableName() { return 'gv_pro_bitacoras'; }

}
