import { bookshelf } from "../../Database/Database";

export default class Riesgo extends bookshelf.Model<Riesgo> {

    get tableName() { return 'gv_pro_riesgo'; }

}