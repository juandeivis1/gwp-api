import { bookshelf } from "../../Database/Database";

export default class Barrio extends bookshelf.Model<Barrio> {

    get tableName() { return 'gv_neighborhood'; }

}
