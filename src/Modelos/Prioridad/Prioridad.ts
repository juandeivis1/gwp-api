import { bookshelf } from "../../Database/Database";

export class Prioridad extends bookshelf.Model<Prioridad> {
    
    get tableName() { return 'gv_priority'; }

}