import { bookshelf } from "../../Database/Database";

export class Permisos extends bookshelf.Model<Permisos> {
    
    get tableName() { return 'gv_permission'; }

}