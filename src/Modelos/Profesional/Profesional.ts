import { bookshelf } from "../../Database/Database";

export class Profesional extends bookshelf.Model<Profesional> {
    
    get tableName() { return 'gv_profesional'; }

}