import { bookshelf } from "../../Database/Database";
import Usuario from "../Usuario/Usuario";

export default class Token extends bookshelf.Model<Token> {

    get tableName() { return 'gv_tokens'; }

    user = () => {
        return this.belongsTo(Usuario, 'userId');
    } 

}