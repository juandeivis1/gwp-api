import { bookshelf } from "../../Database/Database";

export default class Actividad extends bookshelf.Model<Actividad> {

    get tableName() { return 'gv_users_activity'; }

}
