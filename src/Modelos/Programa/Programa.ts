import { bookshelf } from "../../Database/Database";

export class Programa extends bookshelf.Model<Programa> {
    
    get tableName() { return 'gv_program'; }

}