import { bookshelf } from "../../Database/Database";

export default class Ciudad extends bookshelf.Model<Ciudad> {

    get tableName() { return 'gv_city'; }

}

const insertarCiudades = (pueblos: string[]) => {
    pueblos.map(ciudad => {

        Ciudad
            .forge()
            .save({ name: ciudad, district: 11 })
            .then(res => {
                console.log(res);

            })
            .catch(err => {
                console.log(err);

            })
    })
}
