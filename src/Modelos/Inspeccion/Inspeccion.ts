import { bookshelf } from "../../Database/Database";

export default class Inspeccion extends bookshelf.Model<Inspeccion> {
    
    get tableName() { return 'gv_pro_inspeccion'; }

}

