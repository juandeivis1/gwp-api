import { bookshelf } from "../../Database/Database";

import Adquisicion from "../Adquisicion/Adquisicion";
import Archivos from "../Archivos/Archivos";
import Avance from "../Avance/Avance";
import Calidad from "../Calidad/Calidad";
import Costos from "../Costos/Costos";
import Informacion from "../Informacion/Informacion";
import Riesgo from "../Riesgo/Riesgo";
import Seguimiento from "../Seguimiento/Seguimiento";
import Mensajes from "../Mensajes/Mensajes";
import { Bitacora } from "../Bitacora/Bitacora";
import { Pendiente } from "../Pendiente/Pendiente";
import { Completar } from "../Completar/Completar";
import Inspeccion from "../Inspeccion/Inspeccion";


export default class Proyecto extends bookshelf.Model<Proyecto> {

    get tableName() { return 'gv_pro'; }
    get idAttribute() { return 'proId' }

    static get relationships() {
        return [
            'adquisicion',
            'archivos',
            'avance',
            'bitacora',
            'pendiente',
            'calidad',
            'costos',
            'informacion',
            'mensajes',
            'seguimiento',
            'completar',
            'riesgos',
            'inspecciones'
        ];
    }

    static get columns() {
        return [
            'actualizado',
            'creado',
            'expediente',
            'nombre',
            'portafolio',
            'prioridad',
            'proId',
            'programa',
            'sinonimo'
        ];
    }

    adquisicion = () => {
        return this.hasOne(Adquisicion, 'proId')
    }

    archivos = () => {
        return this.hasMany(Archivos, 'proId')
    }

    avance = () => {
        return this.hasOne(Avance, 'proId')
    }

    bitacora = () => {
        return this.hasMany(Bitacora, 'proId')
    }

    recientes = () => {
        return this.hasMany(Bitacora, 'proId').query( qb => qb.orderBy('ts', 'asc').limit(2))
    }

    pendiente = () => {
        return this.hasMany(Pendiente, 'proId')
    }

    pendientes = () => {
        return this.hasMany(Pendiente, 'proId').query(qb => qb.whereNull('resuelto'))
    }

    calidad = () => {
        return this.hasOne(Calidad, 'proId')
    }

    completar = () => {
        return this.hasMany(Completar, 'proId')
    }

    costos = () => {
        return this.hasOne(Costos, 'proId')
    }

    informacion = () => {
        return this.hasOne(Informacion, 'proId')
    }

    mensajes = () => {
        return this.hasMany(Mensajes, 'proId').query(q => q.orderBy('ts', 'asc'))
    }

    seguimiento = () => {
        return this.hasOne(Seguimiento, 'proId')
    }

    riesgos = () => {
        return this.hasMany(Riesgo, 'proId')
    }

    inspecciones = () => {
        return this.hasMany(Inspeccion, 'proId')
    }

}