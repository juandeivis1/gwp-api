import { bookshelf } from "../../Database/Database";

export default class Distrito extends bookshelf.Model<Distrito> {

    get tableName() { return 'gv_district'; }

}