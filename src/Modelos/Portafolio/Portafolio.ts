import { bookshelf } from "../../Database/Database";

export class Portafolio extends bookshelf.Model<Portafolio> {
    
    get tableName() { return 'gv_portfolio'; }

}