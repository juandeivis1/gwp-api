import { bookshelf } from "../../Database/Database";

export class Pendiente extends bookshelf.Model<Pendiente> {
    
    get tableName() { return 'gv_pro_pendientes'; }

}