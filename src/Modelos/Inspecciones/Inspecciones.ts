import { bookshelf } from "../../Database/Database";

export default class Inspecciones extends bookshelf.Model<Inspecciones> {
    
    get tableName() { return 'gv_inspecciones'; }

}

