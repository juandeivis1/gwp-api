import { bookshelf } from "../../Database/Database";

export default class Adquisicion extends bookshelf.Model<Adquisicion> {

    get tableName() { return 'gv_pro_adquisicion'; }

}
