import { bookshelf } from "../../Database/Database";

export default class Fase extends bookshelf.Model<Fase> {

    get tableName() { return 'gv_phase'; }

}