import express from 'express'
import * as bodyParser from "body-parser";
import knex from "knex";

import { parametros } from './Parametros/Parametros';
import { dev } from './Dev/Dev';

import { adquisicion2 } from './2.0/Adquisicion/Adquisicion';
import { avance2 } from './2.0/Avance/Avance';
import { proyectos2 } from './2.0/Proyectos/Proyectos';
import { expressOptions, originValidation } from './Security/Security';
import { auth2 } from './2.0/Auth/Auth';
import { authChecker } from './2.0/Auth/Auth.functions';
import { seguimiento2 } from './2.0/Seguimiento/Seguimiento';
import { costos2 } from './2.0/Costos/Costos';
import { calidad2 } from './2.0/Calidad/Calidad';
import { informacion2 } from './2.0/Informacion/Informacion';
import { archivos2 } from './2.0/Archivos/Archivos';
import { mensajes2 } from './2.0/Mensajes/Mensajes';
import { reportes2 } from './2.0/Reportes/Reportes';
import { inspecciones2 } from './2.0/Inspecciones/Inspecciones';
import { riesgos2 } from './2.0/Riesgos/Riesgos';

const PORT = process.env.PORT || 5100

export const app = express();

import * as admin from "firebase-admin";
import { mapa } from './2.0/Mapa/Mapa';
const credentials = require('./Messaging/gv.json')

admin.initializeApp({
  credential: admin.credential.cert(credentials)
});

app.use(bodyParser.json());
app.use(expressOptions);
app.use(originValidation)
app.use(authChecker)

app.use("/auth2", auth2);
app.use("/adquisicion2", adquisicion2);
app.use("/archivo2", archivos2);
app.use("/avance2", avance2);
app.use("/calidad2", calidad2);
app.use("/costos2", costos2);
app.use("/informacion2", informacion2);
app.use("/mensajes2", mensajes2);
app.use("/proyectos2", proyectos2);
app.use("/seguimiento2", seguimiento2);
app.use("/riesgos2", riesgos2);
app.use("/reporte2", reportes2);
app.use("/inspecciones2", inspecciones2);

app.use("/parameters", parametros);
app.use("/map", mapa);
app.use("/dev", dev);

app.listen(PORT, () => {
  console.log(`GV Server ready on port: ${PORT}`);
})