export interface Riesgo {
    fase: string;
    categoria: string;
    subcategoria: string;
    causa: string;
    consecuencia: string;
    actividad: string;
    id?: number;
    prob?: number;
    imp?: number;
    indice?: number;
    respuesta?: string;
    completado?: boolean;
  }
  