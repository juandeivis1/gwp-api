export interface Pendiente {
  asunto: string;
  generado?: number;
  resuelto?: number;
}
