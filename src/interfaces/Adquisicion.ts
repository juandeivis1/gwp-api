export interface Adquisicion{
    abs?: string;
    mopt?: string;
    cne?: string;
    ordenCompra?: string;
    otras?: string;
    contratoCalidad?: string;
    ordenCalidad?: string;
    contratacion?: string;
    refrendo?: number;
 }