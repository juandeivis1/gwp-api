import { Pendiente } from "./Pendiente";
import { Bitacora } from "./Bitacora";


export interface Avance {
    porcentaje?: number;
    inicio?: number;
    fin?: number;
    pendientes?: Pendiente[];
    bitacora?: Bitacora[];
}