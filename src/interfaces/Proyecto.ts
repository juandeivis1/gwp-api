export interface ProyectoInterface {
    proId?: number;
    nombre: string;
    sinonimo: string;
    portafolio: string;
    prioridad: number;
    programa: string;
    expediente: string;
    creado?: number;
    actualizado?: number;
  }