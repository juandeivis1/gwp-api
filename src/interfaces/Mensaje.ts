export interface Mensaje {
    usuario: string;
    mensaje: string;
    ts: number;
}