import * as CryptoJS from 'crypto-js';

export const expressOptions = (req, res, next) => {
  
    // set origin for the desire page
    res.setHeader("Access-Control-Allow-Origin", "https://www.perezzeledon.go.cr:803");
    // res.setHeader("Access-Control-Allow-Origin", "http://localhost:4200");
  
    // Request methods you wish to allow
    // "GET, POST, OPTIONS, PUT, PATCH, DELETE"
    // In this case only is allowd POST and GET
    res.setHeader("Access-Control-Allow-Methods", "GET, POST");
    
    // Request headers allowed in MPZ to control all the logic
    res.setHeader("Access-Control-Allow-Headers", "X-Requested-With,content-type,Authorization,Geolocation");
    
    res.setHeader("X-Powered-By", "Jarvis-Dev");
    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    
    // res.setHeader("Access-Control-Allow-Credentials", 'true');      
    if (req.method == 'OPTIONS') {
      res.sendStatus(200);
    } else {
      next();
    }
  };

  export const originValidation = (req, res,next) => {
    if (req.headers.origin){
      let origin =  req.headers.origin;
      if (origin == 'https://www.perezzeledon.go.cr:803' || origin == 'http://localhost:4200' || origin == 'https://perezzeledon.go.cr:803') {
        next()    
      } else {
        res.send('<h1 style="font-family: monospace;color: cornflowerblue;margin-top: 45%;text-align: center;">Desde tener un token autorizado para estar en esta sección</h1>')
      }
    } else {
      res.send('<h1 style="font-family: monospace;color: cornflowerblue;margin-top: 45%;text-align: center;">Sección de información con acceso, solicite un token a informatica</h1>')
    }
  }

  export const encryptByToken = (data, token) => {
    return CryptoJS.AES.encrypt(JSON.stringify(data), token).toString()
  }