import * as admin from "firebase-admin";
import { obtenerTokensEquipo } from "../2.0/Functions/Functions.common";

export const sendNewMessageTo = async (tittle: string, body: string, device: string[], mensaje, proId) => {

  var payload = {
    notification: {
      title: tittle,
      body: body,
      icon: "/assets/icons/gv-icon-512x512.png",
      click_action: `https://perezzeledon.go.cr:803/gestion/proyectos/actual/${proId}/mensajeria`,
      data: JSON.stringify(mensaje)
    }
  };

  return await admin
    .messaging()
    .sendToDevice(device, payload)
}

export const sendTo = async (tittle: string, body: string, device: string[], proId) => {

  var payload = {
    notification: {
      title: tittle,
      body: body,
      icon: "/assets/icons/gv-icon-512x512.png",
      click_action: `https://perezzeledon.go.cr:803/gestion/proyectos/actual/${proId}/avance`
    }
  };

  return await admin
    .messaging()
    .sendToDevice(device, payload)
}

export const subscribeToUpdates = async (fcmToken: string, dev) => {
  let entorno = dev ? 'dev' : 'update'
  await admin
    .messaging()
    .subscribeToTopic(fcmToken, entorno)
}

export const unsubscribeToUpdates = async (fcmToken: string, dev) => {
  let entorno = dev ? 'dev' : 'update'
  await admin
    .messaging()
    .unsubscribeFromTopic(fcmToken, entorno)
}

export const sendUpdates = (payload: admin.messaging.MessagingPayload, dev) => {
  let entorno = dev ? 'dev' : 'update'

  admin
    .messaging()
    .sendToTopic(entorno, payload)
    .then(res => console.log(res))
    .catch(err => console.log(err))
}

export const enviarNuevoProyecto = (sinonimo, expediente, proId, dev) => {
  let entorno = dev ? `http://localhost:4200/gestion/proyectos/actual/${proId}` : `https://perezzeledon.go.cr:803/gestion/proyectos/actual/${proId}`
  sendUpdates({
    notification: {
      body: sinonimo,
      title: `Nueva bitácora ${expediente}`,
      icon: "/assets/icons/gv-icon-512x512.png",
      click_action: entorno
    }, data: { proId: proId.toString() }
  }, dev)
}

export const enviarNuevoAvance = (sinonimo, expediente, proId, ts, dev) => {
  let entorno = dev ? `http://localhost:4200/gestion/proyectos/actual/${proId}/avance` : `https://perezzeledon.go.cr:803/gestion/proyectos/actual/${proId}/avance`

  sendUpdates({
    notification: {
      body: sinonimo,
      title: `Nueva bitácora ${expediente}`,
      icon: "/assets/icons/gv-icon-512x512.png",
      click_action: entorno,
    }, data: { proId: proId.toString(), ts: ts.toString() }
  }, dev)
}

export const enviarNuevaBitacora = async (body: string, expediente: string, proId: number) => {
  let tokens = await obtenerTokensEquipo(proId);
  
  tokens.length > 0 ?
    await sendTo(`Nueva bitácora ${expediente}`, body, tokens, proId)
    : null;
}

export const enviarArchivo = (tokens: string[], usuario: string, fase: string, link: string) => {
  return admin.messaging()
    .sendToDevice(tokens, {
      notification: {
        title: fase,
        body: `${usuario.toUpperCase()} ha enviado un archivo`,
        clickAction: link,
        icon: "/assets/icons/gv-icon-512x512.png",
      }
    })
}
