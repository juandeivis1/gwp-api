import * as express from 'express';
import { checkTeam, nuevoCamino, eliminarCaminos, distritos, ciudades, comunidades, barrios, portafolios, programas, roles, fases, profesionales, acciones, cumplir, prioridades, permisos, addCity, addCommunity, addNeighborhood, addComply, updateCity, updateCommunity, updateNeighborhood, addPortfolio, addProgram, addProfesional } from './Parametros.functions';

const parametrosRouter = express.Router();

parametrosRouter.get('/team', checkTeam);
parametrosRouter.post('/camino', nuevoCamino);
parametrosRouter.get('/borrarCaminos', eliminarCaminos);
parametrosRouter.post('/district', distritos);
parametrosRouter.post('/city', ciudades);
parametrosRouter.post('/community', comunidades);
parametrosRouter.post('/neighborhood', barrios);
parametrosRouter.post('/portfolio', portafolios);
parametrosRouter.post('/programs', programas);
parametrosRouter.post('/roles', roles);
parametrosRouter.post('/phase', fases);
parametrosRouter.post('/profesionals', profesionales);
parametrosRouter.post('/actions', acciones);
parametrosRouter.post('/comply', cumplir);
parametrosRouter.post('/priority', prioridades);
parametrosRouter.post('/permission', permisos);
parametrosRouter.post('/addcity', addCity);
parametrosRouter.post('/addcommunity', addCommunity);
parametrosRouter.post('/addneighborhood', addNeighborhood);
parametrosRouter.post('/addcomply', addComply);
parametrosRouter.post('/addportfolio', addPortfolio);
parametrosRouter.post('/addprogram', addProgram);
parametrosRouter.post('/addprofesional', addProfesional);
// Editar
parametrosRouter.post('/updatecity', updateCity);
parametrosRouter.post('/updatecommunity', updateCommunity);
parametrosRouter.post('/updateneighborhood', updateNeighborhood);

export { parametrosRouter as parametros }