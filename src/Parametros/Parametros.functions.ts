import Usuario from '../Modelos/Usuario/Usuario';
import { knexGV } from '../Database/Database';
import Caminos from '../Modelos/Caminos/Caminos';
import Comunidad from '../Modelos/Comunidad/Comunidad';
import Distrito from '../Modelos/Distrito/Distrito';
import Ciudad from '../Modelos/Ciudad/Ciudad';
import Update from '../Modelos/Update/Update';
import Barrio from '../Modelos/Barrio/Barrio';
import { Portafolio } from '../Modelos/Portafolio/Portafolio';
import { Programa } from '../Modelos/Programa/Programa';
import { RolProyecto } from '../Modelos/RolProyecto/RolProyecto';
import Fase from '../Modelos/Fase/Fase';
import { Profesional } from '../Modelos/Profesional/Profesional';
import Accion from '../Modelos/Accion/Accion';
import Cumplir from '../Modelos/Cumplir/Cumplir';
import { Prioridad } from '../Modelos/Prioridad/Prioridad';
import { Permisos } from '../Modelos/Permisos/Permisos';

export const eliminarCaminos = (req, resp) => {

    knexGV('gv_caminos')
        .truncate()
        .then(() => {
            resp.json({ res: 'success' })
        })
        .catch(err => {
            console.log('Algo salio mal al eliminar los caminos \n', err);
            resp.status(500).json({ mpz: 'Error al eliminar los caminos en MPZ! :(' })
        })
}

export const checkTeam = async (req, resp) => {

    try {
        let equipo = await Usuario.query(qb => qb.select('user').where('team', true).orderBy('user', 'asc')).fetchAll().call('toJSON')
        equipo = equipo.map(user => user.user);
        resp.json(equipo)
    }
    catch (err) {
        resp.send({ MPZ: "Error al intentar obtener el equipo :(" })
        console.log(err)
    }
}

export const nuevoCamino = (req, resp) => {
    const { camino } = req.body
    camino.coordenadas = JSON.stringify(camino.coordenadas)
    Caminos
        .forge()
        .save(camino)
        .then(() => {
            resp.json({ res: 'success' })
        })
        .catch(err => {
            console.log('Algo salio mal al actualizar el camino \n', err);
            resp.status(500).json({ mpz: 'Error al actualizar camino en MPZ! :(' })
        })
}

export const distritos = async (req, resp) => {
    const { update } = req.body;
    try {
        if (await paramUpdate('district', update)) {
            const distritoList = await Distrito
                .fetchAll()
                .call('toJSON')
            resp.json(distritoList);
        } else {
            resp.json(null)
        }
    }
    catch (err) {
        resp.send({ MPZ: "Error al intentar obtener los distritos :(" });
        console.log(err);
    }
}

export const comunidades = async (req, resp) => {
    const { update } = req.body;
    try {
        if (await paramUpdate('community', update)) {
            const comunidadList = await Comunidad
                .fetchAll()
                .call('toJSON')
            resp.json(comunidadList);
        } else {
            resp.json(null)
        }
    }
    catch (err) {
        resp.send({ MPZ: "Error al intentar obtener las comunidades :(" });
        console.log(err);
    }
}

export const ciudades = async (req, resp) => {
    const { update } = req.body;
    try {
        if (await paramUpdate('city', update)) {
            const ciudadList = await Ciudad
                .fetchAll()
                .call('toJSON')
            resp.json(ciudadList);
        } else {
            resp.json(null)
        }
    }
    catch (err) {
        resp.send({ MPZ: "Error al intentar obtener las ciudades :(" });
        console.log(err);
    }
}

export const barrios = async (req, resp) => {
    const { update } = req.body;
    try {
        if (await paramUpdate('neighborhood', update)) {
            const barriosList = await Barrio
                .fetchAll()
                .call('toJSON')
            resp.json(barriosList);
        } else {
            resp.json(null)
        }
    }
    catch (err) {
        resp.send({ MPZ: "Error al intentar obtener las ciudades :(" });
        console.log(err);
    }
}

export const portafolios = async (req, resp) => {
    const { update } = req.body;
    try {
        if (await paramUpdate('portfolio', update)) {
            const portafoliosList = await Portafolio
                .fetchAll()
                .call('toJSON')
            resp.json(portafoliosList);
        } else {
            resp.json(null)
        }
    }
    catch (err) {
        resp.send({ MPZ: "Error al intentar obtener las ciudades :(" });
        console.log(err);
    }
}

export const programas = async (req, resp) => {
    const { update } = req.body;
    try {
        if (await paramUpdate('program', update)) {
            const programasList = await Programa
                .fetchAll()
                .call('toJSON')
            resp.json(programasList);
        } else {
            resp.json(null)
        }
    }
    catch (err) {
        resp.send({ MPZ: "Error al intentar obtener las ciudades :(" });
        console.log(err);
    }
}

export const roles = async (req, resp) => {
    const { update } = req.body;
    try {
        if (await paramUpdate('roles', update)) {
            const rolesList = await RolProyecto
                .fetchAll()
                .call('toJSON')
                .map(rol => rol.name)
            resp.json(rolesList);
        } else {
            resp.json(null)
        }
    }
    catch (err) {
        resp.send({ MPZ: "Error al intentar obtener las ciudades :(" });
        console.log(err);
    }
}

export const profesionales = async (req, resp) => {
    const { update } = req.body;
    try {
        if (await paramUpdate('profesional', update)) {
            const profesionalesList = await Profesional
                .fetchAll()
                .call('toJSON')
            resp.json(profesionalesList);
        } else {
            resp.json(null)
        }
    }
    catch (err) {
        resp.send({ MPZ: "Error al intentar obtener las ciudades :(" });
        console.log(err);
    }
}

export const acciones = async (req, resp) => {
    const { update } = req.body;
    try {
        if (await paramUpdate('action', update)) {
            const accionesList = await Accion
                .fetchAll()
                .call('toJSON')
                .map(accion => accion.name)
            resp.json(accionesList);
        } else {
            resp.json(null)
        }
    }
    catch (err) {
        resp.send({ MPZ: "Error al intentar obtener las ciudades :(" });
        console.log(err);
    }
}

export const fases = async (req, resp) => {
    const { update } = req.body;
    try {
        if (await paramUpdate('phase', update)) {
            const faseList = await Fase
                .fetchAll()
                .call('toJSON')
                .map(accion => accion.name)
            resp.json(faseList);
        } else {
            resp.json(null)
        }
    }
    catch (err) {
        resp.send({ MPZ: "Error al intentar obtener las ciudades :(" });
        console.log(err);
    }
}

export const cumplir = async (req, resp) => {
    const { update } = req.body;
    try {
        if (await paramUpdate('comply', update)) {
            const cumplirList = await Cumplir
                .fetchAll()
                .call('toJSON')
            resp.json(cumplirList);
        } else {
            resp.json(null)
        }
    }
    catch (err) {
        resp.send({ MPZ: "Error al intentar obtener las ciudades :(" });
        console.log(err);
    }
}

export const prioridades = async (req, resp) => {
    const { update } = req.body;
    try {
        if (await paramUpdate('priority', update)) {
            const prioridadList = await Prioridad
                .fetchAll()
                .call('toJSON')
                .map(prioridad => prioridad.priority)
            resp.json(prioridadList);
        } else {
            resp.json(null)
        }
    }
    catch (err) {
        resp.send({ MPZ: "Error al intentar obtener las ciudades :(" });
        console.log(err);
    }
}

export const permisos = async (req, resp) => {
    const { update } = req.body;
    try {
        if (await paramUpdate('permission', update)) {
            const permisosList = await Permisos
                .fetchAll()
                .call('toJSON')
                .map(permiso => permiso.name)
            resp.json(permisosList);
        } else {
            resp.json(null)
        }
    }
    catch (err) {
        resp.send({ MPZ: "Error al intentar obtener las ciudades :(" });
        console.log(err);
    }
}

export const addCity = async (req, resp) => {
    const { ciudad } = req.body;
    try {
        const _ciudad = await Ciudad
            .forge()
            .save(ciudad)
            .call('toJSON')
        resp.status(201).json(_ciudad);
    } catch (err) {
        resp.status(500).send('Error on insert the city!!')
    }
    newParamTs('city');
}

export const addCommunity = async (req, resp) => {
    const { comunidad } = req.body;
    try {
        const _comunidad = await Comunidad
            .forge()
            .save(comunidad)
            .call('toJSON')
        resp.status(201).json(_comunidad);
    } catch (err) {
        resp.status(500).send('Error on insert the city!!')
    }
    newParamTs('community');
}

export const addNeighborhood = async (req, resp) => {
    const { barrio } = req.body;
    try {
        const _barrio = await Barrio
            .forge()
            .save(barrio)
            .call('toJSON')
        resp.status(201).json(_barrio);
    } catch (err) {
        resp.status(500).send('Error on insert the city!!')
    }
    newParamTs('neighborhood');
}

export const addComply = async (req, resp) => {
    const { cumplir } = req.body;
    try {
        const _cumplir = await Cumplir
            .forge()
            .save({ name: cumplir })
            .call('toJSON')
        resp.status(201).json(_cumplir);
    } catch (err) {
        resp.status(500).send('Error on insert the city!!')
    }
    newParamTs('comply');
}

export const addPortfolio = async (req, resp) => {
    const { portafolio } = req.body;
    try {
        const _portafolio = await Portafolio
            .forge()
            .save({ name: portafolio })
            .call('toJSON')
        resp.status(201).json(_portafolio);
    } catch (err) {
        resp.status(500).send('Error on insert the city!!')
    }
    newParamTs('portfolio');
}

export const addProgram = async (req, resp) => {
    const { programa } = req.body;
    try {
        const _programa = await Programa
            .forge()
            .save({ name: programa })
            .call('toJSON')
        resp.status(201).json(_programa);
    } catch (err) {
        resp.status(500).send('Error on insert the city!!')
    }
    newParamTs('program');
}

export const addProfesional = async (req, resp) => {
    const { profesional } = req.body;
    try {
        const _profesional = await Profesional
            .forge()
            .save({ name: profesional })
            .call('toJSON')
        resp.status(201).json(_profesional);
    } catch (err) {
        resp.status(500).send('Error on insert the city!!')
    }
    newParamTs('profesional');
}

export const updateCity = async (req, resp) => {
    const { id, ciudad } = req.body;
    try {
        const _ciudad = await Ciudad
            .forge()
            .where({ id })
            .save(ciudad, { patch: true })
            .call('toJSON')
        resp.status(201).json(_ciudad);
    } catch (err) {
        resp.status(500).send('Error on update the city!!')
    }
    newParamTs('city');
}

export const updateCommunity = async (req, resp) => {
    const { id, comunidad } = req.body;
    try {
        const _comunidad = await Comunidad
            .forge()
            .where({ id })
            .save(comunidad, { patch: true })
            .call('toJSON')
        resp.status(201).json(_comunidad);
    } catch (err) {
        resp.status(500).send('Error on update the city!!')
    }
    newParamTs('community');
}

export const updateNeighborhood = async (req, resp) => {
    const { id, barrio } = req.body;
    try {
        const _barrio = await Barrio
            .forge()
            .where({ id })
            .save(barrio, { patch: true })
            .call('toJSON')
        resp.status(201).json(_barrio);
    } catch (err) {
        resp.status(500).send('Error on update the city!!')
    }
    newParamTs('neighborhood');
}

const paramUpdate = async (name, update) => {
    const param = await Update
        .where({ param: name })
        .fetch()
        .call('toJSON')
    const actualizar = update < param.update
    return actualizar;
}

const newParamTs = async (param) => {
    await Update
        .where({ param })
        .save({ update: Date.now() }, { patch: true })
        .call('toJSON')
}
