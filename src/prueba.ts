
import knex from 'knex'
import bookshelf from 'bookshelf'
export const knexDev = knex({
    client: 'mysql',
    connection: {
      host : '172.19.0.26',
      port: 3306,
      user : 'root',
      password : 'root',
      database : 'bd_gv_proyectos_dev'
    }
  });

const Bookshelf = bookshelf(knexDev);

// var User = bookshelf.Model.extend({
//   tableName: 'gv_users',
//   tokens: () => {
//     return this.hasMany(Tokens, 'user');
//   }
// });

// var Tokens = bookshelf.Model.extend({
//   tableName: 'gv_tokens',
//   user: () => {
//     return this.belongsTo(User, 'user');
//   }
// });


// User.forge({userId: 1}).fetch({withRelated: ['tokens']}).then((user) =>{
//     console.log('Got user:', user.get('name'));
//   console.log(user.toJSON());
// }).catch((err) => {
//   console.error(err);
// });



// export default
class User extends Bookshelf.Model<User> {
    get tableName() { return 'gv_users'; }
    tokens = (): bookshelf.Collection<Tokens> => {
        return this.hasMany(Tokens, 'user', 'user');
    }
}

class Tokens extends Bookshelf.Model<Tokens> {
    get tableName() { return 'gv_tokens'; }
    tokens = () => {
        return this.belongsTo(User, 'user', 'user');
    }
}

class Proyecto extends Bookshelf.Model<Proyecto> {

    get tableName() { return 'gv_pro'; }
    get idAttribute () { return 'proId'}

    archivos = (): bookshelf.Collection<Archivos> => {
        return this.hasMany(Archivos, 'proId')
    }
    
    adquisicion = (): bookshelf.Model<Adquisicion> => {
        return this.hasOne(Adquisicion, 'proId')
    }
    
    avance = (): bookshelf.Model<Avance> => {
        return this.hasOne(Avance, 'proId')
    }

    calidad = (): bookshelf.Model<Calidad> => {
        return this.hasOne(Calidad, 'proId')
    }

    costos = (): bookshelf.Model<Costos> => {
        return this.hasOne(Costos, 'proId')
    }

    informacion = (): bookshelf.Model<Informacion> => {
        return this.hasOne(Informacion, 'proId')
    }

    mensajes = (): bookshelf.Collection<Mensajes> => {
        return this.hasMany(Mensajes, 'proId')
    }

    seguimiento = (): bookshelf.Model<Seguimiento> => {
        return this.hasOne(Seguimiento, 'proId')
    }

    riesgos = (): bookshelf.Collection<Riesgos> => {
        return this.hasMany(Riesgos, 'proId')
    }


}

class Adquisicion extends Bookshelf.Model<Adquisicion> {

    get tableName() { return 'gv_pro_adquisicion'; }

}

class Bitacoras extends Bookshelf.Model<Bitacoras> {

    get tableName() { return 'gv_pro_bitacoras'; }

}

class Costos extends Bookshelf.Model<Costos> {

    get tableName() { return 'gv_pro_costos'; }

}

class Seguimiento extends Bookshelf.Model<Seguimiento> {
    get tableName() { return 'gv_pro_seguimiento'; }
    get idAttribute () { return 'proId'}

    completar() {
        return this.hasMany(Completar, 'proId')
    }
}

class Completar extends Bookshelf.Model<Completar> {
    get tableName() { return 'gv_pro_completar'; }

}

class Archivos extends Bookshelf.Model<Archivos> {
    get tableName() { return 'gv_pro_archivos'; }

}

class Pendientes extends Bookshelf.Model<Pendientes> {
    get tableName() { return 'gv_pro_pendientes'; }

}

class Avance extends Bookshelf.Model<Avance> {
    get tableName() { return 'gv_pro_avance'; }
    get idAttribute () { return 'proId'}

    bitacoras () {
        return this.hasMany(Bitacoras, 'proId').sortBy('fecha', 'desc')
    }

    pendientes () {
        return this.hasMany(Pendientes, 'proId')
    }
}

class Informacion extends Bookshelf.Model<Informacion> {

    get tableName() { return 'gv_pro_informacion'; }

}

class Mensajes extends Bookshelf.Model<Mensajes> {

    get tableName() { return 'gv_pro_mensaje'; }

}

class Calidad extends Bookshelf.Model<Calidad> {

    get tableName() { return 'gv_pro_calidad'; }

}

class Riesgos extends Bookshelf.Model<Riesgos> {

    get tableName() { return 'gv_pro_riesgo'; }

}

Proyecto.collection().fetch()