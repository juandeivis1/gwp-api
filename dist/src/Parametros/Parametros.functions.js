"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Usuario_1 = __importDefault(require("../Modelos/Usuario/Usuario"));
const Database_1 = require("../Database/Database");
const Caminos_1 = __importDefault(require("../Modelos/Caminos/Caminos"));
const Comunidad_1 = __importDefault(require("../Modelos/Comunidad/Comunidad"));
const Distrito_1 = __importDefault(require("../Modelos/Distrito/Distrito"));
const Ciudad_1 = __importDefault(require("../Modelos/Ciudad/Ciudad"));
const Update_1 = __importDefault(require("../Modelos/Update/Update"));
const Barrio_1 = __importDefault(require("../Modelos/Barrio/Barrio"));
const Portafolio_1 = require("../Modelos/Portafolio/Portafolio");
const Programa_1 = require("../Modelos/Programa/Programa");
const RolProyecto_1 = require("../Modelos/RolProyecto/RolProyecto");
const Fase_1 = __importDefault(require("../Modelos/Fase/Fase"));
const Profesional_1 = require("../Modelos/Profesional/Profesional");
const Accion_1 = __importDefault(require("../Modelos/Accion/Accion"));
const Cumplir_1 = __importDefault(require("../Modelos/Cumplir/Cumplir"));
const Prioridad_1 = require("../Modelos/Prioridad/Prioridad");
const Permisos_1 = require("../Modelos/Permisos/Permisos");
exports.eliminarCaminos = (req, resp) => {
    Database_1.knexGV('gv_caminos')
        .truncate()
        .then(() => {
        resp.json({ res: 'success' });
    })
        .catch(err => {
        console.log('Algo salio mal al eliminar los caminos \n', err);
        resp.status(500).json({ mpz: 'Error al eliminar los caminos en MPZ! :(' });
    });
};
exports.checkTeam = (req, resp) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let equipo = yield Usuario_1.default.query(qb => qb.select('user').where('team', true).orderBy('user', 'asc')).fetchAll().call('toJSON');
        equipo = equipo.map(user => user.user);
        resp.json(equipo);
    }
    catch (err) {
        resp.send({ MPZ: "Error al intentar obtener el equipo :(" });
        console.log(err);
    }
});
exports.nuevoCamino = (req, resp) => {
    const { camino } = req.body;
    camino.coordenadas = JSON.stringify(camino.coordenadas);
    Caminos_1.default
        .forge()
        .save(camino)
        .then(() => {
        resp.json({ res: 'success' });
    })
        .catch(err => {
        console.log('Algo salio mal al actualizar el camino \n', err);
        resp.status(500).json({ mpz: 'Error al actualizar camino en MPZ! :(' });
    });
};
exports.distritos = (req, resp) => __awaiter(void 0, void 0, void 0, function* () {
    const { update } = req.body;
    try {
        if (yield paramUpdate('district', update)) {
            const distritoList = yield Distrito_1.default
                .fetchAll()
                .call('toJSON');
            resp.json(distritoList);
        }
        else {
            resp.json(null);
        }
    }
    catch (err) {
        resp.send({ MPZ: "Error al intentar obtener los distritos :(" });
        console.log(err);
    }
});
exports.comunidades = (req, resp) => __awaiter(void 0, void 0, void 0, function* () {
    const { update } = req.body;
    try {
        if (yield paramUpdate('community', update)) {
            const comunidadList = yield Comunidad_1.default
                .fetchAll()
                .call('toJSON');
            resp.json(comunidadList);
        }
        else {
            resp.json(null);
        }
    }
    catch (err) {
        resp.send({ MPZ: "Error al intentar obtener las comunidades :(" });
        console.log(err);
    }
});
exports.ciudades = (req, resp) => __awaiter(void 0, void 0, void 0, function* () {
    const { update } = req.body;
    try {
        if (yield paramUpdate('city', update)) {
            const ciudadList = yield Ciudad_1.default
                .fetchAll()
                .call('toJSON');
            resp.json(ciudadList);
        }
        else {
            resp.json(null);
        }
    }
    catch (err) {
        resp.send({ MPZ: "Error al intentar obtener las ciudades :(" });
        console.log(err);
    }
});
exports.barrios = (req, resp) => __awaiter(void 0, void 0, void 0, function* () {
    const { update } = req.body;
    try {
        if (yield paramUpdate('neighborhood', update)) {
            const barriosList = yield Barrio_1.default
                .fetchAll()
                .call('toJSON');
            resp.json(barriosList);
        }
        else {
            resp.json(null);
        }
    }
    catch (err) {
        resp.send({ MPZ: "Error al intentar obtener las ciudades :(" });
        console.log(err);
    }
});
exports.portafolios = (req, resp) => __awaiter(void 0, void 0, void 0, function* () {
    const { update } = req.body;
    try {
        if (yield paramUpdate('portfolio', update)) {
            const portafoliosList = yield Portafolio_1.Portafolio
                .fetchAll()
                .call('toJSON');
            resp.json(portafoliosList);
        }
        else {
            resp.json(null);
        }
    }
    catch (err) {
        resp.send({ MPZ: "Error al intentar obtener las ciudades :(" });
        console.log(err);
    }
});
exports.programas = (req, resp) => __awaiter(void 0, void 0, void 0, function* () {
    const { update } = req.body;
    try {
        if (yield paramUpdate('program', update)) {
            const programasList = yield Programa_1.Programa
                .fetchAll()
                .call('toJSON');
            resp.json(programasList);
        }
        else {
            resp.json(null);
        }
    }
    catch (err) {
        resp.send({ MPZ: "Error al intentar obtener las ciudades :(" });
        console.log(err);
    }
});
exports.roles = (req, resp) => __awaiter(void 0, void 0, void 0, function* () {
    const { update } = req.body;
    try {
        if (yield paramUpdate('roles', update)) {
            const rolesList = yield RolProyecto_1.RolProyecto
                .fetchAll()
                .call('toJSON')
                .map(rol => rol.name);
            resp.json(rolesList);
        }
        else {
            resp.json(null);
        }
    }
    catch (err) {
        resp.send({ MPZ: "Error al intentar obtener las ciudades :(" });
        console.log(err);
    }
});
exports.profesionales = (req, resp) => __awaiter(void 0, void 0, void 0, function* () {
    const { update } = req.body;
    try {
        if (yield paramUpdate('profesional', update)) {
            const profesionalesList = yield Profesional_1.Profesional
                .fetchAll()
                .call('toJSON');
            resp.json(profesionalesList);
        }
        else {
            resp.json(null);
        }
    }
    catch (err) {
        resp.send({ MPZ: "Error al intentar obtener las ciudades :(" });
        console.log(err);
    }
});
exports.acciones = (req, resp) => __awaiter(void 0, void 0, void 0, function* () {
    const { update } = req.body;
    try {
        if (yield paramUpdate('action', update)) {
            const accionesList = yield Accion_1.default
                .fetchAll()
                .call('toJSON')
                .map(accion => accion.name);
            resp.json(accionesList);
        }
        else {
            resp.json(null);
        }
    }
    catch (err) {
        resp.send({ MPZ: "Error al intentar obtener las ciudades :(" });
        console.log(err);
    }
});
exports.fases = (req, resp) => __awaiter(void 0, void 0, void 0, function* () {
    const { update } = req.body;
    try {
        if (yield paramUpdate('phase', update)) {
            const faseList = yield Fase_1.default
                .fetchAll()
                .call('toJSON')
                .map(accion => accion.name);
            resp.json(faseList);
        }
        else {
            resp.json(null);
        }
    }
    catch (err) {
        resp.send({ MPZ: "Error al intentar obtener las ciudades :(" });
        console.log(err);
    }
});
exports.cumplir = (req, resp) => __awaiter(void 0, void 0, void 0, function* () {
    const { update } = req.body;
    try {
        if (yield paramUpdate('comply', update)) {
            const cumplirList = yield Cumplir_1.default
                .fetchAll()
                .call('toJSON');
            resp.json(cumplirList);
        }
        else {
            resp.json(null);
        }
    }
    catch (err) {
        resp.send({ MPZ: "Error al intentar obtener las ciudades :(" });
        console.log(err);
    }
});
exports.prioridades = (req, resp) => __awaiter(void 0, void 0, void 0, function* () {
    const { update } = req.body;
    try {
        if (yield paramUpdate('priority', update)) {
            const prioridadList = yield Prioridad_1.Prioridad
                .fetchAll()
                .call('toJSON')
                .map(prioridad => prioridad.priority);
            resp.json(prioridadList);
        }
        else {
            resp.json(null);
        }
    }
    catch (err) {
        resp.send({ MPZ: "Error al intentar obtener las ciudades :(" });
        console.log(err);
    }
});
exports.permisos = (req, resp) => __awaiter(void 0, void 0, void 0, function* () {
    const { update } = req.body;
    try {
        if (yield paramUpdate('permission', update)) {
            const permisosList = yield Permisos_1.Permisos
                .fetchAll()
                .call('toJSON')
                .map(permiso => permiso.name);
            resp.json(permisosList);
        }
        else {
            resp.json(null);
        }
    }
    catch (err) {
        resp.send({ MPZ: "Error al intentar obtener las ciudades :(" });
        console.log(err);
    }
});
exports.addCity = (req, resp) => __awaiter(void 0, void 0, void 0, function* () {
    const { ciudad } = req.body;
    try {
        const _ciudad = yield Ciudad_1.default
            .forge()
            .save(ciudad)
            .call('toJSON');
        resp.status(201).json(_ciudad);
    }
    catch (err) {
        resp.status(500).send('Error on insert the city!!');
    }
    newParamTs('city');
});
exports.addCommunity = (req, resp) => __awaiter(void 0, void 0, void 0, function* () {
    const { comunidad } = req.body;
    try {
        const _comunidad = yield Comunidad_1.default
            .forge()
            .save(comunidad)
            .call('toJSON');
        resp.status(201).json(_comunidad);
    }
    catch (err) {
        resp.status(500).send('Error on insert the city!!');
    }
    newParamTs('community');
});
exports.addNeighborhood = (req, resp) => __awaiter(void 0, void 0, void 0, function* () {
    const { barrio } = req.body;
    try {
        const _barrio = yield Barrio_1.default
            .forge()
            .save(barrio)
            .call('toJSON');
        resp.status(201).json(_barrio);
    }
    catch (err) {
        resp.status(500).send('Error on insert the city!!');
    }
    newParamTs('neighborhood');
});
exports.addComply = (req, resp) => __awaiter(void 0, void 0, void 0, function* () {
    const { cumplir } = req.body;
    try {
        const _cumplir = yield Cumplir_1.default
            .forge()
            .save({ name: cumplir })
            .call('toJSON');
        resp.status(201).json(_cumplir);
    }
    catch (err) {
        resp.status(500).send('Error on insert the city!!');
    }
    newParamTs('comply');
});
exports.addPortfolio = (req, resp) => __awaiter(void 0, void 0, void 0, function* () {
    const { portafolio } = req.body;
    try {
        const _portafolio = yield Portafolio_1.Portafolio
            .forge()
            .save({ name: portafolio })
            .call('toJSON');
        resp.status(201).json(_portafolio);
    }
    catch (err) {
        resp.status(500).send('Error on insert the city!!');
    }
    newParamTs('portfolio');
});
exports.addProgram = (req, resp) => __awaiter(void 0, void 0, void 0, function* () {
    const { programa } = req.body;
    try {
        const _programa = yield Programa_1.Programa
            .forge()
            .save({ name: programa })
            .call('toJSON');
        resp.status(201).json(_programa);
    }
    catch (err) {
        resp.status(500).send('Error on insert the city!!');
    }
    newParamTs('program');
});
exports.addProfesional = (req, resp) => __awaiter(void 0, void 0, void 0, function* () {
    const { profesional } = req.body;
    try {
        const _profesional = yield Profesional_1.Profesional
            .forge()
            .save({ name: profesional })
            .call('toJSON');
        resp.status(201).json(_profesional);
    }
    catch (err) {
        resp.status(500).send('Error on insert the city!!');
    }
    newParamTs('profesional');
});
exports.updateCity = (req, resp) => __awaiter(void 0, void 0, void 0, function* () {
    const { id, ciudad } = req.body;
    try {
        const _ciudad = yield Ciudad_1.default
            .forge()
            .where({ id })
            .save(ciudad, { patch: true })
            .call('toJSON');
        resp.status(201).json(_ciudad);
    }
    catch (err) {
        resp.status(500).send('Error on update the city!!');
    }
    newParamTs('city');
});
exports.updateCommunity = (req, resp) => __awaiter(void 0, void 0, void 0, function* () {
    const { id, comunidad } = req.body;
    try {
        const _comunidad = yield Comunidad_1.default
            .forge()
            .where({ id })
            .save(comunidad, { patch: true })
            .call('toJSON');
        resp.status(201).json(_comunidad);
    }
    catch (err) {
        resp.status(500).send('Error on update the city!!');
    }
    newParamTs('community');
});
exports.updateNeighborhood = (req, resp) => __awaiter(void 0, void 0, void 0, function* () {
    const { id, barrio } = req.body;
    try {
        const _barrio = yield Barrio_1.default
            .forge()
            .where({ id })
            .save(barrio, { patch: true })
            .call('toJSON');
        resp.status(201).json(_barrio);
    }
    catch (err) {
        resp.status(500).send('Error on update the city!!');
    }
    newParamTs('neighborhood');
});
const paramUpdate = (name, update) => __awaiter(void 0, void 0, void 0, function* () {
    const param = yield Update_1.default
        .where({ param: name })
        .fetch()
        .call('toJSON');
    const actualizar = update < param.update;
    return actualizar;
});
const newParamTs = (param) => __awaiter(void 0, void 0, void 0, function* () {
    yield Update_1.default
        .where({ param })
        .save({ update: Date.now() }, { patch: true })
        .call('toJSON');
});
