"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const knex_1 = __importDefault(require("knex"));
const knexNew = knex_1.default({
    client: 'mysql',
    connection: {
        host: '172.19.0.26',
        port: 3306,
        user: 'root',
        password: 'root',
        database: 'bd_gv_proyectos_viales'
    }
});
const knexProd = knex_1.default({
    client: 'mysql',
    connection: {
        host: '172.19.0.26',
        port: 3306,
        user: 'root',
        password: 'root',
        database: 'bd_gv_proyectos'
    }
});
exports.migrarProyectos = () => {
    knexNew('gv_pro')
        .truncate().then(res => {
        knexProd('gv_pro')
            .select('proId', 'proyecto', 'created', 'updated')
            .map((proyecto) => {
            try {
                let proyectoObj = Object.assign(JSON.parse(proyecto.proyecto), { creado: proyecto.created, actualizado: proyecto.updated, proId: proyecto.proId });
                delete proyectoObj.porcentaje;
                knexNew('gv_pro')
                    .insert(proyectoObj)
                    .then(res => {
                })
                    .catch(err => {
                    console.log('error de insertado:', proyecto.proId, err);
                });
                return proyectoObj;
            }
            catch (err) {
                console.log('error json:', proyecto.proId);
            }
        })
            .then((proyectos) => {
            console.log(proyectos);
        })
            .catch(err => {
            console.log('error:', err);
        });
    })
        .catch(err => {
        console.log('error truncado:', err);
    });
};
exports.migrarAdquiciones = () => {
    knexNew('gv_pro_adquisicion')
        .truncate().then(res => {
        knexProd('gv_pro_adquisicion')
            .select('proId', 'adquisicion')
            .map((adquisicion) => {
            try {
                let tempAdquisicion = Object.assign({ proId: adquisicion.proId, abs: null, mopt: null, cne: null, ordenCompra: null, otras: null, contratoCalidad: null, ordenCalidad: null, contratacion: null, refrendo: null }, JSON.parse(adquisicion.adquisicion));
                knexNew('gv_pro_adquisicion')
                    .insert(tempAdquisicion)
                    .then(res => {
                    // console.log('listo:', adquisicion.proId);
                })
                    .catch(err => {
                    console.log('error:', adquisicion.proId);
                });
                return tempAdquisicion;
            }
            catch (err) {
                console.log('error json:', adquisicion.proId);
            }
        })
            .then((adquisicion) => {
            console.log(adquisicion.length);
        });
    });
};
exports.migrarCostos = () => {
    knexNew('gv_pro_costos')
        .truncate().then(res => {
        knexProd('gv_pro_costos')
            .select('proId', 'costos')
            .map((costo) => {
            try {
                let costosTemp = Object.assign({ proId: costo.proId, procedencia: null, prevision: null, estimado: null, adjudicado: null, calidad: null, comunales: null, ordenServicio: null, especie: null, especifica: null, ampliacion: null, reajuste: null, instituciones: null, extra1: null, extra2: null }, JSON.parse(costo.costos));
                knexNew('gv_pro_costos')
                    .insert(costosTemp)
                    .then(res => {
                    // console.log('listo:', costo.proId);
                })
                    .catch(err => {
                    console.log('error:', costo.proId);
                });
                return costosTemp;
            }
            catch (err) {
                console.log('error json:', costo.proId);
            }
        })
            .then(res => {
            console.log(res.length);
        })
            .catch(err => {
            console.log(err);
        });
    });
};
exports.migrarCalidad = () => {
    knexNew('gv_pro_calidad')
        .truncate().then(res => {
        knexProd('gv_pro_calidad')
            .select('proId', 'calidad')
            .map((calidad) => {
            let calidadTemp = Object.assign({ proId: calidad.proId }, JSON.parse(calidad.calidad));
            knexNew('gv_pro_calidad')
                .insert(calidadTemp)
                .then(() => {
            })
                .catch(err => {
                console.log('error', calidadTemp);
            });
        })
            .then((calidad) => {
            console.log(calidad.length);
        })
            .catch(err => {
            console.log('error:', err);
        });
    });
};
exports.migrarRiesgos = () => {
    knexNew('gv_riesgos')
        .truncate().then(res => {
        knexProd('gv_riesgos')
            .select('riesgoId', 'riesgo')
            .map((riesgo) => {
            try {
                let riesgoTemp = Object.assign({ riesgoId: riesgo.riesgoId }, JSON.parse(riesgo.riesgo));
                knexNew('gv_riesgos')
                    .insert(riesgoTemp)
                    .then(res => {
                    console.log('listo:', riesgo.riesgoId);
                })
                    .catch(err => {
                    console.log('error:', riesgo.riesgoId);
                });
                return riesgoTemp;
            }
            catch (err) {
                console.log('error json:', riesgo.riesgoId, err);
            }
        })
            .then((riesgos) => {
            console.log(riesgos.length);
        })
            .catch(err => {
            console.log('error:', err);
        });
    });
};
exports.migrarAvance = () => {
    knexNew('gv_pro_avance')
        .truncate().then(res => {
        knexProd('gv_pro_avance')
            .select('proId', 'avance')
            .map((avance) => {
            try {
                let avanceTemp = Object.assign({ proId: avance.proId }, JSON.parse(avance.avance));
                delete avanceTemp.bitacora;
                delete avanceTemp.pendientes;
                // actualizar avance
                knexNew('gv_pro_avance')
                    .insert(avanceTemp)
                    .then(res => {
                    // console.log('listo:',avance.proId);
                })
                    .catch(err => {
                    console.log('error:', avance.proId, avanceTemp);
                });
                return avanceTemp;
            }
            catch (err) {
                console.log('error json:', avance.proId, err);
            }
        })
            .then((avances) => {
            console.log(avances.length);
        })
            .catch(err => {
            console.log('error:', err);
        });
    });
};
exports.migrarBitacoras = () => {
    knexNew('gv_pro_bitacoras')
        .truncate()
        .then(res => {
        console.log('bitacoras limpias');
        knexProd('gv_pro_avance')
            .select('proId', 'avance')
            .map((avance) => {
            try {
                let avanceTemp = Object.assign({ proId: avance.proId }, JSON.parse(avance.avance));
                // insertar bitacoras
                if (avanceTemp.bitacora) {
                    avanceTemp.bitacora.map(bitacora => {
                        bitacora = Object.assign(Object.assign({}, bitacora), { proId: avance.proId });
                        knexNew('gv_pro_bitacoras')
                            .insert(bitacora)
                            .then(res => {
                            // console.log('correcto');
                        })
                            .catch(err => {
                            console.log(bitacora);
                        });
                        return bitacora;
                    });
                }
            }
            catch (err) {
                console.log('error json:', avance.proId, err);
            }
        })
            .then((bitacoras) => {
            console.log(bitacoras.length);
        })
            .catch(err => {
            console.log('error:', err);
        });
    })
        .catch(err => {
        console.log(err);
    });
};
exports.migrarPendientes = () => {
    knexNew('gv_pro_pendientes')
        .truncate()
        .then(res => {
        console.log('pendientes limpias');
        knexProd('gv_pro_avance')
            .select('proId', 'avance')
            .map((avance) => {
            try {
                let avanceTemp = JSON.parse(avance.avance);
                // insertar pendientes
                if (avanceTemp.pendientes) {
                    avanceTemp.pendientes.map(pendiente => {
                        pendiente = Object.assign(Object.assign({}, pendiente), { proId: avance.proId });
                        knexNew('gv_pro_pendientes')
                            .insert(pendiente)
                            .then(res => {
                            // console.log('correcto');
                        })
                            .catch(err => {
                            console.log(pendiente);
                        });
                        return pendiente;
                    });
                }
            }
            catch (err) {
                console.log('error json:', avance.proId, err);
            }
        })
            .then((pendientes) => {
            console.log(pendientes.length);
        })
            .catch(err => {
            console.log('error:', err);
        });
    })
        .catch(err => {
        console.log(err);
    });
};
exports.migrarMensajes = () => {
    knexNew(' gv_pro_mensaje')
        .truncate()
        .then(res => {
        knexProd('gv_pro_mensajes')
            .map((res) => {
            let mensajes = JSON.parse(res.mensajes);
            return mensajes.map(mensaje => {
                let nuevoMensaje = Object.assign(Object.assign({}, mensaje), { proId: res.proId });
                return knexNew(' gv_pro_mensaje')
                    .insert(nuevoMensaje)
                    .then(res => {
                    return nuevoMensaje;
                })
                    .catch(err => {
                    console.log('error', nuevoMensaje);
                });
            });
        })
            .then(res => {
            console.log(res.length);
        })
            .catch(err => {
            console.log('error:', err);
        });
    });
};
exports.migrarRiesgosProyectos = () => {
    knexNew('gv_pro_riesgo')
        .truncate()
        .then(res => {
        knexProd('gv_pro_riesgos')
            .map((res) => {
            let riesgos = JSON.parse(res.riesgos);
            return riesgos.map(riesgo => {
                let id = riesgo.id;
                delete riesgo.id;
                let nuevoRiesgo = Object.assign(Object.assign({}, riesgo), { proId: res.proId, idRiesgo: id });
                delete nuevoRiesgo.id;
                return knexNew('gv_pro_riesgo')
                    .insert(nuevoRiesgo)
                    .then(res => {
                    return nuevoRiesgo;
                })
                    .catch(err => {
                    console.log('error', nuevoRiesgo, err);
                });
            });
        })
            .then(res => {
            console.log(res.length);
        })
            .catch(err => {
            console.log('error:', err);
        });
    });
};
exports.migrarCompletar = () => {
    knexNew('gv_pro_completar')
        .truncate()
        .then(res => {
        return knexProd('gv_pro_seguimiento')
            .map((res) => {
            let seguimiento = JSON.parse(res.seguimiento);
            if (seguimiento.completar) {
                return seguimiento.completar.map(punto => {
                    punto = Object.assign(Object.assign({}, punto), { proId: res.proId });
                    // console.log(punto);
                    return knexNew('gv_pro_completar')
                        .insert(punto)
                        .then(res => {
                        return punto;
                    })
                        .catch(err => {
                        console.log('error', punto);
                    });
                });
            }
            else {
                return null;
            }
        })
            .then(res => {
            console.log(res.length);
        })
            .catch(err => {
            console.log(err);
        });
    });
};
exports.migrarSeguimiento = () => {
    knexNew('gv_pro_seguimiento')
        .truncate()
        .then(res => {
        knexProd('gv_pro_seguimiento')
            .map((seguimiento) => {
            let seguimientoTemp = Object.assign({ proId: seguimiento.proId }, JSON.parse(seguimiento.seguimiento));
            delete seguimientoTemp.completar;
            delete seguimientoTemp.inspecciones;
            knexNew('gv_pro_seguimiento')
                .insert(seguimientoTemp)
                .then(res => {
                return seguimientoTemp;
            })
                .catch(err => {
                console.log('error', seguimiento.proId, seguimientoTemp);
            });
        })
            .then(seguimientos => {
            console.log(seguimientos);
        })
            .catch(err => {
            console.log('error', err);
        });
    });
};
exports.migrarProyectos();
exports.migrarAdquiciones();
exports.migrarCostos();
exports.migrarCalidad();
exports.migrarRiesgos();
exports.migrarAvance();
exports.migrarBitacoras();
exports.migrarPendientes();
exports.migrarMensajes();
exports.migrarRiesgosProyectos();
exports.migrarCompletar();
exports.migrarSeguimiento();
