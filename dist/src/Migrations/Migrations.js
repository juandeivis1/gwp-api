"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const knex_1 = __importDefault(require("knex"));
const knexNew = knex_1.default({
    client: 'mysql',
    connection: {
        host: '172.19.0.26',
        port: 3306,
        user: 'root',
        password: 'root',
        database: 'bd_gv_proyectos_viales'
    }
});
const knexProd = knex_1.default({
    client: 'mysql',
    connection: {
        host: '172.19.0.26',
        port: 3306,
        user: 'root',
        password: 'root',
        database: 'bd_gv_proyectos'
    }
});
exports.migrarProyectos = () => {
    knexNew('gv_pro')
        .truncate().then(res => {
        knexProd('gv_pro')
            .select('proId', 'proyecto', 'created', 'updated')
            .map((proyecto) => {
            try {
                let proyectoObj = Object.assign(JSON.parse(proyecto.proyecto), { creado: proyecto.created, actualizado: proyecto.updated });
                knexNew('gv_pro')
                    .insert(proyectoObj)
                    // .where('proId', '=', proyecto.proId)
                    .then(res => {
                })
                    .catch(err => {
                    console.log('error:', proyecto.proId);
                });
                return proyectoObj;
            }
            catch (err) {
                console.log('error json:', proyecto.proId);
            }
        })
            .then((proyectos) => {
            console.log(proyectos);
        })
            .catch(err => {
            console.log('error:', err);
        });
    });
};
exports.obtenerProyectos = () => {
    return knexNew('gv_pro')
        .select('proId', 'nombre', 'sinonimo', 'expediente', 'portafolio', 'prioridad', 'programa', 'creado', 'actualizado')
        .then((proyectos) => {
        return proyectos;
    })
        .catch(err => {
        console.log('error:', err);
    });
};
exports.migrarAdquiciones = () => {
    knexNew('gv_pro_adquisicion')
        .select('proId', 'adquisicion')
        .map((adquisicion) => {
        try {
            let tempAdquisicion = Object.assign({ abs: null, mopt: null, cne: null, ordenCompra: null, otras: null, contratoCalidad: null, ordenCalidad: null, contratacion: null, refrendo: null }, JSON.parse(adquisicion.adquisicion));
            knexNew('gv_pro_adquisicion')
                .update(tempAdquisicion)
                .where('proId', '=', adquisicion.proId)
                .then(res => {
                // console.log('listo:', adquisicion.proId);
            })
                .catch(err => {
                console.log('error:', adquisicion.proId);
            });
            return tempAdquisicion;
        }
        catch (err) {
            console.log('error json:', adquisicion.proId);
        }
    })
        .then((adquisicion) => {
        console.log(adquisicion.length);
    });
};
exports.obtenerAdquisiciones = () => {
    return knexNew('gv_pro_adquisicion')
        .select('proId', 'abs', 'mopt', 'cne', 'otras', 'contratoCalidad', 'ordenCalidad', 'contratacion', 'refrendo')
        .then((adquisiciones) => {
        return { adquisiciones };
    })
        .catch(err => {
        console.log('error:', err);
    });
};
exports.migrarCostos = () => {
    knexNew('gv_pro_costos')
        .select('proId', 'costos')
        .map((costo) => {
        try {
            let costosTemp = Object.assign({ procedencia: null, prevision: null, estimado: null, adjudicado: null, calidad: null, comunales: null, ordenServicio: null, especie: null, especifica: null, ampliacion: null, reajuste: null, instituciones: null, extra1: null, extra2: null }, JSON.parse(costo.costos));
            knexNew('gv_pro_costos')
                .update(costosTemp)
                .where('proId', '=', costo.proId)
                .then(res => {
                console.log('listo:', costo.proId);
            })
                .catch(err => {
                console.log('error:', costo.proId);
            });
            return costosTemp;
        }
        catch (err) {
            console.log('error json:', costo.proId);
        }
    })
        .then(res => {
        console.log(res);
    })
        .catch(err => {
        console.log(err);
    });
};
exports.obtenerCostos = () => {
    knexNew('gv_pro_costos')
        .select('proId', 'procedencia', 'prevision', 'estimado', 'adjudicado', 'calidad', 'comunales', 'ordenServicio', 'especie', 'especifica', 'ampliacion', 'reajuste', 'instituciones', 'extra1', 'extra2')
        .then((costos) => {
        return { costos };
    })
        .catch(err => {
        console.log('error:', err);
    });
};
exports.obtenerCalidad = () => {
    return knexNew('gv_pro_calidad')
        .select('proId', 'estado')
        .then((calidad) => {
        return { calidad };
    })
        .catch(err => {
        console.log('error:', err);
    });
};
exports.migrarRiesgos = () => {
    knexNew('gv_riesgos')
        .select('riesgoId', 'riesgo')
        .map((riesgo) => {
        try {
            let riesgoTemp = JSON.parse(riesgo.riesgo);
            knexNew('gv_riesgos')
                .update(riesgoTemp)
                .where('riesgoId', '=', riesgo.riesgoId)
                .then(res => {
                console.log('listo:', riesgo.riesgoId);
            })
                .catch(err => {
                console.log('error:', riesgo.riesgoId);
            });
            return riesgoTemp;
        }
        catch (err) {
            console.log('error json:', riesgo.riesgoId, err);
        }
    })
        .then((riesgos) => {
        console.log(riesgos);
    })
        .catch(err => {
        console.log('error:', err);
    });
};
exports.obtenerRiesgos = () => {
    return knexNew('gv_riesgos')
        .select('riesgoId', 'fase', 'categoria', 'subcategoria', 'causa', 'consecuencia', 'actividad')
        .then((riesgos) => {
        return { riesgos };
    })
        .catch(err => {
        console.log('error:', err);
    });
};
exports.migrarAvance = () => {
    knexNew('gv_pro_avance')
        .select('proId', 'avance')
        .map((avance) => {
        try {
            let avanceTemp = JSON.parse(avance.avance);
            delete avanceTemp.bitacora;
            delete avanceTemp.pendientes;
            // actualizar avance
            knexNew('gv_pro_avance')
                .update(avanceTemp)
                .where('proId', '=', avance.proId)
                .then(res => {
                // console.log('listo:',avance.proId);
            })
                .catch(err => {
                console.log('error:', avance.proId, avanceTemp);
            });
            return avanceTemp;
        }
        catch (err) {
            console.log('error json:', avance.proId, err);
        }
    })
        .then((avances) => {
        console.log(avances);
    })
        .catch(err => {
        console.log('error:', err);
    });
};
exports.migrarBitacoras = () => {
    knexNew('gv_pro_bitacoras')
        .truncate()
        .then(res => {
        console.log('bitacoras limpias');
        knexNew('gv_pro_avance')
            .select('proId', 'avance')
            .map((avance) => {
            try {
                let avanceTemp = JSON.parse(avance.avance);
                // insertar bitacoras
                if (avanceTemp.bitacora) {
                    avanceTemp.bitacora.map(bitacora => {
                        bitacora = Object.assign(Object.assign({}, bitacora), { proId: avance.proId });
                        knexNew('gv_pro_bitacoras')
                            .insert(bitacora)
                            .then(res => {
                            // console.log('correcto');
                        })
                            .catch(err => {
                            console.log(bitacora);
                        });
                        return bitacora;
                    });
                }
            }
            catch (err) {
                console.log('error json:', avance.proId, err);
            }
        })
            .then((bitacoras) => {
            console.log(bitacoras.length);
        })
            .catch(err => {
            console.log('error:', err);
        });
    })
        .catch(err => {
        console.log(err);
    });
};
exports.obtenerBitacoras = (proId) => {
    return knexNew('gv_pro_bitacoras')
        .select('usuario', 'linea', 'fecha')
        .where('proId', proId)
        .then((bitacoras) => {
        return { bitacoras };
    })
        .catch(err => {
        console.log('error:', err);
    });
};
exports.migrarPendientes = () => {
    knexNew('gv_pro_pendientes')
        .truncate()
        .then(res => {
        console.log('pendientes limpias');
        knexNew('gv_pro_avance')
            .select('proId', 'avance')
            .map((avance) => {
            try {
                let avanceTemp = JSON.parse(avance.avance);
                // insertar pendientes
                if (avanceTemp.pendientes) {
                    avanceTemp.pendientes.map(pendiente => {
                        pendiente = Object.assign(Object.assign({}, pendiente), { proId: avance.proId });
                        knexNew('gv_pro_pendientes')
                            .insert(pendiente)
                            .then(res => {
                            // console.log('correcto');
                        })
                            .catch(err => {
                            console.log(pendiente);
                        });
                        return pendiente;
                    });
                }
            }
            catch (err) {
                console.log('error json:', avance.proId, err);
            }
        })
            .then((pendientes) => {
            console.log(pendientes.length);
        })
            .catch(err => {
            console.log('error:', err);
        });
    })
        .catch(err => {
        console.log(err);
    });
};
exports.obtenerPendientes = (proId) => {
    return knexNew('gv_pro_pendientes')
        .select('asunto', 'generado', '	resuelto')
        .where('proId', proId)
        .then((pendientes) => {
        return { pendientes };
    })
        .catch(err => {
        console.log('error:', err);
    });
};
exports.obtenerAvance = (proId) => {
    return knexNew('gv_pro_avance')
        .select('porcentaje', 'inicio', 'fin')
        .where('proId', proId)
        .then((avance) => {
        return { avance };
    })
        .catch(err => {
        console.log(err);
    });
};
exports.migrarMensajes = () => {
    knexNew(' gv_pro_mensaje')
        .truncate()
        .then(res => {
        knexNew('gv_pro_mensajes')
            .map((res) => {
            let mensajes = JSON.parse(res.mensajes);
            return mensajes.map(mensaje => {
                let nuevoMensaje = Object.assign(Object.assign({}, mensaje), { proId: res.proId });
                return knexNew(' gv_pro_mensaje')
                    .insert(nuevoMensaje)
                    .then(res => {
                    return nuevoMensaje;
                })
                    .catch(err => {
                    console.log('error', nuevoMensaje);
                });
            });
        })
            .then(res => {
            console.log(res.length);
        })
            .catch(err => {
            console.log('error:', err);
        });
    });
};
exports.obtenerMensajes = (proId) => {
    return knexNew(' gv_pro_mensaje')
        .select('mensaje', 'usuario', 'ts')
        .where('proId', proId)
        .then((mensajes) => {
        return { mensajes };
    })
        .catch(err => {
        console.log('error:', err);
        return [];
    });
};
exports.obtenerProyecto = (proId) => {
    return knexNew(' gv_pro')
        .select('proId', 'nombre', 'sinonimo', 'expediente', 'portafolio', 'prioridad', 'programa', 'creado', 'actualizado')
        .where('proId', proId)
        .then((proyecto) => {
        return { proyecto: proyecto[0] };
    })
        .catch(err => {
        console.log('error:', err);
        return {};
    });
};
exports.migrarRiesgosProyectos = () => {
    knexNew('gv_pro_riesgo')
        .truncate()
        .then(res => {
        knexNew('gv_pro_riesgos')
            .map((res) => {
            let riesgos = JSON.parse(res.riesgos);
            return riesgos.map(riesgo => {
                let nuevoRiesgo = Object.assign(Object.assign({}, riesgo), { proId: res.proId });
                return knexNew('gv_pro_riesgo')
                    .insert(nuevoRiesgo)
                    .then(res => {
                    return nuevoRiesgo;
                })
                    .catch(err => {
                    console.log('error', nuevoRiesgo);
                });
            });
        })
            .then(res => {
            console.log(res.length);
        })
            .catch(err => {
            console.log('error:', err);
        });
    });
};
exports.obtenerSeguimiento = (proId) => {
    return knexNew('gv_pro_seguimiento')
        .select('ejecutado', 'ejecutar')
        .where('proId', proId)
        .then(seguimiento => {
        return { seguimiento: seguimiento[0] };
    })
        .catch(err => {
        console.log('error:', err);
        return {};
    });
};
exports.migrarSeguimiento = () => {
    knexNew('gv_pro_seguimiento')
        .map((seguimiento) => {
        let seguimientoTemp = Object.assign({}, JSON.parse(seguimiento.seguimiento));
        delete seguimientoTemp.completar;
        knexNew('gv_pro_seguimiento')
            .update(seguimientoTemp)
            .where('proId', seguimiento.proId)
            .then(res => {
            return seguimientoTemp;
        })
            .catch(err => {
            console.log('error', seguimiento.proId, seguimientoTemp);
        });
    })
        .then(seguimientos => {
        console.log(seguimientos);
    })
        .catch(err => {
        console.log('error', err);
    });
};
exports.migrarCompletar = () => {
    knexNew('gv_pro_completar')
        .truncate()
        .then(res => {
        return knexNew('gv_pro_seguimiento')
            .map((res) => {
            let seguimiento = JSON.parse(res.seguimiento);
            if (seguimiento.completar) {
                return seguimiento.completar.map(punto => {
                    punto = Object.assign(Object.assign({}, punto), { proId: res.proId });
                    // console.log(punto);
                    return knexNew('gv_pro_completar')
                        .insert(punto)
                        .then(res => {
                        return punto;
                    })
                        .catch(err => {
                        console.log('error', punto);
                    });
                });
            }
            else {
                return null;
            }
        })
            .then(res => {
            console.log(res.length);
        })
            .catch(err => {
            console.log(err);
        });
    });
};
exports.obtenerCompletar = (proId) => {
    return knexNew('gv_pro_completar')
        .select('punto', 'check')
        .where('proId', proId)
        .then(completar => {
        return { completar };
    });
};
