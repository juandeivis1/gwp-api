"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Database_1 = require("../../Database/Database");
class Inspeccion extends Database_1.bookshelf.Model {
    get tableName() { return 'gv_pro_inspeccion'; }
}
exports.default = Inspeccion;
