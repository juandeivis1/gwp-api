"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Database_1 = require("../../Database/Database");
class Riesgos extends Database_1.bookshelf.Model {
    get tableName() { return 'gv_riesgos'; }
}
exports.default = Riesgos;
