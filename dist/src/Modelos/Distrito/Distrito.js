"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Database_1 = require("../../Database/Database");
class Distrito extends Database_1.bookshelf.Model {
    get tableName() { return 'gv_district'; }
}
exports.default = Distrito;
