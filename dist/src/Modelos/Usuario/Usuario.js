"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Database_1 = require("../../Database/Database");
const Token_1 = __importDefault(require("../Token/Token"));
class Usuario extends Database_1.bookshelf.Model {
    constructor() {
        super(...arguments);
        this.tokens = () => {
            return this.hasMany(Token_1.default, 'userId');
        };
    }
    get tableName() { return 'gv_users'; }
    get idAttribute() { return 'userId'; }
}
exports.default = Usuario;
