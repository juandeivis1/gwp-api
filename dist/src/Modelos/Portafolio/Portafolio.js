"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Database_1 = require("../../Database/Database");
class Portafolio extends Database_1.bookshelf.Model {
    get tableName() { return 'gv_portfolio'; }
}
exports.Portafolio = Portafolio;
