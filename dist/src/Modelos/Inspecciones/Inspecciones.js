"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Database_1 = require("../../Database/Database");
class Inspecciones extends Database_1.bookshelf.Model {
    get tableName() { return 'gv_inspecciones'; }
}
exports.default = Inspecciones;
