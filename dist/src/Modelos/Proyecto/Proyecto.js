"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Database_1 = require("../../Database/Database");
const Adquisicion_1 = __importDefault(require("../Adquisicion/Adquisicion"));
const Archivos_1 = __importDefault(require("../Archivos/Archivos"));
const Avance_1 = __importDefault(require("../Avance/Avance"));
const Calidad_1 = __importDefault(require("../Calidad/Calidad"));
const Costos_1 = __importDefault(require("../Costos/Costos"));
const Informacion_1 = __importDefault(require("../Informacion/Informacion"));
const Riesgo_1 = __importDefault(require("../Riesgo/Riesgo"));
const Seguimiento_1 = __importDefault(require("../Seguimiento/Seguimiento"));
const Mensajes_1 = __importDefault(require("../Mensajes/Mensajes"));
const Bitacora_1 = require("../Bitacora/Bitacora");
const Pendiente_1 = require("../Pendiente/Pendiente");
const Completar_1 = require("../Completar/Completar");
const Inspeccion_1 = __importDefault(require("../Inspeccion/Inspeccion"));
class Proyecto extends Database_1.bookshelf.Model {
    constructor() {
        super(...arguments);
        this.adquisicion = () => {
            return this.hasOne(Adquisicion_1.default, 'proId');
        };
        this.archivos = () => {
            return this.hasMany(Archivos_1.default, 'proId');
        };
        this.avance = () => {
            return this.hasOne(Avance_1.default, 'proId');
        };
        this.bitacora = () => {
            return this.hasMany(Bitacora_1.Bitacora, 'proId');
        };
        this.recientes = () => {
            return this.hasMany(Bitacora_1.Bitacora, 'proId').query(qb => qb.orderBy('ts', 'asc').limit(2));
        };
        this.pendiente = () => {
            return this.hasMany(Pendiente_1.Pendiente, 'proId');
        };
        this.pendientes = () => {
            return this.hasMany(Pendiente_1.Pendiente, 'proId').query(qb => qb.whereNull('resuelto'));
        };
        this.calidad = () => {
            return this.hasOne(Calidad_1.default, 'proId');
        };
        this.completar = () => {
            return this.hasMany(Completar_1.Completar, 'proId');
        };
        this.costos = () => {
            return this.hasOne(Costos_1.default, 'proId');
        };
        this.informacion = () => {
            return this.hasOne(Informacion_1.default, 'proId');
        };
        this.mensajes = () => {
            return this.hasMany(Mensajes_1.default, 'proId').query(q => q.orderBy('ts', 'asc'));
        };
        this.seguimiento = () => {
            return this.hasOne(Seguimiento_1.default, 'proId');
        };
        this.riesgos = () => {
            return this.hasMany(Riesgo_1.default, 'proId');
        };
        this.inspecciones = () => {
            return this.hasMany(Inspeccion_1.default, 'proId');
        };
    }
    get tableName() { return 'gv_pro'; }
    get idAttribute() { return 'proId'; }
    static get relationships() {
        return [
            'adquisicion',
            'archivos',
            'avance',
            'bitacora',
            'pendiente',
            'calidad',
            'costos',
            'informacion',
            'mensajes',
            'seguimiento',
            'completar',
            'riesgos',
            'inspecciones'
        ];
    }
    static get columns() {
        return [
            'actualizado',
            'creado',
            'expediente',
            'nombre',
            'portafolio',
            'prioridad',
            'proId',
            'programa',
            'sinonimo'
        ];
    }
}
exports.default = Proyecto;
