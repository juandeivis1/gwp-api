"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Database_1 = require("../../Database/Database");
class Avance extends Database_1.bookshelf.Model {
    get tableName() { return 'gv_pro_avance'; }
    get idAttribute() { return 'proId'; }
}
exports.default = Avance;
