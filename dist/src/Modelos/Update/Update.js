"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Database_1 = require("../../Database/Database");
class Update extends Database_1.bookshelf.Model {
    get tableName() { return 'gv_updates'; }
}
exports.default = Update;
