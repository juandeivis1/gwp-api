"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Database_1 = require("../../Database/Database");
class Barrio extends Database_1.bookshelf.Model {
    get tableName() { return 'gv_neighborhood'; }
}
exports.default = Barrio;
