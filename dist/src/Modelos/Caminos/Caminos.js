"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Database_1 = require("../../Database/Database");
class Caminos extends Database_1.bookshelf.Model {
    get tableName() { return 'gv_caminos'; }
}
exports.default = Caminos;
