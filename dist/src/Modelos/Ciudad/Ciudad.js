"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Database_1 = require("../../Database/Database");
class Ciudad extends Database_1.bookshelf.Model {
    get tableName() { return 'gv_city'; }
}
exports.default = Ciudad;
const insertarCiudades = (pueblos) => {
    pueblos.map(ciudad => {
        Ciudad
            .forge()
            .save({ name: ciudad, district: 11 })
            .then(res => {
            console.log(res);
        })
            .catch(err => {
            console.log(err);
        });
    });
};
