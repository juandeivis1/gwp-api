"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Database_1 = require("../../Database/Database");
const Usuario_1 = __importDefault(require("../Usuario/Usuario"));
class Token extends Database_1.bookshelf.Model {
    constructor() {
        super(...arguments);
        this.user = () => {
            return this.belongsTo(Usuario_1.default, 'userId');
        };
    }
    get tableName() { return 'gv_tokens'; }
}
exports.default = Token;
