"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const admin = __importStar(require("firebase-admin"));
const Functions_common_1 = require("../2.0/Functions/Functions.common");
exports.sendNewMessageTo = (tittle, body, device, mensaje, proId) => __awaiter(void 0, void 0, void 0, function* () {
    var payload = {
        notification: {
            title: tittle,
            body: body,
            icon: "/assets/icons/gv-icon-512x512.png",
            click_action: `https://perezzeledon.go.cr:803/gestion/proyectos/actual/${proId}/mensajeria`,
            data: JSON.stringify(mensaje)
        }
    };
    return yield admin
        .messaging()
        .sendToDevice(device, payload);
});
exports.sendTo = (tittle, body, device, proId) => __awaiter(void 0, void 0, void 0, function* () {
    var payload = {
        notification: {
            title: tittle,
            body: body,
            icon: "/assets/icons/gv-icon-512x512.png",
            click_action: `https://perezzeledon.go.cr:803/gestion/proyectos/actual/${proId}/avance`
        }
    };
    return yield admin
        .messaging()
        .sendToDevice(device, payload);
});
exports.subscribeToUpdates = (fcmToken, dev) => __awaiter(void 0, void 0, void 0, function* () {
    let entorno = dev ? 'dev' : 'update';
    yield admin
        .messaging()
        .subscribeToTopic(fcmToken, entorno);
});
exports.unsubscribeToUpdates = (fcmToken, dev) => __awaiter(void 0, void 0, void 0, function* () {
    let entorno = dev ? 'dev' : 'update';
    yield admin
        .messaging()
        .unsubscribeFromTopic(fcmToken, entorno);
});
exports.sendUpdates = (payload, dev) => {
    let entorno = dev ? 'dev' : 'update';
    admin
        .messaging()
        .sendToTopic(entorno, payload)
        .then(res => console.log(res))
        .catch(err => console.log(err));
};
exports.enviarNuevoProyecto = (sinonimo, expediente, proId, dev) => {
    let entorno = dev ? `http://localhost:4200/gestion/proyectos/actual/${proId}` : `https://perezzeledon.go.cr:803/gestion/proyectos/actual/${proId}`;
    exports.sendUpdates({
        notification: {
            body: sinonimo,
            title: `Nueva bitácora ${expediente}`,
            icon: "/assets/icons/gv-icon-512x512.png",
            click_action: entorno
        }, data: { proId: proId.toString() }
    }, dev);
};
exports.enviarNuevoAvance = (sinonimo, expediente, proId, ts, dev) => {
    let entorno = dev ? `http://localhost:4200/gestion/proyectos/actual/${proId}/avance` : `https://perezzeledon.go.cr:803/gestion/proyectos/actual/${proId}/avance`;
    exports.sendUpdates({
        notification: {
            body: sinonimo,
            title: `Nueva bitácora ${expediente}`,
            icon: "/assets/icons/gv-icon-512x512.png",
            click_action: entorno,
        }, data: { proId: proId.toString(), ts: ts.toString() }
    }, dev);
};
exports.enviarNuevaBitacora = (body, expediente, proId) => __awaiter(void 0, void 0, void 0, function* () {
    let tokens = yield Functions_common_1.obtenerTokensEquipo(proId);
    tokens.length > 0 ?
        yield exports.sendTo(`Nueva bitácora ${expediente}`, body, tokens, proId)
        : null;
});
exports.enviarArchivo = (tokens, usuario, fase, link) => {
    return admin.messaging()
        .sendToDevice(tokens, {
        notification: {
            title: fase,
            body: `${usuario.toUpperCase()} ha enviado un archivo`,
            clickAction: link,
            icon: "/assets/icons/gv-icon-512x512.png",
        }
    });
};
