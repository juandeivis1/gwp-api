"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const express = __importStar(require("express"));
const Dev_functions_1 = require("./Dev.functions");
const devRouter = express.Router();
exports.dev = devRouter;
devRouter.get('/implemented', Dev_functions_1.implemented);
devRouter.get('/requested', Dev_functions_1.requested);
devRouter.post('/request', Dev_functions_1.request);
devRouter.post('/finish', Dev_functions_1.finish);
devRouter.get('/users', Dev_functions_1.users);
devRouter.post('/user', Dev_functions_1.user);
