"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Database_1 = require("../Database/Database");
exports.implemented = (req, resp) => {
    Database_1.knexGV('dev_solicitudes')
        .where('public', '=', 1)
        .select('implemented', 'tsRelease')
        .orderBy('tsRelease', 'desc')
        .limit(10)
        .map((implementacion) => Object.assign(JSON.parse(implementacion.implemented), { tsRelease: implementacion.tsRelease }))
        .then(res => {
        resp.json(res);
    })
        .catch(err => {
        resp.status(400).json({ mpzError: 'Algo salio mal al obtener implementaciones' });
        console.log(err);
    });
};
exports.requested = (req, resp) => {
    Database_1.knexGV('dev_solicitudes')
        .where('public', '=', 0)
        .select('id', 'tsCreated', 'request')
        .orderBy('tsCreated', 'asc')
        .map((solicitud) => Object.assign(JSON.parse(solicitud.request), { tsCreated: solicitud.tsCreated, id: solicitud.id }))
        .then(res => {
        resp.json(res);
    })
        .catch(err => {
        resp.status(400).json({ mpzError: 'Algo salio mal al obtener solicitudes' });
        console.log(err);
    });
};
exports.request = (req, resp) => {
    let { request } = req.body;
    const ts = new Date().getTime();
    Database_1.knexGV('dev_solicitudes')
        .insert({
        tsCreated: ts,
        request: JSON.stringify(request)
    }, 'id')
        .then(res => {
        resp.json(res[0]);
    })
        .catch(err => {
        resp.status(400).json({ mpzError: 'Algo salio mal al obtener solicitudes' });
        console.log(err);
    });
};
exports.finish = (req, resp) => {
    let { id, implementado } = req.body;
    const ts = new Date().getTime();
    if (id) {
        Database_1.knexGV('dev_solicitudes')
            .update({
            implemented: JSON.stringify(implementado),
            public: 1,
            tsRelease: ts
        }, 'id')
            .where('id', '=', id)
            .then(() => {
            resp.json(id);
        })
            .catch(err => {
            resp.status(400).json({ mpzError: 'Algo salio mal al finalizar implementación' });
            console.log(err);
        });
    }
    else {
        Database_1.knexGV('dev_solicitudes')
            .insert({
            implemented: JSON.stringify(implementado),
            public: 1,
            tsRelease: ts,
            tsCreated: ts
        }, 'id')
            .then(res => {
            resp.json(res[0]);
        })
            .catch(err => {
            resp.status(400).json({ mpzError: 'Algo salio mal al obtener solicitudes' });
            console.log(err);
        });
    }
};
exports.users = (req, resp) => {
    Database_1.knexGV('gv_users')
        .select('userId', 'user', 'active', 'team', 'permissions', 'name')
        .orderBy('user', 'asc')
        .map((usuario) => {
        usuario.permissions = JSON.parse(usuario.permissions);
        return usuario;
    })
        .then(res => {
        resp.json(res);
    })
        .catch(err => {
        resp.status(400).json({ mpzError: 'Algo salio mal al obtener usuarios' });
        console.log(err);
    });
};
exports.user = (req, resp) => {
    let { user, userId } = req.body;
    if (userId) {
        Database_1.knexGV('gv_users')
            .update({
            user: user.user,
            active: user.active,
            team: user.team,
            permissions: JSON.stringify(user.permissions),
            name: user.name
        }, 'userId')
            .where('userId', '=', userId)
            .then(res => {
            resp.json(userId);
        })
            .catch(err => {
            resp.status(400).json({ mpzError: 'Algo salio mal al actualizar usuario' });
            console.log(err);
        });
    }
    else {
        Database_1.knexGV('gv_users')
            .insert({
            user: user.user,
            active: user.active,
            team: user.team,
            permissions: JSON.stringify(user.permissions),
            name: user.name,
            fcmTokens: '[]'
        }, 'userId')
            .then(res => {
            resp.json(res);
        })
            .catch(err => {
            resp.status(400).json({ mpzError: 'Algo salio mal al actualizar usuario' });
            console.log(err);
        });
    }
};
