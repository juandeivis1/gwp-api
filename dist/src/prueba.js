"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const knex_1 = __importDefault(require("knex"));
const bookshelf_1 = __importDefault(require("bookshelf"));
exports.knexDev = knex_1.default({
    client: 'mysql',
    connection: {
        host: '172.19.0.26',
        port: 3306,
        user: 'root',
        password: 'root',
        database: 'bd_gv_proyectos_dev'
    }
});
const Bookshelf = bookshelf_1.default(exports.knexDev);
// var User = bookshelf.Model.extend({
//   tableName: 'gv_users',
//   tokens: () => {
//     return this.hasMany(Tokens, 'user');
//   }
// });
// var Tokens = bookshelf.Model.extend({
//   tableName: 'gv_tokens',
//   user: () => {
//     return this.belongsTo(User, 'user');
//   }
// });
// User.forge({userId: 1}).fetch({withRelated: ['tokens']}).then((user) =>{
//     console.log('Got user:', user.get('name'));
//   console.log(user.toJSON());
// }).catch((err) => {
//   console.error(err);
// });
// export default
class User extends Bookshelf.Model {
    constructor() {
        super(...arguments);
        this.tokens = () => {
            return this.hasMany(Tokens, 'user', 'user');
        };
    }
    get tableName() { return 'gv_users'; }
}
class Tokens extends Bookshelf.Model {
    constructor() {
        super(...arguments);
        this.tokens = () => {
            return this.belongsTo(User, 'user', 'user');
        };
    }
    get tableName() { return 'gv_tokens'; }
}
class Proyecto extends Bookshelf.Model {
    constructor() {
        super(...arguments);
        this.archivos = () => {
            return this.hasMany(Archivos, 'proId');
        };
        this.adquisicion = () => {
            return this.hasOne(Adquisicion, 'proId');
        };
        this.avance = () => {
            return this.hasOne(Avance, 'proId');
        };
        this.calidad = () => {
            return this.hasOne(Calidad, 'proId');
        };
        this.costos = () => {
            return this.hasOne(Costos, 'proId');
        };
        this.informacion = () => {
            return this.hasOne(Informacion, 'proId');
        };
        this.mensajes = () => {
            return this.hasMany(Mensajes, 'proId');
        };
        this.seguimiento = () => {
            return this.hasOne(Seguimiento, 'proId');
        };
        this.riesgos = () => {
            return this.hasMany(Riesgos, 'proId');
        };
    }
    get tableName() { return 'gv_pro'; }
    get idAttribute() { return 'proId'; }
}
class Adquisicion extends Bookshelf.Model {
    get tableName() { return 'gv_pro_adquisicion'; }
}
class Bitacoras extends Bookshelf.Model {
    get tableName() { return 'gv_pro_bitacoras'; }
}
class Costos extends Bookshelf.Model {
    get tableName() { return 'gv_pro_costos'; }
}
class Seguimiento extends Bookshelf.Model {
    get tableName() { return 'gv_pro_seguimiento'; }
    get idAttribute() { return 'proId'; }
    completar() {
        return this.hasMany(Completar, 'proId');
    }
}
class Completar extends Bookshelf.Model {
    get tableName() { return 'gv_pro_completar'; }
}
class Archivos extends Bookshelf.Model {
    get tableName() { return 'gv_pro_archivos'; }
}
class Pendientes extends Bookshelf.Model {
    get tableName() { return 'gv_pro_pendientes'; }
}
class Avance extends Bookshelf.Model {
    get tableName() { return 'gv_pro_avance'; }
    get idAttribute() { return 'proId'; }
    bitacoras() {
        return this.hasMany(Bitacoras, 'proId').sortBy('fecha', 'desc');
    }
    pendientes() {
        return this.hasMany(Pendientes, 'proId');
    }
}
class Informacion extends Bookshelf.Model {
    get tableName() { return 'gv_pro_informacion'; }
}
class Mensajes extends Bookshelf.Model {
    get tableName() { return 'gv_pro_mensaje'; }
}
class Calidad extends Bookshelf.Model {
    get tableName() { return 'gv_pro_calidad'; }
}
class Riesgos extends Bookshelf.Model {
    get tableName() { return 'gv_pro_riesgo'; }
}
Proyecto.collection().fetch();
