"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const knex_1 = __importDefault(require("knex"));
exports.knexGV = knex_1.default({
    client: 'mysql',
    connection: {
        port: 3306,
        user: 'root',
        host: '172.19.0.26',
        password: 'root',
        // host : 'localhost',
        // password : '12345678',
        database: 'bd_gv_proyectos_viales'
    }
});
exports.bookshelf = require('bookshelf')(exports.knexGV);
