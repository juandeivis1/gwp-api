"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Messaging_1 = require("../../Messaging/Messaging");
const Mensajes_1 = __importDefault(require("../../Modelos/Mensajes/Mensajes"));
const Proyectos_functions_1 = require("../Proyectos/Proyectos.functions");
const Functions_common_1 = require("../Functions/Functions.common");
exports.nuevoMensaje = (request, response) => __awaiter(void 0, void 0, void 0, function* () {
    let { geolocation, authorization } = request.headers;
    let { mensaje } = request.body;
    let ts = Date.now();
    try {
        let nuevoMensaje = yield Mensajes_1.default.forge().save(mensaje).call('toJSON');
        const proyecto = yield Proyectos_functions_1.proById(mensaje.proId);
        let tokens = yield Functions_common_1.obtenerTokensEquipo(mensaje.proId);
        let resultado;
        tokens.length > 0 ?
            resultado = yield Messaging_1.sendNewMessageTo(proyecto.expediente, `${mensaje.usuario.toUpperCase()}: ${mensaje.mensaje}`, tokens, mensaje, mensaje.proId)
            : resultado = { successCount: 0 };
        response.json({ ts: ts, success: resultado });
        Proyectos_functions_1.proyectoActualizado(mensaje.proId, ts);
        Functions_common_1.actividadUsuario(mensaje.proId, authorization, { ts: ts, tipo: 'Nuevo mensaje', geolocation: geolocation, data: JSON.stringify(mensaje) });
    }
    catch (err) {
        console.log(err);
        response.status(500).json({ mpz: 'Error al insertar mensaje en MPZ! :(' });
    }
});
