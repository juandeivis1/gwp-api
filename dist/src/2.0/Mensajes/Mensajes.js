"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const express = __importStar(require("express"));
const Mensajes_functions_1 = require("./Mensajes.functions");
let mensajesRouter2 = express.Router();
exports.mensajes2 = mensajesRouter2;
mensajesRouter2.post("/nuevo", Mensajes_functions_1.nuevoMensaje);
