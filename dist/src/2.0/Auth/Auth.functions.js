"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const axios = __importStar(require("axios"));
const uuid_token_generator_1 = __importDefault(require("uuid-token-generator"));
const Usuario_1 = __importDefault(require("../../Modelos/Usuario/Usuario"));
const Messaging_1 = require("../../Messaging/Messaging");
const Token_1 = __importDefault(require("../../Modelos/Token/Token"));
const tokgen = new uuid_token_generator_1.default(512);
exports.authChecker = (req, res, next) => {
    if (req.headers.authorization == 'undefined') {
        let originalUrl = req.originalUrl;
        if (originalUrl === "/auth2/") {
            next();
        }
        else {
            res.status(401).json({ mpz: 'Autorización invalida!!!' });
        }
    }
    else if (req.headers.geolocation == 'undefined') {
        res.status(401).json({ mpz: 'Geolocalización requerida para GWPVI!' });
    }
    else {
        exports.tokenValidity(req, res, next);
    }
};
exports.tokenValidity = (req, response, next) => {
    let { authorization } = req.headers;
    exports.tokenIdByToken(authorization)
        .then(token => {
        let ts = Date.now();
        if (token.lastAccess) {
            if ((ts - token.lastAccess) < 2592000000) {
                updateValidity(token.idToken, ts.toString()); //escribir nuevo ts
                next();
            }
            else {
                invalidarToken(token.idToken, 'Token expirado');
                response.status(401).json({ mpz: 'Token expirado!' });
            }
        }
        else {
            response.status(401).json({ mpz: 'Token invalido!' });
        }
    })
        .catch(err => {
        response.status(401).json({ mpz: 'No posee Token para consulta!' });
    });
};
exports.tokenIdByToken = (authorization) => __awaiter(void 0, void 0, void 0, function* () {
    return Token_1.default
        .forge({ token: authorization })
        .fetch({ columns: ['idToken', 'lastAccess', 'invalidated'] })
        .call('toJSON');
});
const updateValidity = (idToken, ts) => {
    Token_1.default
        .where({ idToken })
        .save({ lastAccess: ts }, { patch: true })
        .call('toJSON')
        .then(user => { })
        .catch(err => {
        console.log(`Error al actualizar la validez ${idToken}`, err, '\n');
    });
};
const invalidarToken = (idToken, razon) => {
    let ts = Date.now();
    Token_1.default
        .where({ idToken })
        .save({ reason: razon, invalidated: ts }, { patch: true })
        .call('toJSON')
        .then(user => { })
        .catch(err => {
        console.log(`Error al invalidar token ${idToken}`, err, '\n');
    });
};
exports.autenticarUsuario = (request, response) => {
    let { user, password } = request.body;
    axios.default
        .post("http://172.19.0.60:2300/Auth_API.asmx/Autenticar", `x_user=${user}&x_password=${password}`)
        .then(res => {
        usuarioActivo(user, response);
    })
        .catch(err => {
        response.status(500).json({ mpz: 'Credenciales invalidas!' });
    });
};
const usuarioActivo = (user, response) => {
    Usuario_1.default
        .forge({ user })
        .fetch({ columns: ['userId', 'permissions', 'active'] })
        .call('toJSON')
        .then(_user => {
        _user.active ? crearToken(_user, response) : response.status(500).json({ mpz: 'Usuario sin acceso' });
    })
        .catch((err) => {
        console.log(err);
        response.status(500).json({ mpz: 'Error al consultar usuario' });
    });
};
const crearToken = (user, response) => {
    let tokenAcceso = tokgen.generate();
    let ts = Date.now();
    Token_1.default
        .forge()
        .save({ userId: user.userId, token: tokenAcceso, ts: ts, lastAccess: ts })
        .call('toJSON')
        .then(() => {
        response.json({ token: tokenAcceso, permisos: JSON.parse(user.permissions), nombre: user.name });
    })
        .catch(err => {
        console.log(err);
        response.status(500).json({ mpz: 'Error al registrar token de acceso!' });
    });
};
exports.logout = (request, response) => {
    let dev = request.headers.origin == 'http://localhost:4200';
    let { token, fcm } = request.body;
    Token_1.default
        .forge({ token })
        .fetch({
        columns: ['idToken']
    })
        .call('toJSON')
        .then(token => {
        invalidarToken(token.idToken, 'Logout');
    })
        .catch(err => {
        response.status(401).json({ mpz: 'No posee Token para consulta!' });
    });
    removerFCM(fcm, dev);
    response.json(null);
};
const removerFCM = (fcmToken, dev) => {
    Messaging_1.unsubscribeToUpdates(fcmToken, dev);
};
exports.registarFCM = (request, response) => __awaiter(void 0, void 0, void 0, function* () {
    let dev = request.headers.origin == 'http://localhost:4200';
    let { authorization } = request.headers;
    let { fcmToken } = request.body;
    yield Token_1.default
        .where({ token: authorization })
        .save({ fcmToken: fcmToken }, { patch: true });
    let usuario = yield Token_1.default
        .where({ token: authorization })
        .fetch({ withRelated: ['user'] })
        .call('toJSON');
    response.json(usuario.user.permissions);
    Messaging_1.subscribeToUpdates(fcmToken, dev);
});
exports.nuevoFCM = (request, response) => __awaiter(void 0, void 0, void 0, function* () {
    let { oldFcmToken, newFcmToken } = request.body;
    let dev = request.headers.origin == 'http://localhost:4200';
    let { authorization } = request.headers;
    yield Token_1.default
        .where({ token: authorization })
        .save({ fcmToken: newFcmToken }, { patch: true });
    response.json(newFcmToken);
    Messaging_1.subscribeToUpdates(newFcmToken, dev);
    Messaging_1.unsubscribeToUpdates(oldFcmToken, dev);
});
