"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const express = __importStar(require("express"));
const Auth_functions_1 = require("./Auth.functions");
const authRouter2 = express.Router();
exports.auth2 = authRouter2;
authRouter2.post("/", Auth_functions_1.autenticarUsuario);
authRouter2.post("/logout", Auth_functions_1.logout);
authRouter2.post("/fcm", Auth_functions_1.registarFCM);
authRouter2.post("/newfcm", Auth_functions_1.nuevoFCM);
