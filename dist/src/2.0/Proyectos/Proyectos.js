"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const express = __importStar(require("express"));
const Proyectos_functions_1 = require("./Proyectos.functions");
let proyectosRouter2 = express.Router();
exports.proyectos2 = proyectosRouter2;
proyectosRouter2.get("/todos", Proyectos_functions_1.sincronizarProyectos);
proyectosRouter2.post("/sincronizar", Proyectos_functions_1.sincronizarProyecto);
proyectosRouter2.post("/portafolio", Proyectos_functions_1.sincronizarPortafolio);
proyectosRouter2.post("/registrar", Proyectos_functions_1.registrarProyecto);
proyectosRouter2.post("/actualizar", Proyectos_functions_1.actualizarProyecto);
