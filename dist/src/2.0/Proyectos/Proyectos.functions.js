"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Messaging_1 = require("../../Messaging/Messaging");
const Proyecto_1 = __importDefault(require("../../Modelos/Proyecto/Proyecto"));
const Functions_common_1 = require("../Functions/Functions.common");
const Security_1 = require("../../Security/Security");
// Funciones 2.0
exports.proById = (proId) => __awaiter(void 0, void 0, void 0, function* () {
    return Proyecto_1.default
        .where({ proId })
        .fetch({
        columns: Proyecto_1.default.columns,
        withRelated: Proyecto_1.default.relationships
    })
        .call('toJSON');
});
exports.sincronizarProyecto = (request, response) => __awaiter(void 0, void 0, void 0, function* () {
    const { authorization } = request.headers;
    const { proId } = request.body;
    let proyecto = yield Proyecto_1.default
        .where({ proId })
        .fetch({
        columns: Proyecto_1.default.columns,
        withRelated: Proyecto_1.default.relationships
    })
        .call('toJSON');
    if (proyecto.informacion.equipo) {
        proyecto.informacion.equipo = JSON.parse(proyecto.informacion.equipo);
    }
    if (proyecto.informacion.coordenadas) {
        proyecto.informacion.coordenadas = JSON.parse(proyecto.informacion.coordenadas);
    }
    const encryptedProyecto = Security_1.encryptByToken(proyecto, authorization);
    response.json({ proyecto: encryptedProyecto });
});
exports.sincronizarProyectos = (request, response) => __awaiter(void 0, void 0, void 0, function* () {
    let { authorization } = request.headers;
    let proyectos = yield Proyecto_1.default
        .forge()
        .orderBy('actualizado', 'desc')
        .fetchAll({
        columns: Proyecto_1.default.columns,
        withRelated: [
            'avance',
            'informacion'
        ]
    })
        .call('toJSON');
    proyectos = proyectos.map(proyecto => {
        proyecto.informacion.equipo = proyecto.informacion.equipo ? JSON.parse(proyecto.informacion.equipo) : null;
        proyecto.informacion.coordenadas = proyecto.informacion.coordenadas ? JSON.parse(proyecto.informacion.coordenadas) : null;
        return proyecto;
    });
    const encryptedProyectos = Security_1.encryptByToken(proyectos, authorization);
    response.json({ proyectos: encryptedProyectos });
});
exports.sincronizarPortafolio = (request, response) => __awaiter(void 0, void 0, void 0, function* () {
    let { portafolio } = request.body;
    let { authorization } = request.headers;
    let proyectos = yield Proyecto_1.default
        .forge()
        .where({ portafolio: portafolio })
        .orderBy('actualizado', 'desc')
        .fetchAll({ columns: Proyecto_1.default.columns, withRelated: ['bitacora', 'informacion'] })
        .call('toJSON');
    proyectos = proyectos.map(proyecto => {
        if (proyecto.informacion.equipo) {
            proyecto.informacion.equipo = JSON.parse(proyecto.informacion.equipo);
        }
        if (proyecto.informacion.coordenadas) {
            proyecto.informacion.coordenadas = JSON.parse(proyecto.informacion.coordenadas);
        }
        return proyecto;
    });
    const encryptedProyecto = Security_1.encryptByToken(proyectos, authorization);
    response.json({ proyectos: encryptedProyecto });
});
exports.proyectoActualizado = (proId, ts) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const proyecto = yield Proyecto_1.default
            .forge()
            .where({ proId: proId })
            .save({ actualizado: ts }, { patch: true })
            .call('toJSON');
    }
    catch (err) {
        console.log(err);
    }
});
exports.actualizarProyecto = (request, response) => __awaiter(void 0, void 0, void 0, function* () {
    let { geolocation, authorization } = request.headers;
    let { proId, proyecto } = request.body;
    let ts = Date.now();
    proyecto.actualizado = ts;
    try {
        yield Proyecto_1.default
            .where({ proId })
            .save(proyecto, { patch: true });
        response.json({ ts: ts });
        Functions_common_1.actividadUsuario(proId, authorization, { ts: ts, tipo: 'Actualización proyecto', geolocation: geolocation, data: JSON.stringify(proyecto) });
    }
    catch (err) {
        console.log(`'Error al actualizar el proyecto ${proId}, ${proyecto}, ${ts}`, err, '\n');
        response.status(500).json({ mpz: 'Error al actualizar el proyecto de MPZ! :(' });
    }
});
exports.registrarProyecto = (request, response) => __awaiter(void 0, void 0, void 0, function* () {
    let dev = request.headers.origin == 'http://localhost:4200';
    let { geolocation, authorization } = request.headers;
    let { proyecto } = request.body;
    let ts = Date.now();
    proyecto.creado = ts;
    try {
        let nuevoProyecto = yield Proyecto_1.default
            .forge()
            .save(proyecto)
            .call('toJSON');
        response.json({ proId: nuevoProyecto.proId, ts: ts });
        Messaging_1.enviarNuevoProyecto(nuevoProyecto.sinonimo, nuevoProyecto.expediente, nuevoProyecto.proId, dev);
        Functions_common_1.actividadUsuario(nuevoProyecto.proId, authorization, { ts: ts, tipo: 'Creación proycto', geolocation: geolocation, data: JSON.stringify(proyecto) });
    }
    catch (err) {
        console.log(`call insertPro(${proyecto},${ts})`, new Date(), err, '\n');
        response.status(500).json({ mpz: 'Error al crear proyecto en MPZ! :(' });
    }
});
