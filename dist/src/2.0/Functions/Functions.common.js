"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Actividad_1 = __importDefault(require("../../Modelos/Actividad/Actividad"));
const Auth_functions_1 = require("../Auth/Auth.functions");
const Token_1 = __importDefault(require("../../Modelos/Token/Token"));
const Informacion_1 = __importDefault(require("../../Modelos/Informacion/Informacion"));
exports.actividadUsuario = (proId, authorization, actividad) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let infoToken = yield Auth_functions_1.tokenIdByToken(authorization);
        insertarActividad(proId, actividad, infoToken.idToken);
    }
    catch (error) {
        console.log(`Error al obtener el token '${authorization}`, actividad, error, '\n');
    }
});
const insertarActividad = (proId, actividad, idToken) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let listo = yield Actividad_1.default
            .forge()
            .save({
            idToken: idToken,
            proId: proId,
            ts: actividad.ts,
            tipo: actividad.tipo,
            geolocation: actividad.geolocation,
            data: actividad.data
        })
            .call('toJSON');
    }
    catch (error) {
        console.log(`Error al intentar insertar actividad: proId: '${proId}, idTokne: ${idToken}`, actividad, error, '\n');
    }
});
exports.obtenerTokens = (usuarios) => __awaiter(void 0, void 0, void 0, function* () {
    let ts = Date.now() - 3888000000;
    return yield Token_1.default
        .query(qb => {
        qb
            .select('fcmToken')
            .where('lastAccess', '>=', ts)
            .whereNull('invalidated')
            .leftJoin('gv_users', 'gv_users.userId', 'gv_tokens.userId')
            .andWhere('gv_users.user', 'in', usuarios)
            .whereNotNull('gv_tokens.fcmToken');
    })
        .fetchAll()
        .call('toJSON')
        .map(fcm => fcm.fcmToken);
});
exports.obtenerTokensEquipo = (proId) => __awaiter(void 0, void 0, void 0, function* () {
    let informacion = yield Informacion_1.default
        .forge({ proId: proId })
        .fetch()
        .call('toJSON');
    let usuarios = JSON.parse(informacion.equipo);
    usuarios = usuarios.map(user => user.nombre);
    return yield exports.obtenerTokens(usuarios);
});
