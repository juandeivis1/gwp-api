"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Messaging_1 = require("./../../Messaging/Messaging");
const Proyectos_functions_1 = require("../Proyectos/Proyectos.functions");
const Bitacora_1 = require("../../Modelos/Bitacora/Bitacora");
const Pendiente_1 = require("../../Modelos/Pendiente/Pendiente");
const Avance_1 = __importDefault(require("../../Modelos/Avance/Avance"));
const Functions_common_1 = require("../Functions/Functions.common");
exports.nuevoAvance = (request, response) => {
    let { proId, avance } = request.body;
    let ts = Date.now();
    existeAvance(avance, proId, ts, request, response);
};
const existeAvance = (avance, proId, ts, request, response) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { geolocation, authorization } = request.headers;
        let existe = yield Avance_1.default.forge({ proId }).fetch();
        existe && existe.toJSON() ?
            actualizar(avance, proId, ts, geolocation, authorization, response) :
            insertar(avance, proId, ts, geolocation, authorization, response);
    }
    catch (err) {
        console.log(`Error desconocido en el avance ${proId},'${avance}',${ts}`, new Date(), err, '\n');
        response.status(500).json({ mpz: 'Error al comprobar el avance en MPZ! :(' });
    }
});
const actualizar = (avance, proId, ts, geolocation, authorization, response) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let nuevoAvance = yield Avance_1.default
            .where({ proId })
            .save(Object.assign({}, avance), { patch: true })
            .call('toJSON');
        response.json({ ts: ts });
        Proyectos_functions_1.proyectoActualizado(proId, ts);
        Functions_common_1.actividadUsuario(proId, authorization, { ts: ts, tipo: 'Nuevo avance', geolocation: geolocation, data: JSON.stringify(avance) });
    }
    catch (err) {
        console.log(`Error al actualizar el avance: ${proId},'${avance}',${ts})`, new Date(), err, '\n');
        response.status(500).json({ mpz: 'Error al actualizar el avance en MPZ! :(' });
    }
});
const insertar = (avance, proId, ts, geolocation, authorization, response) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let newAvance = Object.assign(Object.assign({}, avance), { proId: proId });
        let nuevoAvance = yield Avance_1.default
            .forge()
            .save(newAvance)
            .call('toJSON');
        response.json({ ts: ts });
        Proyectos_functions_1.proyectoActualizado(proId, ts);
        Functions_common_1.actividadUsuario(proId, authorization, { ts: ts, tipo: 'Creación avance', geolocation: geolocation, data: JSON.stringify(avance) });
    }
    catch (err) {
        console.log(`Error al insertar avance ${proId},'${avance}',${ts} `, new Date(), err, '\n');
        response.status(500).json({ mpz: 'Error al insertar el avance en MPZ! :(' });
    }
});
exports.nuevaBitacora = (request, response) => __awaiter(void 0, void 0, void 0, function* () {
    let dev = request.headers.origin == 'http://localhost:4200';
    let { geolocation, authorization } = request.headers;
    let { proId, bitacora } = request.body;
    let ts = Date.now();
    bitacora = Object.assign(Object.assign({}, bitacora), { proId: proId });
    try {
        let nuevaBitacora = yield Bitacora_1.Bitacora
            .forge()
            .save(bitacora)
            .call('toJSON');
        response.json(ts);
        const proyecto = yield Proyectos_functions_1.proById(proId);
        const body = `${bitacora.usuario.toUpperCase()}: ${bitacora.linea}`;
        Messaging_1.enviarNuevaBitacora(body, proyecto.expediente, proId);
        Proyectos_functions_1.proyectoActualizado(proId, ts);
        Functions_common_1.actividadUsuario(proId, authorization, { ts: ts, tipo: 'Nuevo bitacora', geolocation: geolocation, data: JSON.stringify(bitacora) });
    }
    catch (err) {
        console.log(`Error al insertar la bitacora: ${proId}, ${bitacora}`, ts, err, '\n');
        response.status(500).json({ mpz: 'Error al insertar la bitacora en MPZ! :(' });
    }
});
exports.pendienteNuevo = (request, response) => __awaiter(void 0, void 0, void 0, function* () {
    let { geolocation, authorization } = request.headers;
    let { proId, pendiente } = request.body;
    let ts = Date.now();
    try {
        let nuevoPendiente = yield Pendiente_1.Pendiente
            .forge()
            .save(Object.assign({ proId: proId }, pendiente))
            .call('toJSON');
        pendiente.pendienteId = nuevoPendiente.id;
        delete pendiente.id;
        response.json({ ts: ts, pendiente: pendiente });
        Proyectos_functions_1.proyectoActualizado(proId, ts);
        Functions_common_1.actividadUsuario(proId, authorization, { ts: ts, tipo: 'Nuevo pendiente', geolocation: geolocation, data: JSON.stringify(pendiente) });
    }
    catch (err) {
        console.log(`'Error al insertar pendiente: ${pendiente}, proId: ${proId}`, new Date(), err, '\n');
        response.status(500).json({ mpz: 'Error al insertar pendiente! :(' });
    }
});
exports.pendienteAtendido = (request, response) => __awaiter(void 0, void 0, void 0, function* () {
    let { geolocation, authorization } = request.headers;
    let { pendienteId, resuelto, proId } = request.body;
    let ts = Date.now();
    try {
        let pendienteFinalizdo = yield Pendiente_1.Pendiente
            .where({ pendienteId })
            .save({ resuelto: resuelto }, { patch: true })
            .call('toJSON');
        response.json(ts);
        Proyectos_functions_1.proyectoActualizado(proId, ts);
        Functions_common_1.actividadUsuario(proId, authorization, { ts: ts, tipo: 'Pendiente finalizado', geolocation: geolocation, data: JSON.stringify({ pendienteId: pendienteId, resuelto: resuelto }) });
    }
    catch (err) {
        console.log(`'Error al finalizar pendiente: ${pendienteId}`, ts, err, '\n');
        response.status(500).json({ mpz: 'Error al finalizar pendiente! :(' });
    }
});
