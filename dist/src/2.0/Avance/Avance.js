"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const express = __importStar(require("express"));
const Avance_functions_1 = require("./Avance.functions");
let avanceRouter2 = express.Router();
exports.avance2 = avanceRouter2;
avanceRouter2.post("/actualizar", Avance_functions_1.nuevoAvance);
avanceRouter2.post("/bitacora", Avance_functions_1.nuevaBitacora);
avanceRouter2.post("/pendiente", Avance_functions_1.pendienteNuevo);
avanceRouter2.post("/pendiente/atendido", Avance_functions_1.pendienteAtendido);
