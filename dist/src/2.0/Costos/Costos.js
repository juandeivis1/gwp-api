"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const express = __importStar(require("express"));
const Costos_functions_1 = require("./Costos.functions");
let costosRouter2 = express.Router();
exports.costos2 = costosRouter2;
costosRouter2.post("/actualizar", Costos_functions_1.nuevosCostos);
