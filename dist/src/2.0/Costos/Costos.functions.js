"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Functions_common_1 = require("../Functions/Functions.common");
const Costos_1 = __importDefault(require("../../Modelos/Costos/Costos"));
const Proyectos_functions_1 = require("../Proyectos/Proyectos.functions");
exports.nuevosCostos = (request, response) => {
    let { proId, costos } = request.body;
    let ts = Date.now();
    existeCostos(costos, proId, ts, request, response);
};
const existeCostos = (costos, proId, ts, request, response) => __awaiter(void 0, void 0, void 0, function* () {
    let { geolocation, authorization } = request.headers;
    let existe = yield Costos_1.default.forge({ proId }).fetch();
    existe && existe.toJSON() ?
        actualizar(costos, proId, ts, geolocation, authorization, response)
        : insertar(costos, proId, ts, geolocation, authorization, response);
});
const actualizar = (costos, proId, ts, geolocation, authorization, response) => __awaiter(void 0, void 0, void 0, function* () {
    delete costos.proId;
    try {
        let nuevoSeguimiento = yield Costos_1.default
            .where({ proId })
            .save(Object.assign({}, costos), { patch: true })
            .call('toJSON');
        response.json({ ts: ts });
        Proyectos_functions_1.proyectoActualizado(proId, ts);
        Functions_common_1.actividadUsuario(proId, authorization, { ts: ts, tipo: 'Nuevos costos', geolocation: geolocation, data: JSON.stringify(costos) });
    }
    catch (err) {
        console.log(`Error al actualizar el seguimiento (${proId},'${costos}',${ts})`, err, '\n');
        response.status(500).json({ mpz: 'Error al actualizar el seguimiento en MPZ! :(' });
    }
});
const insertar = (costos, proId, ts, geolocation, authorization, response) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let newseguimiento = Object.assign(Object.assign({}, costos), { proId: proId });
        let nuevoSeguimiento = yield Costos_1.default
            .forge()
            .save(newseguimiento)
            .call('toJSON');
        response.json({ ts: ts });
        Proyectos_functions_1.proyectoActualizado(proId, ts);
        Functions_common_1.actividadUsuario(proId, authorization, { ts: ts, tipo: 'Creación costos', geolocation: geolocation, data: JSON.stringify(costos) });
    }
    catch (err) {
        console.log(`Error al insertar el seguimiento (${proId},'${costos}',${ts})`, err, '\n');
        response.status(500).json({ mpz: 'Error al insertar el seguimiento en MPZ! :(' });
    }
});
