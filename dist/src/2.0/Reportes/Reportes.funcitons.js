"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Proyecto_1 = __importDefault(require("../../Modelos/Proyecto/Proyecto"));
const Pendiente_1 = require("../../Modelos/Pendiente/Pendiente");
const Security_1 = require("../../Security/Security");
exports.reporteSeguimiento = (request, response) => __awaiter(void 0, void 0, void 0, function* () {
    const { ejecutada, ejecutar } = request.body;
    let { authorization } = request.headers;
    let ts = Date.now();
    try {
        let filtrado = yield Proyecto_1.default
            .query(qb => {
            qb
                .select('gv_pro.proId', 'gv_pro.nombre', 'gv_pro_informacion.fase', 'gv_pro.expediente', 'gv_pro_seguimiento.ejecutado', 'gv_pro_seguimiento.ejecutar')
                .leftJoin('gv_pro_seguimiento', 'gv_pro_seguimiento.proId', 'gv_pro.proId')
                .leftJoin('gv_pro_informacion', 'gv_pro_informacion.proId', 'gv_pro.proId');
            if (ejecutada.length > 0) {
                qb.where('gv_pro_seguimiento.ejecutado', 'in', ejecutada);
            }
            if (ejecutar.length > 0) {
                qb.where('gv_pro_seguimiento.ejecutar', 'in', ejecutar);
            }
        })
            .fetchAll()
            .call('toJSON');
        const encryptedSeguimiento = Security_1.encryptByToken(filtrado, authorization);
        response.json({ seguimiento: encryptedSeguimiento });
    }
    catch (err) {
        console.log(`Reporte de seguimiento`, ts, err, '\n');
        response.status(500).json({ mpz: 'Error al generar reporte de seguimiento en MPZ! :(' });
    }
});
exports.reporteFase = (request, response) => __awaiter(void 0, void 0, void 0, function* () {
    const { fase, portafolio, ingenieria } = request.body;
    let ts = Date.now();
    try {
        let filtrado = yield Proyecto_1.default
            .query(qb => {
            qb
                .select('gv_pro.proId', 'gv_pro.nombre', 'gv_pro_avance.porcentaje', 'gv_pro.expediente', 'gv_pro.portafolio', 'gv_pro_informacion.equipo')
                .leftJoin('gv_pro_informacion', 'gv_pro_informacion.proId', 'gv_pro.proId')
                .leftJoin('gv_pro_avance', 'gv_pro_avance.proId', 'gv_pro.proId')
                .where('gv_pro_informacion.fase', fase)
                .andWhere('gv_pro.portafolio', 'in', portafolio);
        })
            .fetchAll({ withRelated: ['bitacora'] })
            .call('toJSON');
        filtrado = filtrado.map(_proyecto => {
            if (_proyecto.equipo) {
                let equipo = JSON.parse(_proyecto.equipo);
                let ingeniero = equipo.find(_miembro => _miembro.rol == 'Ingeniería');
                ingeniero ? _proyecto.ingeniero = ingeniero.nombre : null;
            }
            delete _proyecto.equipo;
            _proyecto.bitacora = _proyecto.bitacora.slice(-2);
            return _proyecto;
        });
        if (ingenieria.length > 0) {
            filtrado = filtrado.filter(_proyecto => ingenieria.includes(_proyecto.ingeniero));
        }
        response.json(filtrado);
    }
    catch (err) {
        console.log(`Reporte de seguimiento`, ts, err, '\n');
        response.status(500).json({ mpz: 'Error al generar reporte de seguimiento en MPZ! :(' });
    }
});
exports.reportePrioridad = (request, response) => __awaiter(void 0, void 0, void 0, function* () {
    const { portafolio, prioridad } = request.body;
    try {
        let filtrado = yield Proyecto_1.default
            .query(qb => qb.where('portafolio', portafolio).whereIn('prioridad', prioridad))
            .fetchAll()
            .call('toJSON');
        response.json(filtrado);
    }
    catch (err) {
        console.log(`Reporte por prioridad`, new Date(), err, '\n');
        response.status(500).json({ mpz: 'Error al generar reporte por prioridad en MPZ! :(' });
    }
});
exports.reporteCostosPortafolio = (request, response) => __awaiter(void 0, void 0, void 0, function* () {
    let { authorization } = request.headers;
    try {
        let proyectos = yield Proyecto_1.default
            .fetchAll({ withRelated: ['costos', 'informacion', 'avance'] })
            .call('toJSON');
        let grupos = proyectos.reduce((lista, proyecto) => {
            lista.hasOwnProperty(proyecto.portafolio) ? null : lista[proyecto.portafolio] = [];
            lista[proyecto.portafolio].push(proyecto);
            return lista;
        }, {});
        let portafolios = Object.keys(grupos);
        let sumatoria = portafolios.map(portafolio => {
            let resumen = grupos[portafolio].reduce(sumatoriaProyecto, { avance: 0, prevision: 0, inversion: 0, cantidad: 0, fases: { preinversion: 0, detenido: 0, ejecucion: 0, operacion: 0 } });
            resumen.inversion = resumen.inversion * 1.05;
            resumen.avance = resumen.avance / resumen.cantidad;
            return { portafolio: portafolio, resumen: resumen };
        });
        const encryptedPortafolios = Security_1.encryptByToken(sumatoria, authorization);
        response.json({ portafolios: encryptedPortafolios });
    }
    catch (err) {
        console.log(`Reporte por prioridad`, new Date(), err, '\n');
        response.status(500).json({ mpz: 'Error al generar reporte en MPZ! :(' });
    }
});
exports.reporteCostosPrograma = (request, response) => __awaiter(void 0, void 0, void 0, function* () {
    let { authorization } = request.headers;
    try {
        let proyectos = yield Proyecto_1.default
            .fetchAll({ withRelated: ['costos', 'informacion', 'avance'] })
            .call('toJSON');
        let grupos = proyectos.reduce((lista, proyecto) => {
            lista.hasOwnProperty(proyecto.portafolio) ? null : lista[proyecto.portafolio] = [];
            lista[proyecto.portafolio].push(proyecto);
            return lista;
        }, {});
        let portafolios = Object.keys(grupos);
        let resultadoPortafolios = portafolios.reduce((portafolio, actual) => {
            let programas = grupos[actual].reduce((programas, proyecto) => {
                programas.hasOwnProperty(proyecto.programa) ? null : programas[proyecto.programa] = [];
                programas[proyecto.programa].push(proyecto);
                return programas;
            }, {});
            let programasPortafolio = Object.keys(programas);
            let programaSuma = programasPortafolio.map((programa) => {
                let suma = programas[programa].reduce(sumatoriaProyecto, { avance: 0, prevision: 0, inversion: 0, cantidad: 0, fases: { preinversion: 0, detenido: 0, ejecucion: 0, operacion: 0 } });
                suma.inversion = suma.inversion * 1.05;
                return { programa: programa, sumatoria: suma };
            });
            programaSuma.map(programa => programa.sumatoria.avance = programa.sumatoria.avance / programa.sumatoria.cantidad);
            portafolio.push({ portafolio: actual, sumatorias: programaSuma });
            return portafolio;
        }, []);
        const encryptedProgramas = Security_1.encryptByToken(resultadoPortafolios, authorization);
        response.json({ programas: encryptedProgramas });
    }
    catch (err) {
        console.log(`Reporte por prioridad`, new Date(), err, '\n');
        response.status(500).json({ mpz: 'Error al generar reporte en MPZ! :(' });
    }
});
const sumarFases = {
    'Preinversión': (sumatoria) => sumatoria.fases.preinversion += 1,
    'Operación': (sumatoria) => sumatoria.fases.operacion += 1,
    'Ejecución': (sumatoria) => sumatoria.fases.ejecucion += 1,
    'Detenido': (sumatoria) => sumatoria.fases.detenido += 1
};
const sumatoriaProyecto = (sumatoria, proyecto) => {
    proyecto.avance.porcentaje ? sumatoria.avance += Number(proyecto.avance.porcentaje) : null;
    proyecto.costos.prevision ? sumatoria.prevision += Number(proyecto.costos.prevision) : null;
    proyecto.costos.adjudicado ? sumatoria.inversion += Number(proyecto.costos.adjudicado) : null;
    proyecto.costos.calidad ? sumatoria.inversion += Number(proyecto.costos.calidad) : null;
    proyecto.costos.comunales ? sumatoria.inversion += Number(proyecto.costos.comunales) : null;
    proyecto.costos.ordenServicio ? sumatoria.inversion += Number(proyecto.costos.ordenServicio) : null;
    proyecto.costos.especie ? sumatoria.inversion += Number(proyecto.costos.especie) : null;
    proyecto.costos.especifica ? sumatoria.inversion += Number(proyecto.costos.especifica) : null;
    proyecto.costos.reajuste ? sumatoria.inversion += Number(proyecto.costos.reajuste) : null;
    proyecto.costos.ampliacion ? sumatoria.inversion += Number(proyecto.costos.ampliacion) : null;
    proyecto.costos.instituciones ? sumatoria.inversion += Number(proyecto.costos.instituciones) : null;
    proyecto.costos.extra1 ? sumatoria.inversion += Number(proyecto.costos.extra1) : null;
    proyecto.costos.extra2 ? sumatoria.inversion += Number(proyecto.costos.extra2) : null;
    sumatoria.cantidad += 1;
    proyecto.informacion.fase ? sumarFases[proyecto.informacion.fase](sumatoria) : null;
    return sumatoria;
};
const proyectosPendientes = () => __awaiter(void 0, void 0, void 0, function* () {
    return yield Pendiente_1.Pendiente.query(qb => qb.select('proId').groupBy('proId').whereNull('resuelto')).fetchAll().call('toJSON');
});
exports.reportePendientes = (request, resp) => __awaiter(void 0, void 0, void 0, function* () {
    let { authorization } = request.headers;
    try {
        let proyectos = yield proyectosPendientes();
        proyectos = proyectos.map(pro => pro.proId);
        let pendientes = yield Proyecto_1.default
            .query(qb => {
            qb.whereIn('proId', proyectos);
        })
            .fetchAll({ withRelated: ['pendientes'] })
            .call('toJSON');
        const encryptedPendientes = Security_1.encryptByToken(pendientes, authorization);
        resp.json({ pendientes: encryptedPendientes });
    }
    catch (err) {
        console.log(`Reporte por pendientes`, new Date(), err, '\n');
        resp.status(500).json({ mpz: 'Error al generar reporte en MPZ! :(' });
    }
});
const modalidadesBase = ['Obra por Contrato', 'Administración', 'Mixto', 'Sin clasificar'];
exports.reporteCostosModalidad = (request, response) => __awaiter(void 0, void 0, void 0, function* () {
    let { authorization } = request.headers;
    try {
        let proyectos = yield Proyecto_1.default
            .fetchAll({ withRelated: ['costos', 'informacion', 'avance'] })
            .call('toJSON');
        let gruposModalidades = modalidadesBase.reduce((modalidades, actual) => {
            modalidades[actual] = [];
            return modalidades;
        }, {});
        let proyectosPorModalidad = proyectos.reduce((lista, proyecto) => {
            let proModalidad = proyecto.informacion.modalidad;
            proModalidad ? lista[proModalidad] = [...lista[proModalidad], proyecto] : lista['Sin clasificar'] = [...lista['Sin clasificar'], proyecto];
            return lista;
        }, gruposModalidades);
        let resultadoModalidades = modalidadesBase.reduce((_modalidades, actual) => {
            let portafolios = proyectosPorModalidad[actual].reduce(portafoliosAgrupar, {});
            let portafoliosProyectos = Object.keys(portafolios);
            let protafolios = portafoliosProyectos.reduce((programas, portafolio) => {
                let programasProyectos = portafolios[portafolio].reduce(programaAgrupar, {});
                let programasPortafolio = Object.keys(programasProyectos);
                let programaSuma = programasPortafolio.map((programa) => {
                    let suma = programasProyectos[programa].reduce(sumatoriaProyecto, { avance: 0, prevision: 0, inversion: 0, cantidad: 0, fases: { preinversion: 0, detenido: 0, ejecucion: 0, operacion: 0 } });
                    // suma.inversion = suma.inversion * 1.05
                    return { programa: programa, sumatoria: suma };
                });
                programaSuma.map(programa => programa.sumatoria.avance = programa.sumatoria.avance / programa.sumatoria.cantidad);
                let sumatoria = programaSuma.reduce(sumatoriaProgramas, { prevision: 0, inversion: 0, cantidad: 0, fases: { preinversion: 0, detenido: 0, ejecucion: 0, operacion: 0 } });
                programas = [...programas, { portafolio: portafolio, programas: programaSuma, sumatoria: sumatoria }];
                return programas;
            }, []);
            let sumatoria = protafolios.reduce(sumatoriaPortafolios, { prevision: 0, inversion: 0, cantidad: 0, fases: { preinversion: 0, detenido: 0, ejecucion: 0, operacion: 0 } });
            _modalidades = [..._modalidades, { modalidad: actual, portafolios: protafolios, sumatoria: sumatoria }];
            return _modalidades;
        }, []);
        const encryptedModalidades = Security_1.encryptByToken(resultadoModalidades, authorization);
        response.json({ modalidades: encryptedModalidades });
    }
    catch (err) {
        console.log(`Reporte por modalidad`, new Date(), err, '\n');
        response.status(500).json({ mpz: 'Error al generar reporte en MPZ! :(' });
    }
});
const sumatoriaPortafolios = (sumatoria, actual) => {
    // sumatoria.avance += actual.sumatoria.avance
    sumatoria.cantidad += actual.sumatoria.cantidad;
    sumatoria.inversion += actual.sumatoria.inversion;
    sumatoria.prevision += actual.sumatoria.prevision;
    sumatoria.fases.preinversion += actual.sumatoria.fases.preinversion;
    sumatoria.fases.detenido += actual.sumatoria.fases.detenido;
    sumatoria.fases.ejecucion += actual.sumatoria.fases.ejecucion;
    sumatoria.fases.operacion += actual.sumatoria.fases.operacion;
    return sumatoria;
};
const sumatoriaProgramas = (sumatoria, actual) => {
    // sumatoria.avance += actual.sumatoria.avance
    sumatoria.cantidad += actual.sumatoria.cantidad;
    sumatoria.inversion += actual.sumatoria.inversion;
    sumatoria.prevision += actual.sumatoria.prevision;
    sumatoria.fases.preinversion += actual.sumatoria.fases.preinversion;
    sumatoria.fases.detenido += actual.sumatoria.fases.detenido;
    sumatoria.fases.ejecucion += actual.sumatoria.fases.ejecucion;
    sumatoria.fases.operacion += actual.sumatoria.fases.operacion;
    return sumatoria;
};
const portafoliosAgrupar = (portafolios, proyecto) => {
    let portafolio = proyecto.portafolio;
    portafolios.hasOwnProperty(portafolio) ? null : portafolios[portafolio] = [];
    portafolios[portafolio] = [...portafolios[portafolio], proyecto];
    return portafolios;
};
const programaAgrupar = (programas, proyecto) => {
    let programa = proyecto.programa;
    programas.hasOwnProperty(programa) ? null : programas[programa] = [];
    programas[programa] = [...programas[programa], proyecto];
    return programas;
};
exports.reporteProyectosPorFecha = (request, response) => __awaiter(void 0, void 0, void 0, function* () {
    let { authorization } = request.headers;
    const { inicioA, inicioB, finA, finB, portafolio, programa } = request.body;
    let proyectos = yield Proyecto_1.default
        .query(pro => {
        pro
            .leftJoin('gv_pro_costos', 'gv_pro_costos.proId', '.gv_pro.proId')
            .leftJoin('gv_pro_avance', 'gv_pro_avance.proId', '.gv_pro.proId')
            .leftJoin('gv_pro_informacion', 'gv_pro_informacion.proId', '.gv_pro.proId')
            .select('gv_pro.proId', 'gv_pro.expediente', 'gv_pro.portafolio', 'gv_pro.nombre', 'gv_pro.sinonimo')
            .select('gv_pro_informacion.poblacion');
        if (inicioA) {
            pro.where('gv_pro_avance.inicio', '>=', inicioA);
        }
        if (inicioB) {
            pro.where('gv_pro_avance.inicio', '<=', inicioB);
        }
        if (finA) {
            pro.where('gv_pro_avance.fin', '>=', finA);
        }
        if (finB) {
            pro.where('gv_pro_avance.fin', '<=', finB);
        }
        if (portafolio && portafolio.length > 0) {
            pro.andWhere('gv_pro.portafolio', 'in', portafolio);
        }
        if (programa && programa.length > 0) {
            pro.andWhere('gv_pro.programa', 'in', programa);
        }
    })
        .fetchAll({ withRelated: ['costos'] })
        .call('toJSON');
    const sumatoria = proyectos
        .reduce(sumatoriaCostoReal, { poblacion: 0, costoReal: 0 });
    const encrypted = Security_1.encryptByToken({ proyectos, sumatoria }, authorization);
    response.json(encrypted);
});
const sumatoriaCostoReal = (sumatoria, proyecto) => {
    proyecto.costoReal = 0;
    if (proyecto.poblacion) {
        sumatoria.poblacion += proyecto.poblacion;
    }
    if (proyecto.costos.adjudicado) {
        proyecto.costoReal += proyecto.costos.adjudicado;
    }
    if (proyecto.costos.calidad) {
        proyecto.costoReal += proyecto.costos.calidad;
    }
    if (proyecto.costos.comunales) {
        proyecto.costoReal += proyecto.costos.comunales;
    }
    if (proyecto.costos.ordenServicio) {
        proyecto.costoReal += proyecto.costos.ordenServicio;
    }
    if (proyecto.costos.especie) {
        proyecto.costoReal += proyecto.costos.especie;
    }
    if (proyecto.costos.especifica) {
        proyecto.costoReal += proyecto.costos.especifica;
    }
    if (proyecto.costos.reajuste) {
        proyecto.costoReal += proyecto.costos.reajuste;
    }
    if (proyecto.costos.ampliacion) {
        proyecto.costoReal += proyecto.costos.ampliacion;
    }
    if (proyecto.costos.instituciones) {
        proyecto.costoReal += proyecto.costos.instituciones;
    }
    if (proyecto.costos.extra1) {
        proyecto.costoReal += proyecto.costos.extra1;
    }
    if (proyecto.costos.extra2) {
        proyecto.costoReal += proyecto.costos.extra2;
    }
    if (proyecto.costoReal > 0) {
        proyecto.costoReal += (proyecto.costoReal * 0.05);
    }
    sumatoria.costoReal += proyecto.costoReal;
    if (sumatoria.proyectos) {
        sumatoria.proyectos.push(proyecto);
    }
    return sumatoria;
};
exports.reporteProyectosPorPortafolioPrograma = (request, response) => __awaiter(void 0, void 0, void 0, function* () {
    let { authorization } = request.headers;
    const { portafolio, programa } = request.body;
    let proyectos = yield Proyecto_1.default
        .query(pro => {
        pro
            .leftJoin('gv_pro_costos', 'gv_pro_costos.proId', '.gv_pro.proId')
            .leftJoin('gv_pro_avance', 'gv_pro_avance.proId', '.gv_pro.proId')
            .leftJoin('gv_pro_informacion', 'gv_pro_informacion.proId', '.gv_pro.proId')
            .select('gv_pro.proId', 'gv_pro.expediente', 'gv_pro.portafolio', 'gv_pro.nombre', 'gv_pro.sinonimo', 'gv_pro.programa')
            .select('gv_pro_informacion.poblacion')
            .select('gv_pro_avance.porcentaje');
        if (portafolio && portafolio.length > 0) {
            pro.andWhere('gv_pro.portafolio', 'in', portafolio);
        }
        if (programa && programa.length > 0) {
            pro.andWhere('gv_pro.programa', 'in', programa);
        }
    })
        .fetchAll({ withRelated: ['costos'] })
        .call('toJSON');
    let grupos = proyectos.reduce((lista, proyecto) => {
        lista.hasOwnProperty(proyecto.portafolio) ? null : lista[proyecto.portafolio] = [];
        lista[proyecto.portafolio].push(proyecto);
        return lista;
    }, {});
    let portafolios = Object.keys(grupos);
    let resultadoPortafolios = portafolios.reduce((portafolio, actual) => {
        let programas = grupos[actual].reduce((programas, proyecto) => {
            programas.hasOwnProperty(proyecto.programa) ? null : programas[proyecto.programa] = [];
            programas[proyecto.programa].push(proyecto);
            return programas;
        }, {});
        let programasPortafolio = Object.keys(programas);
        let programaSuma = programasPortafolio.map((programa) => {
            let suma = programas[programa].reduce(sumatoriaCostoReal, { poblacion: 0, costoReal: 0, proyectos: [] });
            return { programa: programa, sumatoria: suma };
        });
        const sumatoriaPrograma = programaSuma.reduce((totalPortafolio, actual) => {
            totalPortafolio.poblacion += actual.sumatoria.poblacion;
            totalPortafolio.costoReal += actual.sumatoria.costoReal;
            totalPortafolio.volumen += actual.sumatoria.proyectos.length;
            return totalPortafolio;
        }, { costoReal: 0, poblacion: 0, volumen: 0 });
        portafolio.push({ portafolio: actual, sumatoria: sumatoriaPrograma, sumatorias: programaSuma });
        return portafolio;
    }, []);
    const total = resultadoPortafolios.reduce((total, actual) => {
        total.poblacion += actual.sumatoria.poblacion;
        total.costoReal += actual.sumatoria.costoReal;
        total.volumen += actual.sumatoria.volumen;
        return total;
    }, { costoReal: 0, poblacion: 0, volumen: 0 });
    const encrypted = Security_1.encryptByToken({ portafolios: resultadoPortafolios, totalidad: total }, authorization);
    response.json(encrypted);
});
