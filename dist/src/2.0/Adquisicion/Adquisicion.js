"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const express = __importStar(require("express"));
const Adquisicion_functions_1 = require("./Adquisicion.functions");
let adquisicionRouter2 = express.Router();
exports.adquisicion2 = adquisicionRouter2;
adquisicionRouter2.post("/actualizar", Adquisicion_functions_1.nuevaAdquisicion);
