"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Proyectos_functions_1 = require("../Proyectos/Proyectos.functions");
const Adquisicion_1 = __importDefault(require("../../Modelos/Adquisicion/Adquisicion"));
const Functions_common_1 = require("../Functions/Functions.common");
exports.nuevaAdquisicion = (request, response) => {
    let { proId, adquisicion } = request.body;
    let ts = Date.now();
    existeAdquisicion(adquisicion, proId, ts, request, response);
};
const existeAdquisicion = (adquisicion, proId, ts, request, response) => __awaiter(void 0, void 0, void 0, function* () {
    let { geolocation, authorization } = request.headers;
    let existe = yield Adquisicion_1.default.forge({ proId }).fetch();
    existe && existe.toJSON() ?
        actualizar(adquisicion, proId, ts, geolocation, authorization, response)
        : insertar(adquisicion, proId, ts, geolocation, authorization, response);
});
const actualizar = (adquisicion, proId, ts, geolocation, authorization, response) => __awaiter(void 0, void 0, void 0, function* () {
    delete adquisicion.proId;
    try {
        yield Adquisicion_1.default
            .where({ proId })
            .save(adquisicion, { patch: true })
            .call('toJSON');
        response.json({ ts: ts });
        Proyectos_functions_1.proyectoActualizado(proId, ts);
        Functions_common_1.actividadUsuario(proId, authorization, { ts: ts, tipo: 'Nuevas adquisiciones', geolocation: geolocation, data: JSON.stringify(adquisicion) });
    }
    catch (err) {
        console.log(`Error al actualizar las adquisiciones ${proId},${adquisicion},${ts}`, err, '\n');
        response.status(500).json({ mpz: 'Error al actualizar las adquisiciones en MPZ! :(' });
    }
});
const insertar = (adquisicion, proId, ts, geolocation, authorization, response) => __awaiter(void 0, void 0, void 0, function* () {
    let newAdquisicion = Object.assign(Object.assign({}, adquisicion), { proId: proId });
    try {
        let nuevaAdquisicion = yield Adquisicion_1.default
            .forge()
            .save(newAdquisicion)
            .call('toJSON');
        response.json({ ts: ts });
        Proyectos_functions_1.proyectoActualizado(proId, ts);
        Functions_common_1.actividadUsuario(proId, authorization, { ts: ts, tipo: 'Creación adquisiciones', geolocation: geolocation, data: JSON.stringify(adquisicion) });
    }
    catch (err) {
        console.log(`'Error al insertar las adquisiciones ${proId},${adquisicion},${ts}`, err, '\n');
        response.status(500).json({ mpz: 'Error al insertar las adquisiciones en MPZ! :(' });
    }
});
