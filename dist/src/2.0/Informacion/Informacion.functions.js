"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Functions_common_1 = require("../Functions/Functions.common");
const Informacion_1 = __importDefault(require("../../Modelos/Informacion/Informacion"));
const Proyectos_functions_1 = require("../Proyectos/Proyectos.functions");
exports.nuevaInformacion = (request, response) => {
    let { proId, informacion } = request.body;
    let ts = Date.now();
    if (informacion.coordenadas) {
        informacion.coordenadas = JSON.stringify(informacion.coordenadas);
    }
    if (informacion.equipo) {
        informacion.equipo = JSON.stringify(informacion.equipo);
    }
    existeInformacion(informacion, proId, ts, request, response);
};
const existeInformacion = (informacion, proId, ts, request, response) => __awaiter(void 0, void 0, void 0, function* () {
    let { geolocation, authorization } = request.headers;
    let existe = yield Informacion_1.default.forge({ proId }).fetch();
    existe && existe.toJSON() ?
        actualizar(informacion, proId, ts, geolocation, authorization, response)
        : insertar(informacion, proId, ts, geolocation, authorization, response);
});
const actualizar = (informacion, proId, ts, geolocation, authorization, response) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let nuevaInformacion = yield Informacion_1.default
            .where({ proId })
            .save(Object.assign({}, informacion), { patch: true })
            .call('toJSON');
        response.json({ ts: ts });
        Proyectos_functions_1.proyectoActualizado(proId, ts);
        Functions_common_1.actividadUsuario(proId, authorization, { ts: ts, tipo: 'Nueva informacion', geolocation: geolocation, data: JSON.stringify(informacion) });
    }
    catch (err) {
        console.log(`Error al actualizar el informacion (${proId},'${informacion}',${ts})`, err, '\n');
        response.status(500).json({ mpz: 'Error al actualizar el informacion en MPZ! :(' });
    }
});
const insertar = (informacion, proId, ts, geolocation, authorization, response) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let newInformacion = Object.assign(Object.assign({}, informacion), { proId: proId });
        let nuevaInformacion = yield Informacion_1.default
            .forge()
            .save(newInformacion)
            .call('toJSON');
        response.json({ ts: ts });
        Proyectos_functions_1.proyectoActualizado(proId, ts);
        Functions_common_1.actividadUsuario(proId, authorization, { ts: ts, tipo: 'Creación informacion', geolocation: geolocation, data: JSON.stringify(informacion) });
    }
    catch (err) {
        console.log(`Error al insertar el informacion (${proId},'${informacion}',${ts})`, err, '\n');
        response.status(500).json({ mpz: 'Error al insertar el informacion en MPZ! :(' });
    }
});
