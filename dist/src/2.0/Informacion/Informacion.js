"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const express = __importStar(require("express"));
const Informacion_functions_1 = require("./Informacion.functions");
let informacionRouter2 = express.Router();
exports.informacion2 = informacionRouter2;
informacionRouter2.post("/actualizar", Informacion_functions_1.nuevaInformacion);
