"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Functions_common_1 = require("../Functions/Functions.common");
const Riesgos_1 = __importDefault(require("../../Modelos/Riesgos/Riesgos"));
const Riesgo_1 = __importDefault(require("../../Modelos/Riesgo/Riesgo"));
const Proyectos_functions_1 = require("../Proyectos/Proyectos.functions");
const Security_1 = require("../../Security/Security");
exports.insertarRiesgo = (request, response) => __awaiter(void 0, void 0, void 0, function* () {
    let { geolocation, authorization } = request.headers;
    let { riesgo } = request.body;
    let ts = Date.now();
    try {
        let nuevoRiesgo = yield Riesgos_1.default
            .forge()
            .save(Object.assign({}, riesgo));
        response.json({ id: nuevoRiesgo.id });
        Functions_common_1.actividadUsuario(0, authorization, { ts: ts, tipo: 'Riesgo creado', geolocation: geolocation, data: JSON.stringify(riesgo) });
    }
    catch (err) {
        console.log(`Error al insetar riesgo ${riesgo}`, ts, err);
        response.status(500).json({ mpz: 'Error al insetar riesgo en MPZ! :(' });
    }
});
exports.obtenerRiesgos = (request, response) => __awaiter(void 0, void 0, void 0, function* () {
    let { authorization } = request.headers;
    let ts = Date.now();
    try {
        let riesgos = yield Riesgos_1.default
            .fetchAll();
        const encryptedriesgos = Security_1.encryptByToken(riesgos, authorization);
        response.json({ riesgos: encryptedriesgos });
    }
    catch (err) {
        console.log(`Error al obtener resgos`, ts, err);
        response.status(500).json({ mpz: 'Error al obtener riesgos desde MPZ! :(' });
    }
});
exports.agregarRiesgo = (request, response) => __awaiter(void 0, void 0, void 0, function* () {
    let { geolocation, authorization } = request.headers;
    let { riesgo, proId } = request.body;
    riesgo = Object.assign(Object.assign({}, riesgo), { proId: proId, idRiesgo: riesgo.riesgoId });
    delete riesgo.riesgoId;
    let ts = Date.now();
    try {
        let nuevoRiesgo = yield Riesgo_1.default
            .forge()
            .save(Object.assign({}, riesgo))
            .call('toJSON');
        Proyectos_functions_1.proyectoActualizado(proId, ts);
        Functions_common_1.actividadUsuario(proId, authorization, { ts: ts, tipo: 'Riesgo agregado', geolocation: geolocation, data: JSON.stringify(riesgo) });
        response.json({ riesgoId: nuevoRiesgo.id, ts: ts });
    }
    catch (err) {
        console.log(`Error al agregar riesgo ${proId}, ${riesgo},${ts}`, err, '\n');
        response.status(500).json({ mpz: 'Error al actualizar los riesgos en MPZ! :(' });
    }
});
exports.actualizarRiesgo = (request, response) => __awaiter(void 0, void 0, void 0, function* () {
    let { geolocation, authorization } = request.headers;
    let { riesgo } = request.body;
    let ts = Date.now();
    try {
        let nuevoRiesgo = yield Riesgo_1.default
            .where({ riesgoId: riesgo.riesgoId })
            .save(riesgo, { patch: true });
        Proyectos_functions_1.proyectoActualizado(riesgo.proId, ts);
        Functions_common_1.actividadUsuario(riesgo.proId, authorization, { ts: ts, tipo: 'Riesgo actualizado', geolocation: geolocation, data: JSON.stringify(riesgo) });
        response.json({ ts: ts });
    }
    catch (err) {
        console.log(`Error al agregar riesgo ${riesgo.proId}, ${riesgo},${ts}`, err, '\n');
        response.status(500).json({ mpz: 'Error al actualizar riesgo en MPZ! :(' });
    }
});
exports.eliminararRiesgo = (request, response) => __awaiter(void 0, void 0, void 0, function* () {
    let { geolocation, authorization } = request.headers;
    let { riesgo } = request.body;
    let ts = Date.now();
    try {
        let nuevoRiesgo = yield Riesgo_1.default
            .where({ riesgoId: riesgo.riesgoId })
            .destroy();
        Proyectos_functions_1.proyectoActualizado(riesgo.proId, ts);
        Functions_common_1.actividadUsuario(riesgo.proId, authorization, { ts: ts, tipo: 'Riesgo eliminado', geolocation: geolocation, data: JSON.stringify(riesgo) });
        response.json({ ts: ts });
    }
    catch (err) {
        console.log(`Error al agregar riesgo ${riesgo.proId}, ${riesgo},${ts}`, err, '\n');
        response.status(500).json({ mpz: 'Error al eliminar riesgo en MPZ! :(' });
    }
});
