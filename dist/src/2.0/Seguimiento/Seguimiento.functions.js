"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Seguimiento_1 = __importDefault(require("../../Modelos/Seguimiento/Seguimiento"));
const Proyectos_functions_1 = require("../Proyectos/Proyectos.functions");
const Functions_common_1 = require("../Functions/Functions.common");
const Inspeccion_1 = __importDefault(require("../../Modelos/Inspeccion/Inspeccion"));
const Completar_1 = require("../../Modelos/Completar/Completar");
exports.nuevoSegumiento = (request, response) => {
    let { proId, seguimiento } = request.body;
    let ts = Date.now();
    existeSeguiento(seguimiento, proId, ts, request, response);
};
const existeSeguiento = (seguimiento, proId, ts, request, response) => __awaiter(void 0, void 0, void 0, function* () {
    let { geolocation, authorization } = request.headers;
    let existe = yield Seguimiento_1.default.forge({ proId }).fetch();
    existe && existe.toJSON() ?
        actualizar(seguimiento, proId, ts, geolocation, authorization, response)
        : insertar(seguimiento, proId, ts, geolocation, authorization, response);
});
const actualizar = (seguimiento, proId, ts, geolocation, authorization, response) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let nuevoSeguimiento = yield Seguimiento_1.default
            .where({ proId })
            .save(Object.assign({}, seguimiento), { patch: true })
            .call('toJSON');
        response.json({ ts: ts });
        Proyectos_functions_1.proyectoActualizado(proId, ts);
        Functions_common_1.actividadUsuario(proId, authorization, { ts: ts, tipo: 'Nuevo seguimiento', geolocation: geolocation, data: JSON.stringify(seguimiento) });
    }
    catch (err) {
        console.log(`Error al actualizar el seguimiento (${proId},'${seguimiento}',${ts})`, err, '\n');
        response.status(500).json({ mpz: 'Error al actualizar el seguimiento en MPZ! :(' });
    }
});
const insertar = (seguimiento, proId, ts, geolocation, authorization, response) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let newseguimiento = Object.assign(Object.assign({}, seguimiento), { proId: proId });
        let nuevoSeguimiento = yield Seguimiento_1.default
            .forge()
            .save(newseguimiento)
            .call('toJSON');
        response.json({ ts: ts });
        Proyectos_functions_1.proyectoActualizado(proId, ts);
        Functions_common_1.actividadUsuario(proId, authorization, { ts: ts, tipo: 'Creación seguimiento', geolocation: geolocation, data: JSON.stringify(seguimiento) });
    }
    catch (err) {
        console.log(`Error al insertar el seguimiento (${proId},'${seguimiento}',${ts})`, err, '\n');
        response.status(500).json({ mpz: 'Error al insertar el seguimiento en MPZ! :(' });
    }
});
exports.nuevaInspeccion = (request, response) => __awaiter(void 0, void 0, void 0, function* () {
    let { geolocation, authorization } = request.headers;
    let { proId, inspeccion } = request.body;
    let nuevaInspeccion = Object.assign(Object.assign({}, inspeccion), { proId: proId });
    let ts = Date.now();
    try {
        let inspeccionCreada = Inspeccion_1.default.forge().save(nuevaInspeccion).call('toJSON');
        inspeccion.inspeccionId = inspeccionCreada.id;
        response.json({ ts: ts, inspeccion: inspeccion });
        Proyectos_functions_1.proyectoActualizado(proId, ts);
        Functions_common_1.actividadUsuario(proId, authorization, { ts: ts, tipo: 'Nueva inspeccion', geolocation: geolocation, data: JSON.stringify(nuevaInspeccion) });
    }
    catch (err) {
        console.log(`Error al insertar la inspeccion ('${nuevaInspeccion}',${ts})`, err, '\n');
        response.status(500).json({ mpz: 'Error al insertar el seguimiento en MPZ! :(' });
    }
});
exports.finalizarInspeccion = (request, response) => __awaiter(void 0, void 0, void 0, function* () {
    let { geolocation, authorization } = request.headers;
    let { inspeccion } = request.body;
    let { proId, inspeccionId } = inspeccion;
    let ts = Date.now();
    delete inspeccion.inspeccionId;
    try {
        let inspeccionFinalizada = yield Inspeccion_1.default.where({ inspeccionId: inspeccionId }).save(inspeccion, { patch: true }).call('toJSON');
        response.json({ ts: ts });
        Proyectos_functions_1.proyectoActualizado(proId, ts);
        Functions_common_1.actividadUsuario(inspeccion.proId, authorization, { ts: ts, tipo: 'Inspeccion finalizada', geolocation: geolocation, data: JSON.stringify(inspeccion) });
    }
    catch (err) {
        console.log(`Error al insertar la inspeccion ${exports.nuevaInspeccion},${ts}`, err, '\n');
        response.status(500).json({ mpz: 'Error al finalizar la inspeccion en MPZ! :(' });
    }
});
exports.generarCompletar = (request, response) => __awaiter(void 0, void 0, void 0, function* () {
    let { geolocation, authorization } = request.headers;
    let { proId } = request.body;
    let ts = Date.now();
    try {
        let lista = listaGenerar.map(punto => {
            return Completar_1.Completar.forge().save({ punto: punto, proId: proId, check: false }).call('toJSON');
        });
        let resultado = yield Promise.all(lista);
        resultado = resultado.map(_resultado => {
            let id = _resultado.id;
            delete _resultado.id;
            return Object.assign(Object.assign({}, _resultado), { completarId: id });
        });
        response.json({ lista: resultado, ts: ts });
        Proyectos_functions_1.proyectoActualizado(proId, ts);
        Functions_common_1.actividadUsuario(proId, authorization, { ts: ts, tipo: 'Lista completar', geolocation: geolocation, data: 'puntos creados' });
    }
    catch (err) {
        console.log(`Error al insertar completar ${ts})`, err, '\n');
        response.status(500).json({ mpz: 'Error al insertar lista por completar en MPZ! :(' });
    }
});
exports.completarListo = (request, response) => __awaiter(void 0, void 0, void 0, function* () {
    let { geolocation, authorization } = request.headers;
    let { completar } = request.body;
    let ts = Date.now();
    try {
        yield Completar_1.Completar.where({ completarId: completar.completarId }).save({ check: true }, { patch: true }).call('toJSON');
        response.json({ ts: ts });
        Proyectos_functions_1.proyectoActualizado(completar.proId, ts);
        Functions_common_1.actividadUsuario(completar.proId, authorization, { ts: ts, tipo: 'Lista completar', geolocation: geolocation, data: JSON.stringify(completar) });
    }
    catch (err) {
        console.log(`Error al insertar completar ${ts})`, err, '\n');
        response.status(500).json({ mpz: 'Error al completar punto en MPZ! :(' });
    }
});
exports.completarNuevo = (request, response) => __awaiter(void 0, void 0, void 0, function* () {
    let { geolocation, authorization } = request.headers;
    let { completar } = request.body;
    let ts = Date.now();
    try {
        let nuevoCompletar = yield Completar_1.Completar.forge().save(completar).call('toJSON');
        response.json({ ts: ts, completarId: nuevoCompletar.id });
        Proyectos_functions_1.proyectoActualizado(completar.proId, ts);
        Functions_common_1.actividadUsuario(completar.proId, authorization, { ts: ts, tipo: 'Lista completar', geolocation: geolocation, data: JSON.stringify(completar) });
    }
    catch (err) {
        console.log(`Error al insertar completar ${ts})`, err, '\n');
        response.status(500).json({ mpz: 'Error al completar punto en MPZ! :(' });
    }
});
const listaGenerar = [
    "Definir acción",
    "Definir equipo de proyecto",
    "Realizar visita al proyecto",
    "Elaborar Estudio Topográfico",
    "Elaborar Identificación del Proyecto",
    "Revisar Documento de Identificación",
    "Informe Social",
    "Definir Alcances ",
    "Elaborar fundamentos Técnicos",
    "Elaborar Plan de Calidad",
    "Elaborar Plan de Riesgos",
    "Revisar estudios",
    "Presentar estudios corregidos",
    "Aprobar Diseños Finales",
    "Enviar Planos al CFIA",
    "Pagar tasación CFIA",
    "Permiso de construcción",
    "Gestionar Aportes",
    "Elaborar Términos de Referencia",
    "Revisar Términos de Referencia",
    "Generar solicitud ABS",
    "Trámites de contratación Administrativa",
    "Visita Preoferta",
    "Emitir Criterio de Adjudicación",
    "Definir fecha para inicio de obras",
    "Dar orden de inicio",
    "Autorización salida de materiales",
    "Ejecución de obra",
    "Gestión de Cambios",
    "Elaborar Informe Final de Control de Calidad",
    "Revisar Informe Final de Control de Calidad ",
    "Elaborar Informe Final de Obras",
    "Revisar Informe final de Obras",
    "Tramitar Pago a la empresa",
    "Gestionar Transferencia de Beneficios",
    "Realizar Cierre del Proyecto",
    "Proyecto Cerrado"
];
