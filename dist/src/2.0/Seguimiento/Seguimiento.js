"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const express = __importStar(require("express"));
const Seguimiento_functions_1 = require("./Seguimiento.functions");
let seguimientoRouter2 = express.Router();
exports.seguimiento2 = seguimientoRouter2;
seguimientoRouter2.post("/actualizar", Seguimiento_functions_1.nuevoSegumiento);
seguimientoRouter2.post("/inspeccion/nueva", Seguimiento_functions_1.nuevaInspeccion);
seguimientoRouter2.post("/inspeccion/finalizar", Seguimiento_functions_1.finalizarInspeccion);
seguimientoRouter2.post("/completar", Seguimiento_functions_1.generarCompletar);
seguimientoRouter2.post("/completar/listo", Seguimiento_functions_1.completarListo);
seguimientoRouter2.post("/completar/nuevo", Seguimiento_functions_1.completarNuevo);
