"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const express = __importStar(require("express"));
const Inspecciones_functions_1 = require("./Inspecciones.functions");
let inspeccionesRouter2 = express.Router();
exports.inspecciones2 = inspeccionesRouter2;
inspeccionesRouter2.post('/guardar', Inspecciones_functions_1.guardarInspeccion);
