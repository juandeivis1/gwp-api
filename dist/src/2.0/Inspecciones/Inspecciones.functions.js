"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Inspecciones_1 = __importDefault(require("../../Modelos/Inspecciones/Inspecciones"));
exports.guardarInspeccion = (req, resp) => {
    const { inspeccion } = req.body;
    console.log(inspeccion);
    // inspeccion.id ? actualizarInspeccion(inspeccion, resp) : crearInspeccion(inspeccion, resp)
    resp.json(null);
};
const actualizarInspeccion = (inspeccion, resp) => __awaiter(void 0, void 0, void 0, function* () {
    const ts = Date.now();
    try {
        const nuevaInspeccon = yield Inspecciones_1.default.where({ id: inspeccion.id }).save(inspeccion, { patch: true }).call('toJSON');
        resp.json({ id: nuevaInspeccon.id });
    }
    catch (err) {
        console.log(`'Error al insertar la inspeccion ${inspeccion},${ts}`, err, '\n');
        resp.status(500).json({ mpz: 'Error al insertar la inspeccion en MPZ! :(' });
    }
});
const crearInspeccion = (inspeccion, resp) => __awaiter(void 0, void 0, void 0, function* () {
    const ts = Date.now();
    inspeccion.creacion = ts;
    try {
        const nuevaInspeccon = yield Inspecciones_1.default.forge().save(inspeccion).call('toJSON');
        resp.json({ id: nuevaInspeccon.id });
    }
    catch (err) {
        console.log(`'Error al insertar la inspeccion ${inspeccion},${ts}`, err, '\n');
        resp.status(500).json({ mpz: 'Error al insertar la inspeccion en MPZ! :(' });
    }
});
exports.finalizarInspeccion = (req, resp) => {
    const { inspeccion } = req.body;
    const ts = Date.now();
    inspeccion.realizado = ts;
    inspeccion.id ? actualizarInspeccion(inspeccion, resp) : crearInspeccion(inspeccion, resp);
};
