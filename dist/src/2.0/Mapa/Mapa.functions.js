"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Caminos_1 = __importDefault(require("../../Modelos/Caminos/Caminos"));
const Proyecto_1 = __importDefault(require("../../Modelos/Proyecto/Proyecto"));
const Security_1 = require("../../Security/Security");
exports.buscarCamino = (req, resp) => __awaiter(void 0, void 0, void 0, function* () {
    const { camino } = req.body;
    const { authorization } = req.headers;
    const caminos = yield Caminos_1.default.query(qb => {
        if (!!camino.distrito && camino.distrito.length > 0) {
            qb.whereIn('distrito', camino.distrito);
        }
        if (!!camino.caminoId) {
            qb.where({ caminoId: camino.caminoId });
        }
    })
        .fetchAll()
        .call('toJSON');
    const encryptedCaminos = Security_1.encryptByToken(caminos, authorization);
    resp.json(encryptedCaminos);
});
exports.buscarProyectos = (req, resp) => __awaiter(void 0, void 0, void 0, function* () {
    const { filtros } = req.body;
    const { authorization } = req.headers;
    const proyectos = yield Proyecto_1.default
        .query(qb => {
        qb
            .select('gv_pro.proId', 'gv_pro.nombre', 'gv_pro.sinonimo', 'gv_pro.expediente', 'gv_pro.portafolio', 'gv_pro.prioridad', 'gv_pro.programa', 'gv_pro.creado', 'gv_pro.actualizado', 'gv_pro_informacion.coordenadas')
            .leftJoin('gv_pro_informacion', 'gv_pro_informacion.proId', 'gv_pro.proId')
            .whereNotNull('gv_pro_informacion.coordenadas');
        if (filtros.portafolio.length > 0) {
            qb.whereIn('portafolio', filtros.portafolio);
        }
        if (filtros.prioridad.length > 0) {
            qb.whereIn('prioridad', filtros.prioridad);
        }
        if (filtros.programa.length > 0) {
            qb.whereIn('programa', filtros.programa);
        }
        if (filtros.district.length > 0) {
            qb.whereIn('district', filtros.district);
        }
        if (filtros.city.length > 0) {
            qb.whereIn('city', filtros.city);
        }
        if (filtros.community.length > 0) {
            qb.whereIn('community', filtros.community);
        }
        if (filtros.neighborhood.length > 0) {
            qb.whereIn('neighborhood', filtros.neighborhood);
        }
    })
        .fetchAll()
        .call('toJSON');
    const encryptedProyectos = Security_1.encryptByToken(proyectos, authorization);
    resp.json(encryptedProyectos);
});
