"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const express = __importStar(require("express"));
const Mapa_functions_1 = require("./Mapa.functions");
let mapaRouter = express.Router();
exports.mapa = mapaRouter;
mapaRouter.post('/searchRoads', Mapa_functions_1.buscarCamino);
mapaRouter.post('/searchProjects', Mapa_functions_1.buscarProyectos);
