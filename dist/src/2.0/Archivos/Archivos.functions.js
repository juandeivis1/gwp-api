"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Messaging_1 = require("../../Messaging/Messaging");
const Archivos_1 = __importDefault(require("../../Modelos/Archivos/Archivos"));
const Proyectos_functions_1 = require("../Proyectos/Proyectos.functions");
const Functions_common_1 = require("../Functions/Functions.common");
exports.subirArchivo = (req, resp) => __awaiter(void 0, void 0, void 0, function* () {
    const { archivo } = req.body;
    let ts = Date.now();
    archivo.fileId ? actualizar(archivo, ts, resp) : insertar(archivo, ts, resp);
});
const actualizar = (archivo, ts, resp) => __awaiter(void 0, void 0, void 0, function* () {
    delete archivo.fileId;
    try {
        yield Archivos_1.default
            .where({ fileId: archivo.fileId })
            .save(archivo, { patch: true })
            .call('toJSON');
        resp.json({ ts: ts, update: true });
        Proyectos_functions_1.proyectoActualizado(archivo.proId, ts);
    }
    catch (err) {
        resp.status(500).json({ mpz: 'Error al actualizar el archivo en MPZ! :(' });
        console.log('Error al actualizar el archivo en MPZ! :(', err);
    }
});
const insertar = (archivo, ts, resp) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let nuevoArchivo = yield Archivos_1.default
            .forge()
            .save(archivo)
            .call('toJSON');
        resp.json({ ts: ts, fileId: nuevoArchivo.id, update: false });
        Proyectos_functions_1.proyectoActualizado(archivo.proId, ts);
    }
    catch (err) {
        resp.status(500).json({ mpz: 'Error al insertar el archivo en MPZ! :(' });
        console.log('Error al insertar el archivo en MPZ! :(', err);
    }
});
exports.enviarLink = (req, resp) => __awaiter(void 0, void 0, void 0, function* () {
    const { users, user, fase, url } = req.body;
    let tokens = yield Functions_common_1.obtenerTokens(users);
    let respuesta = yield Messaging_1.enviarArchivo(tokens, user, fase, url);
    resp.send(respuesta);
});
