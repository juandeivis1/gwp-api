"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const express = __importStar(require("express"));
const Archivos_functions_1 = require("./Archivos.functions");
let archivosRouter2 = express.Router();
exports.archivos2 = archivosRouter2;
archivosRouter2.post('/subir', Archivos_functions_1.subirArchivo);
archivosRouter2.post('/enviar', Archivos_functions_1.enviarLink);
