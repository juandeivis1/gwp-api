"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const express = __importStar(require("express"));
const Calidad_functions_1 = require("./Calidad.functions");
let calidadRouter2 = express.Router();
exports.calidad2 = calidadRouter2;
calidadRouter2.post("/actualizar", Calidad_functions_1.nuevaCalidad);
