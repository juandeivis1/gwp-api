"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Functions_common_1 = require("../Functions/Functions.common");
const Calidad_1 = __importDefault(require("../../Modelos/Calidad/Calidad"));
const Proyectos_functions_1 = require("../Proyectos/Proyectos.functions");
exports.nuevaCalidad = (request, response) => {
    let { proId, calidad } = request.body;
    let ts = Date.now();
    existeCalidad(calidad, proId, ts, request, response);
};
const existeCalidad = (calidad, proId, ts, request, response) => __awaiter(void 0, void 0, void 0, function* () {
    let { geolocation, authorization } = request.headers;
    let existe = yield Calidad_1.default.forge({ proId }).fetch();
    existe && existe.toJSON() ?
        actualizar(calidad, proId, ts, geolocation, authorization, response)
        : insertar(calidad, proId, ts, geolocation, authorization, response);
});
const actualizar = (calidad, proId, ts, geolocation, authorization, response) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let nuevaCalidad = yield Calidad_1.default
            .where({ proId })
            .save(Object.assign({}, calidad), { patch: true })
            .call('toJSON');
        response.json({ ts: ts });
        Proyectos_functions_1.proyectoActualizado(proId, ts);
        Functions_common_1.actividadUsuario(proId, authorization, { ts: ts, tipo: 'Nueva calidad', geolocation: geolocation, data: JSON.stringify(calidad) });
    }
    catch (err) {
        console.log(`Error al actualizar el calidad (${proId},'${calidad}',${ts})`, err, '\n');
        response.status(500).json({ mpz: 'Error al actualizar el calidad en MPZ! :(' });
    }
});
const insertar = (calidad, proId, ts, geolocation, authorization, response) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let newseguimiento = Object.assign(Object.assign({}, calidad), { proId: proId });
        let nuevaCalidad = yield Calidad_1.default
            .forge()
            .save(newseguimiento)
            .call('toJSON');
        response.json({ ts: ts });
        Proyectos_functions_1.proyectoActualizado(proId, ts);
        Functions_common_1.actividadUsuario(proId, authorization, { ts: ts, tipo: 'Creación calidad', geolocation: geolocation, data: JSON.stringify(calidad) });
    }
    catch (err) {
        console.log(`Error al insertar el calidad (${proId},'${calidad}',${ts})`, err, '\n');
        response.status(500).json({ mpz: 'Error al insertar el calidad en MPZ! :(' });
    }
});
// const actualizarCalidad = (proId, calidad, ts, request: express.Request, response: express.Response) => {
//     let knex = request.headers.origin == 'http://localhost:4200' ? knexDev : knexProd
//     let { geolocation, authorization } = request.headers;
//     let calidadTemp = JSON.stringify(calidad)
//     calidadTemp = calidadTemp
//         .replace(/\n/gm, " ")
//         .replace(/\r/gm, " ")
//         .replace(/\t/gm, " ")
//         .replace(/\f/gm, " ")
//         .replace(/\&/gm, " ")
//         .replace(/\\b/gm, " ")
//         .replace(/\\n/gm, " ")
//         .replace(/\\r/gm, " ")
//         .replace(/\\t/gm, " ")
//         .replace(/\\f/gm, " ")
//         .replace(/\\&/gm, " ")
//         .replace(/\r\n/gm, " ")
//         .replace(/\\r\\n/gm, " ");
//     knex.raw(`call calidad(${proId},'${calidadTemp}',${ts})`)
//         .then(() => {
//             response.json({ ts: ts });
//             actividadUsuario(proId, authorization, { ts: ts, tipo: 'Nueva calidad agregada', geolocation: geolocation, newData: calidad }, knex)
//         })
//         .catch(err => {
//             console.log(`call calidad(${proId},'${calidadTemp}',${ts})`, new Date(), err, '\n');
//             response.status(500).json({ mpz: 'Error al actualizar calidad en MPZ! :(' })
//         })
// }
